///////////////////////////////////////////////////////////
//  EmptyInterface.h
//  Implementation of the Class EmptyInterface
//  Created on:      30-jul-2015 15:06:13
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Interface.h"

class EmptyInterface : public Interface
{

public:
	EmptyInterface();
	virtual ~EmptyInterface();

	void initialize(list <Street*>* horizontalStreets, list <Street*>* verticalStreets);
	virtual void run();
	void step();

};
