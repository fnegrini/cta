///////////////////////////////////////////////////////////
//  EmptyInterface.cpp
//  Implementation of the Class EmptyInterface
//  Created on:      30-jul-2015 15:06:13
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "EmptyInterface.h"


EmptyInterface::EmptyInterface(){

}



EmptyInterface::~EmptyInterface(){

}


void EmptyInterface::initialize(list <Street*>* horizontalStreets, list <Street*>* verticalStreets){

}


void EmptyInterface::run(){

}


void EmptyInterface::step(){

}