///////////////////////////////////////////////////////////
//  Interface.h
//  Implementation of the Class Interface
//  Created on:      30-jul-2015 15:06:13
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Thread.h"
#include "../Simulator/Street.h"

#include <list>
using namespace std;

class Interface : public Thread
{

public:
	Interface();
	virtual ~Interface();

	virtual void initialize(list <Street*>* horizontalStreets, list <Street*>* verticalStreets);
	virtual void run();
	void step();

};
