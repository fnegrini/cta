///////////////////////////////////////////////////////////
//  ConcreteCongestionLevelStrategy.cpp
//  Implementation of the Class ConcreteCongestionLevelStrategy
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "ConcreteCongestionLevelStrategy.h"


ConcreteCongestionLevelStrategy::ConcreteCongestionLevelStrategy(){

}

ConcreteCongestionLevelStrategy::~ConcreteCongestionLevelStrategy(){

}


Interface* ConcreteCongestionLevelStrategy::CreateInterface(){

	//return  new InterfaceForms();
	return  new EmptyInterface();
}


Semaphore* ConcreteCongestionLevelStrategy::CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){

	return  new Semaphore(pSemaphoreId, pHorizontalTrafficLight, pVerticalTrafficLight);
}


Traffic_Control_Strategy* ConcreteCongestionLevelStrategy::CreateTrafficControlStrategy(){

	return  new Congestion_Level_Strategy();
}