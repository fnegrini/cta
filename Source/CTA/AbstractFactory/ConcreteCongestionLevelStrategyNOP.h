///////////////////////////////////////////////////////////
//  ConcreteCongestionLevelStrategyNOP.h
//  Implementation of the Class ConcreteCongestionLevelStrategyNOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "AbstractFactory.h"
#include "../Interface/EmptyInterface.h"
#include "../Simulator/NOP_CBCL/Semaphore_NOP_CBCL.h"
#include "../Controller/Congestion_Level_Strategy_NOP.h"

class ConcreteCongestionLevelStrategyNOP : public AbstractFactory
{

public:
	ConcreteCongestionLevelStrategyNOP();
	virtual ~ConcreteCongestionLevelStrategyNOP();
	
	Interface* CreateInterface();
	Semaphore* CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0);
	Traffic_Control_Strategy* CreateTrafficControlStrategy();

};
