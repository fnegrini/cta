///////////////////////////////////////////////////////////
//  ConcreteCongestionLevelStrategy.h
//  Implementation of the Class ConcreteCongestionLevelStrategy
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "AbstractFactory.h"
#include "../Interface/EmptyInterface.h"
#include "../Simulator/Semaphore.h"
#include "../Controller/Congestion_Level_Strategy.h"

class ConcreteCongestionLevelStrategy : public AbstractFactory
{

public:
	ConcreteCongestionLevelStrategy();
	virtual ~ConcreteCongestionLevelStrategy();

	Interface* CreateInterface();
	Semaphore* CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0);
	Traffic_Control_Strategy* CreateTrafficControlStrategy();

};
