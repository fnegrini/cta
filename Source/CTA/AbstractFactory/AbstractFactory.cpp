///////////////////////////////////////////////////////////
//  AbstractFactory.cpp
//  Implementation of the Class AbstractFactory
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "AbstractFactory.h"

#include "ConcreteCongestionLevelStrategy.h"
#include "ConcreteCongestionLevelStrategyNOP.h"
#include "ConcreteIndependentStrategy.h"
#include "ConcreteIndependentStrategyNOP.h"
#include "ConcreteTrafficFacilitationStrategy.h"
#include "ConcreteTrafficFacilitationStrategyNOP.h"
#include "stdio.h"


AbstractFactory::AbstractFactory(){

}

AbstractFactory::~AbstractFactory(){

}