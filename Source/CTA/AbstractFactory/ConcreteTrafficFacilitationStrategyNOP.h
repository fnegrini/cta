///////////////////////////////////////////////////////////
//  ConcreteTrafficFacilitationStrategyNOP.h
//  Implementation of the Class ConcreteTrafficFacilitationStrategyNOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "AbstractFactory.h"
#include "../Interface/EmptyInterface.h"
#include "../Simulator/NOP_CBTF/Semaphore_NOP_CBTF.h"
#include "../Controller/Traffic_Facilitation_Strategy_NOP.h"


class ConcreteTrafficFacilitationStrategyNOP : public AbstractFactory
{

public:
	ConcreteTrafficFacilitationStrategyNOP();
	virtual ~ConcreteTrafficFacilitationStrategyNOP();

	Interface* CreateInterface();
	Semaphore* CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0);
	Traffic_Control_Strategy* CreateTrafficControlStrategy();

};
