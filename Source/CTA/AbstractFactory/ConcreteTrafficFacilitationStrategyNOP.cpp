///////////////////////////////////////////////////////////
//  ConcreteTrafficFacilitationStrategyNOP.cpp
//  Implementation of the Class ConcreteTrafficFacilitationStrategyNOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "ConcreteTrafficFacilitationStrategyNOP.h"


ConcreteTrafficFacilitationStrategyNOP::ConcreteTrafficFacilitationStrategyNOP(){

}



ConcreteTrafficFacilitationStrategyNOP::~ConcreteTrafficFacilitationStrategyNOP(){

}

Interface* ConcreteTrafficFacilitationStrategyNOP::CreateInterface(){

	//return  new InterfaceForms();
	return  new EmptyInterface();
}


Semaphore* ConcreteTrafficFacilitationStrategyNOP::CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){

	return  new Semaphore_NOP_CBTF(pSemaphoreId, pHorizontalTrafficLight, pVerticalTrafficLight, pTrafficFacilitating);
}


Traffic_Control_Strategy* ConcreteTrafficFacilitationStrategyNOP::CreateTrafficControlStrategy(){

	return  new Traffic_Facilitation_Strategy_NOP();
}