///////////////////////////////////////////////////////////
//  AbstractFactory.h
//  Implementation of the Class AbstractFactory
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once
#include "../Interface/Interface.h"
#include "../Simulator/Semaphore.h"
#include "../Controller/Traffic_Control_Strategy.h"

class AbstractFactory
{

public:
	AbstractFactory();
	virtual ~AbstractFactory();

	virtual Interface* CreateInterface()=0;
	virtual Semaphore* CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0) = 0;
	virtual Traffic_Control_Strategy* CreateTrafficControlStrategy()=0;
};

