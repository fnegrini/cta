///////////////////////////////////////////////////////////
//  ConcreteIndependentStrategyNOP.h
//  Implementation of the Class ConcreteIndependentStrategyNOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "AbstractFactory.h"
#include "../Interface/EmptyInterface.h"
#include "../Simulator/NOP/Semaphore_NOP.h"
#include "../Controller/Independent_Strategy_NOP.h"

class ConcreteIndependentStrategyNOP : public AbstractFactory
{

public:
	ConcreteIndependentStrategyNOP();
	virtual ~ConcreteIndependentStrategyNOP();
	
	Interface* CreateInterface();
	Semaphore* CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0);
	Traffic_Control_Strategy* CreateTrafficControlStrategy();

};
