///////////////////////////////////////////////////////////
//  ConcreteCongestionLevelStrategyNOP.cpp
//  Implementation of the Class ConcreteCongestionLevelStrategyNOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "ConcreteCongestionLevelStrategyNOP.h"


ConcreteCongestionLevelStrategyNOP::ConcreteCongestionLevelStrategyNOP(){

}

ConcreteCongestionLevelStrategyNOP::~ConcreteCongestionLevelStrategyNOP(){

}

Interface* ConcreteCongestionLevelStrategyNOP::CreateInterface(){

	//return  new InterfaceForms();
	return  new EmptyInterface();
}


Semaphore* ConcreteCongestionLevelStrategyNOP::CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){

	return  new Semaphore_NOP_CBCL(pSemaphoreId, pHorizontalTrafficLight, pVerticalTrafficLight);
}


Traffic_Control_Strategy* ConcreteCongestionLevelStrategyNOP::CreateTrafficControlStrategy(){

	return  new Congestion_Level_Strategy_NOP();
}