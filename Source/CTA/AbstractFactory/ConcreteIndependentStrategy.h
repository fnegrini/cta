///////////////////////////////////////////////////////////
//  ConcreteIndependentStrategy.h
//  Implementation of the Class ConcreteIndependentStrategy
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "AbstractFactory.h"
#include "../Interface/EmptyInterface.h"
#include "../Simulator/Semaphore.h"
#include "../Controller/Independent_Strategy.h"

class ConcreteIndependentStrategy : public AbstractFactory
{

public:
	ConcreteIndependentStrategy();
	virtual ~ConcreteIndependentStrategy();

	Interface* CreateInterface();
	Semaphore* CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0);
	Traffic_Control_Strategy* CreateTrafficControlStrategy();

};
