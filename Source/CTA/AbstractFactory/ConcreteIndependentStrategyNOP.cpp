///////////////////////////////////////////////////////////
//  ConcreteIndependentStrategyNOP.cpp
//  Implementation of the Class ConcreteIndependentStrategyNOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "ConcreteIndependentStrategyNOP.h"


ConcreteIndependentStrategyNOP::ConcreteIndependentStrategyNOP(){

}

ConcreteIndependentStrategyNOP::~ConcreteIndependentStrategyNOP(){

}

Interface* ConcreteIndependentStrategyNOP::CreateInterface(){

	//return  new InterfaceForms();
	return  new EmptyInterface();
}


Semaphore* ConcreteIndependentStrategyNOP::CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){

	return  new Semaphore_NOP(pSemaphoreId, pHorizontalTrafficLight, pVerticalTrafficLight);
}


Traffic_Control_Strategy* ConcreteIndependentStrategyNOP::CreateTrafficControlStrategy(){

	return  new Independent_Strategy_NOP();
}