///////////////////////////////////////////////////////////
//  ConcreteIndependentStrategy.cpp
//  Implementation of the Class ConcreteIndependentStrategy
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "ConcreteIndependentStrategy.h"


ConcreteIndependentStrategy::ConcreteIndependentStrategy(){

}



ConcreteIndependentStrategy::~ConcreteIndependentStrategy(){

}

Interface* ConcreteIndependentStrategy::CreateInterface(){

	//return  new InterfaceForms();
	return  new EmptyInterface();
}


Semaphore* ConcreteIndependentStrategy::CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){

	return  new Semaphore(pSemaphoreId, pHorizontalTrafficLight, pVerticalTrafficLight);
}


Traffic_Control_Strategy* ConcreteIndependentStrategy::CreateTrafficControlStrategy(){

	return  new Independent_Strategy();
}