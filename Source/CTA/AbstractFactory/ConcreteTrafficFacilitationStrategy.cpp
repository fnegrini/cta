///////////////////////////////////////////////////////////
//  ConcreteTrafficFacilitationStrategy.cpp
//  Implementation of the Class ConcreteTrafficFacilitationStrategy
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "ConcreteTrafficFacilitationStrategy.h"


ConcreteTrafficFacilitationStrategy::ConcreteTrafficFacilitationStrategy(){

}

ConcreteTrafficFacilitationStrategy::~ConcreteTrafficFacilitationStrategy(){

}

Interface* ConcreteTrafficFacilitationStrategy::CreateInterface(){

	//return  new InterfaceForms();
	return  new EmptyInterface();
}


Semaphore* ConcreteTrafficFacilitationStrategy::CreateSemaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){

	return  new Semaphore(pSemaphoreId, pHorizontalTrafficLight, pVerticalTrafficLight, pTrafficFacilitating);
}


Traffic_Control_Strategy* ConcreteTrafficFacilitationStrategy::CreateTrafficControlStrategy(){

	return  new Traffic_Facilitation_Strategy();
}