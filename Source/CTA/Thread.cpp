///////////////////////////////////////////////////////////
//  Thread.cpp
//  Implementation of the Class Thread
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Thread.h"

// refer�ncia para a vari�vel est�tica;
pthread_mutex_t Thread::_mutex = NULL;

Thread::Thread(){
}

Thread::~Thread(){
	if (NULL != Thread::_mutex)
		pthread_mutex_destroy(&Thread::_mutex);
}

void Thread::join(){
	int status = pthread_join(_threadID, NULL);
	if (status != 0)
		printError("comando join falhou.");
}


void Thread::lock(){
	if (NULL == Thread::_mutex)
		pthread_mutex_init(&Thread::_mutex, NULL);
	pthread_mutex_lock(&Thread::_mutex);
}


void Thread::printError(const string msg){
	lock();     // p�ra para a mensagem;
	cout << "Error: " << msg << endl;
	unlock();
}


void Thread::run(){

}


void* Thread::runThread(void* pThread){
	Thread* sThread = static_cast<Thread*>(pThread);
	if (NULL == sThread){
		cout << "thread falhou." << endl;
	}
	else{
		sThread->run();/* executa a thread; */
	}
	return NULL;
}


void Thread::start(){
	// inicia o atributo;
	int status = pthread_attr_init(&_tAttribute);
	status = pthread_attr_setscope(&_tAttribute, PTHREAD_SCOPE_SYSTEM);
	if (status != 0)
		printError("falha ao iniciar atributo da thread.");
	// cria uma thread;
	status = pthread_create(&_threadID, &_tAttribute, Thread::runThread, (void*)this);
	if (status != 0)
		printError("falha ao iniciar a thread.");
	// destr�i o atributo;
	status = pthread_attr_destroy(&_tAttribute);
	if (status != 0)
		printError("falha ao destruir atributo da thread.");
}


void Thread::unlock(){
	if (NULL != Thread::_mutex)
		pthread_mutex_unlock(&Thread::_mutex);
}


void Thread::yield(){
	sched_yield();
}