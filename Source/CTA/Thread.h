///////////////////////////////////////////////////////////
//  Thread.h
//  Implementation of the Class Thread
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

//#include "pthread.h"
//#include <windows.h>
#include <QThread>
#include <string>
using std::string;
#include <iostream>
using namespace std;

class Thread
{

public:
	Thread();
	virtual ~Thread();

	void join();
	void lock();
	void start();
	void unlock();
	void yield();

private:
	static pthread_mutex_t _mutex;
	pthread_attr_t _tAttribute;
	pthread_t _threadID;

	void printError(const string msg);
	virtual void run();
	static void* runThread(void* pThread);

};
