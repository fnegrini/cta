QT += core
QT -= gui

CONFIG += c++11

TARGET = CTA
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    AbstractFactory/AbstractFactory.cpp \
    AbstractFactory/ConcreteCongestionLevelStrategy.cpp \
    AbstractFactory/ConcreteCongestionLevelStrategyNOP.cpp \
    AbstractFactory/ConcreteIndependentStrategy.cpp \
    AbstractFactory/ConcreteIndependentStrategyNOP.cpp \
    AbstractFactory/ConcreteTrafficFacilitationStrategy.cpp \
    AbstractFactory/ConcreteTrafficFacilitationStrategyNOP.cpp \
    Controller/Congestion_Level_Strategy.cpp \
    Controller/Congestion_Level_Strategy_NOP.cpp \
    Controller/Independent_Strategy.cpp \
    Controller/Independent_Strategy_NOP.cpp \
    Controller/Traffic_Control_Strategy.cpp \
    Controller/Traffic_Facilitation_Strategy.cpp \
    Controller/Traffic_Facilitation_Strategy_NOP.cpp \
    Controller/Traffic_Lights_Controller.cpp \
    Interface/EmptyInterface.cpp \
    Interface/Interface.cpp \
    Util/PerformanceMeasure.cpp \
    Simulator/Block.cpp \
    Simulator/Block_List.cpp \
    Simulator/CTA_Simulator.cpp \
    Simulator/InputFile.cpp \
    Simulator/Intersection.cpp \
    Simulator/Layout.cpp \
    Simulator/Logger.cpp \
    Simulator/Semaphore.cpp \
    Simulator/Street.cpp \
    Simulator/Traffic_Light.cpp \
    Simulator/Vehicle.cpp \
    Simulator/Vehicle_Block.cpp \
    Simulator/Vehicle_Sensor.cpp \
    Simulator/NOP/rlHorizontalTrafficLightGreen.cpp \
    Simulator/NOP/rlHorizontalTrafficLightRed.cpp \
    Simulator/NOP/rlHorizontalTrafficLightYellow.cpp \
    Simulator/NOP/rlVerticalTrafficLightGreen.cpp \
    Simulator/NOP/rlVerticalTrafficLightRed.cpp \
    Simulator/NOP/rlVerticalTrafficLightYellow.cpp \
    Simulator/NOP/Semaphore_NOP.cpp \
    Simulator/NOP_CBCL/prAtHVSS1.cpp \
    Simulator/NOP_CBCL/prAtHVSS2.cpp \
    Simulator/NOP_CBCL/prAtSeconds1.cpp \
    Simulator/NOP_CBCL/prAtSeconds2.cpp \
    Simulator/NOP_CBCL/prAtSeconds3.cpp \
    Simulator/NOP_CBCL/prAtSeconds4.cpp \
    Simulator/NOP_CBCL/prAtSeconds5.cpp \
    Simulator/NOP_CBCL/prAtSeconds6.cpp \
    Simulator/NOP_CBCL/prAtSeconds7.cpp \
    Simulator/NOP_CBCL/prAtSeconds8.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState0.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState1.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState2.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState3.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState4.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState5.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState6.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState7.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState8.cpp \
    Simulator/NOP_CBCL/prAtSemaphoreState9.cpp \
    Simulator/NOP_CBCL/prAtVVSS1.cpp \
    Simulator/NOP_CBCL/prAtVVSS2.cpp \
    Simulator/NOP_CBCL/rlCBCL1.cpp \
    Simulator/NOP_CBCL/rlCBCL2.cpp \
    Simulator/NOP_CBCL/rlCBCL3.cpp \
    Simulator/NOP_CBCL/rlCBCL4.cpp \
    Simulator/NOP_CBCL/rlCBCL5.cpp \
    Simulator/NOP_CBCL/rlCBCL6.cpp \
    Simulator/NOP_CBCL/rlCBCL7.cpp \
    Simulator/NOP_CBCL/rlCBCL8.cpp \
    Simulator/NOP_CBCL/rlCBCL9.cpp \
    Simulator/NOP_CBCL/rlCBCL10.cpp \
    Simulator/NOP_CBCL/rlCBCL11.cpp \
    Simulator/NOP_CBCL/rlCBCL12.cpp \
    Simulator/NOP_CBCL/rlCBCL13.cpp \
    Simulator/NOP_CBCL/rlCBCL14.cpp \
    Simulator/NOP_CBCL/rlCBCL15.cpp \
    Simulator/NOP_CBCL/rlCBCL16.cpp \
    Simulator/NOP_CBCL/rlCBCL17.cpp \
    Simulator/NOP_CBCL/rlCBCL18.cpp \
    Simulator/NOP_CBCL/Semaphore_NOP_CBCL.cpp \
    Simulator/NOP_CBTF/prCBTFAtGWProp1.cpp \
    Simulator/NOP_CBTF/prCBTFAtGWProp2.cpp \
    Simulator/NOP_CBTF/prCBTFAtGWType1.cpp \
    Simulator/NOP_CBTF/prCBTFAtGWType2.cpp \
    Simulator/NOP_CBTF/prCBTFAtHVSS1.cpp \
    Simulator/NOP_CBTF/prCBTFAtHVSS2.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds1.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds2.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds3.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds4.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds5.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds6.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds7.cpp \
    Simulator/NOP_CBTF/prCBTFAtSeconds8.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState0.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState1.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState2.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState3.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState4.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState5.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState6.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState7.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState8.cpp \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState9.cpp \
    Simulator/NOP_CBTF/prCBTFAtVVSS1.cpp \
    Simulator/NOP_CBTF/prCBTFAtVVSS2.cpp \
    Simulator/NOP_CBTF/rlCBTF1.cpp \
    Simulator/NOP_CBTF/rlCBTF2.cpp \
    Simulator/NOP_CBTF/rlCBTF3.cpp \
    Simulator/NOP_CBTF/rlCBTF4.cpp \
    Simulator/NOP_CBTF/rlCBTF5.cpp \
    Simulator/NOP_CBTF/rlCBTF6.cpp \
    Simulator/NOP_CBTF/rlCBTF7.cpp \
    Simulator/NOP_CBTF/rlCBTF8.cpp \
    Simulator/NOP_CBTF/rlCBTF9.cpp \
    Simulator/NOP_CBTF/rlCBTF10.cpp \
    Simulator/NOP_CBTF/rlCBTF11.cpp \
    Simulator/NOP_CBTF/rlCBTF12.cpp \
    Simulator/NOP_CBTF/rlCBTF13.cpp \
    Simulator/NOP_CBTF/rlCBTF14.cpp \
    Simulator/NOP_CBTF/rlCBTF15.cpp \
    Simulator/NOP_CBTF/rlCBTF16.cpp \
    Simulator/NOP_CBTF/rlCBTF17.cpp \
    Simulator/NOP_CBTF/rlCBTF18.cpp \
    Simulator/NOP_CBTF/rlCBTF120.cpp \
    Simulator/NOP_CBTF/rlCBTF140.cpp \
    Simulator/NOP_CBTF/rlCBTF160.cpp \
    Simulator/NOP_CBTF/rlCBTF180.cpp \
    Simulator/NOP_CBTF/Semaphore_NOP_CBTF.cpp \
    Thread.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    AbstractFactory/AbstractFactory.h \
    AbstractFactory/ConcreteCongestionLevelStrategy.h \
    AbstractFactory/ConcreteCongestionLevelStrategyNOP.h \
    AbstractFactory/ConcreteIndependentStrategy.h \
    AbstractFactory/ConcreteIndependentStrategyNOP.h \
    AbstractFactory/ConcreteTrafficFacilitationStrategy.h \
    AbstractFactory/ConcreteTrafficFacilitationStrategyNOP.h \
    Controller/Congestion_Level_Strategy.h \
    Controller/Congestion_Level_Strategy_NOP.h \
    Controller/Independent_Strategy.h \
    Controller/Independent_Strategy_NOP.h \
    Controller/Traffic_Control_Strategy.h \
    Controller/Traffic_Facilitation_Strategy.h \
    Controller/Traffic_Facilitation_Strategy_NOP.h \
    Controller/Traffic_Lights_Controller.h \
    Interface/EmptyInterface.h \
    Interface/Interface.h \
    Util/PerformanceMeasure.h \
    Simulator/Block.h \
    Simulator/Block_List.h \
    Simulator/CTA_Simulator.h \
    Simulator/Direction.h \
    Simulator/GreenWavePropagation.h \
    Simulator/GreenWaveType.h \
    Simulator/InputFile.h \
    Simulator/Intersection.h \
    Simulator/Layout.h \
    Simulator/List.h \
    Simulator/Logger.h \
    Simulator/Semaphore.h \
    Simulator/Street.h \
    Simulator/Traffic_Light.h \
    Simulator/Traffic_Light_State.h \
    Simulator/Vehicle.h \
    Simulator/Vehicle_Block.h \
    Simulator/Vehicle_Sensor.h \
    Simulator/Vehicle_Sensor_State.h \
    Simulator/NOP/rlHorizontalTrafficLightGreen.h \
    Simulator/NOP/rlHorizontalTrafficLightRed.h \
    Simulator/NOP/rlHorizontalTrafficLightYellow.h \
    Simulator/NOP/rlVerticalTrafficLightGreen.h \
    Simulator/NOP/rlVerticalTrafficLightRed.h \
    Simulator/NOP/rlVerticalTrafficLightYellow.h \
    Simulator/NOP/Semaphore_NOP.h \
    Simulator/NOP_CBCL/prAtHVSS1.h \
    Simulator/NOP_CBCL/prAtHVSS2.h \
    Simulator/NOP_CBCL/prAtSeconds1.h \
    Simulator/NOP_CBCL/prAtSeconds2.h \
    Simulator/NOP_CBCL/prAtSeconds3.h \
    Simulator/NOP_CBCL/prAtSeconds4.h \
    Simulator/NOP_CBCL/prAtSeconds5.h \
    Simulator/NOP_CBCL/prAtSeconds6.h \
    Simulator/NOP_CBCL/prAtSeconds7.h \
    Simulator/NOP_CBCL/prAtSeconds8.h \
    Simulator/NOP_CBCL/prAtSemaphoreState0.h \
    Simulator/NOP_CBCL/prAtSemaphoreState1.h \
    Simulator/NOP_CBCL/prAtSemaphoreState2.h \
    Simulator/NOP_CBCL/prAtSemaphoreState3.h \
    Simulator/NOP_CBCL/prAtSemaphoreState4.h \
    Simulator/NOP_CBCL/prAtSemaphoreState5.h \
    Simulator/NOP_CBCL/prAtSemaphoreState6.h \
    Simulator/NOP_CBCL/prAtSemaphoreState7.h \
    Simulator/NOP_CBCL/prAtSemaphoreState8.h \
    Simulator/NOP_CBCL/prAtSemaphoreState9.h \
    Simulator/NOP_CBCL/prAtVVSS1.h \
    Simulator/NOP_CBCL/prAtVVSS2.h \
    Simulator/NOP_CBCL/rlCBCL1.h \
    Simulator/NOP_CBCL/rlCBCL2.h \
    Simulator/NOP_CBCL/rlCBCL3.h \
    Simulator/NOP_CBCL/rlCBCL4.h \
    Simulator/NOP_CBCL/rlCBCL5.h \
    Simulator/NOP_CBCL/rlCBCL6.h \
    Simulator/NOP_CBCL/rlCBCL7.h \
    Simulator/NOP_CBCL/rlCBCL8.h \
    Simulator/NOP_CBCL/rlCBCL9.h \
    Simulator/NOP_CBCL/rlCBCL10.h \
    Simulator/NOP_CBCL/rlCBCL11.h \
    Simulator/NOP_CBCL/rlCBCL12.h \
    Simulator/NOP_CBCL/rlCBCL13.h \
    Simulator/NOP_CBCL/rlCBCL14.h \
    Simulator/NOP_CBCL/rlCBCL15.h \
    Simulator/NOP_CBCL/rlCBCL16.h \
    Simulator/NOP_CBCL/rlCBCL17.h \
    Simulator/NOP_CBCL/rlCBCL18.h \
    Simulator/NOP_CBCL/Semaphore_NOP_CBCL.h \
    Simulator/NOP_CBTF/prCBTFAtGWProp1.h \
    Simulator/NOP_CBTF/prCBTFAtGWProp2.h \
    Simulator/NOP_CBTF/prCBTFAtGWType1.h \
    Simulator/NOP_CBTF/prCBTFAtGWType2.h \
    Simulator/NOP_CBTF/prCBTFAtHVSS1.h \
    Simulator/NOP_CBTF/prCBTFAtHVSS2.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds1.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds2.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds3.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds4.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds5.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds6.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds7.h \
    Simulator/NOP_CBTF/prCBTFAtSeconds8.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState0.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState1.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState2.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState3.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState4.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState5.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState6.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState7.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState8.h \
    Simulator/NOP_CBTF/prCBTFAtSemaphoreState9.h \
    Simulator/NOP_CBTF/prCBTFAtVVSS1.h \
    Simulator/NOP_CBTF/prCBTFAtVVSS2.h \
    Simulator/NOP_CBTF/rlCBTF1.h \
    Simulator/NOP_CBTF/rlCBTF2.h \
    Simulator/NOP_CBTF/rlCBTF3.h \
    Simulator/NOP_CBTF/rlCBTF4.h \
    Simulator/NOP_CBTF/rlCBTF5.h \
    Simulator/NOP_CBTF/rlCBTF6.h \
    Simulator/NOP_CBTF/rlCBTF7.h \
    Simulator/NOP_CBTF/rlCBTF8.h \
    Simulator/NOP_CBTF/rlCBTF9.h \
    Simulator/NOP_CBTF/rlCBTF10.h \
    Simulator/NOP_CBTF/rlCBTF11.h \
    Simulator/NOP_CBTF/rlCBTF12.h \
    Simulator/NOP_CBTF/rlCBTF13.h \
    Simulator/NOP_CBTF/rlCBTF14.h \
    Simulator/NOP_CBTF/rlCBTF15.h \
    Simulator/NOP_CBTF/rlCBTF16.h \
    Simulator/NOP_CBTF/rlCBTF17.h \
    Simulator/NOP_CBTF/rlCBTF18.h \
    Simulator/NOP_CBTF/rlCBTF120.h \
    Simulator/NOP_CBTF/rlCBTF140.h \
    Simulator/NOP_CBTF/rlCBTF160.h \
    Simulator/NOP_CBTF/rlCBTF180.h \
    Simulator/NOP_CBTF/Semaphore_NOP_CBTF.h \
    Thread.h
