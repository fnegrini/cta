///////////////////////////////////////////////////////////
//  Vehicle_Block.h
//  Implementation of the Class Vehicle_Block
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

class Block;
class Vehicle;

class Vehicle_Block
{

public:
	Vehicle_Block();
	~Vehicle_Block();
	
	Block *GetBlock();
	int GetLane();
	Vehicle *GetVehicle();
	int GetPosition();
	void SetBlock(Block *newVal);
	void SetLane(int newVal);
	void SetVehicle(Vehicle *newVal);
	void SetPosition(int newVal);

private:
	Block *block;
	int lane;
	Vehicle* vehicle;
	int position;

};
