#include "InputFile.h"


InputFile::InputFile():input("SimulationInputData.dat", ios::in)
{
}


InputFile::~InputFile()
{
}

void InputFile::readFile()
{
	if (!input)
	{
		cerr << "Arquivo n�o pode ser aberto" << endl;
		fflush(stdin); getchar();
		return;
	}
	result.clear();
	while (!input.eof())
	{

		string line;
		input >> line;
		if (!line.empty())
		{
			result.push_back(line);
		}
	}

	input.close();
}

vector<string> InputFile::getResult()
{
	return result;
}