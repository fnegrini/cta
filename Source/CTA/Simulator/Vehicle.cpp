///////////////////////////////////////////////////////////
//  Vehicle.cpp
//  Implementation of the Class Vehicle
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Vehicle.h"
#include "Vehicle_Block.h"
#include "Traffic_Light_State.h"
#include "Intersection.h"
#include "Block.h"
#include "Street.h"
#include "CTA_Simulator.h"

int Vehicle::vehicleCounter = 0;

Vehicle::Vehicle(int pId, int pVelocity, int pLength){
	id = pId;
	velocity = pVelocity;
	length = pLength;
	vehicleCounter++;
}

Vehicle::~Vehicle(){

}


int Vehicle::GetId(){

	return id;
}


int Vehicle::GetLength(){

	return length;
}


int Vehicle::GetTravelledDistance(){

	return travelledDistance;
}


int Vehicle::GetVelocity(){

	return velocity;
}

Vehicle_Block* Vehicle::GetVehicleBlock(){

	return vehicleBlock;
}


void Vehicle::SetId(int newVal){

	id = newVal;
}


void Vehicle::SetLength(int newVal){

	length = newVal;
}


void Vehicle::SetTravelledDistance(int newVal){

	travelledDistance = newVal;
}


void Vehicle::SetVelocity(int newVal){

	velocity = newVal;
}

void Vehicle::SetVehicleBlock(Vehicle_Block* newVal){

	vehicleBlock = newVal;
}

bool Vehicle::GetIsNewInTheBlock()
{
	return isNewInTheBlock;
}

void Vehicle::SetIsNewInTheBlock(bool pIsNew)
{
	isNewInTheBlock = pIsNew;
}

int Vehicle::getCount(){
	return vehicleCounter;
}

void Vehicle::move(list<Vehicle_Block*>* pListVehicle){
	//if vehicle new in the block it has already moved for this simulation round so skip it
	if (isNewInTheBlock){
		isNewInTheBlock = false;
		return;
	}

	Block* block = vehicleBlock->GetBlock();		
	if (block->GetTrafficLight()->GetState() != RED)
	{
		/*
			Calculo da porcentagem de "turns" e verifica qual � o pr�ximo bloco
		*/
		Block* nextBlock;
		if (block->GetStreet()->GetDirection() == EAST || block->GetStreet()->GetDirection() == WEST)
		{
			if (block->GetCalcPctOfTurns()<100)
			{
				nextBlock = block->GetIntersection()->GetNextHorizontalBlock();				
			}
			else
			{
				nextBlock = block->GetIntersection()->GetNextVerticalBlock();
			}
			
		}else
		{
			if (block->GetCalcPctOfTurns()<100)
			{
				nextBlock = block->GetIntersection()->GetNextVerticalBlock();				
			}
			else
			{
				nextBlock = block->GetIntersection()->GetNextHorizontalBlock();
			}
			
		}

		if (NULL == nextBlock)
		{
			//Final da rua			
			block->GetStreet()->addVehicleToExitPoint(this);
			block->removeVehicle(vehicleBlock);	
			CTA_Simulator::getInstance()->logEvent(Logger::EV_VEHICLE_EXIT_POINT, id);
		}
		else if (nextBlock->hasEnoughSpace(this->GetLength()))
		{			
			//Caso o ve�culo n�o ultrapasse v� passar para o pr�ximo bloco agora, apenas soma a sua velocidade
			if (vehicleBlock->GetPosition() + velocity < 100)
			{				
				vehicleBlock->SetPosition(vehicleBlock->GetPosition() + velocity);
				CTA_Simulator::getInstance()->logEvent(Logger::EV_VEHICLE_MOVE, id, this->GetVehicleBlock()->GetBlock()->GetStreet()->GetId(), this->GetVehicleBlock()->GetBlock()->GetId(), this->GetVehicleBlock()->GetLane(), this->GetVehicleBlock()->GetPosition());
			}
			else{//Se ele entrar mudar de bloco, remove do blovo atual e insere no pr�ximo bloco					
				block->removeVehicle(vehicleBlock);
				isNewInTheBlock = true;				
				nextBlock->addVehicle(this, vehicleBlock->GetPosition() + velocity - 100);
			}
			
		}
		
	}
	else
	{
		int sumPos = MoveLenght(pListVehicle);
		if (vehicleBlock->GetPosition() + sumPos < 100){			
			vehicleBlock->SetPosition(vehicleBlock->GetPosition() + sumPos);
		}
		else
		{
			vehicleBlock->SetPosition(99);
		}
	}
}

int Vehicle::MoveLenght(list<Vehicle_Block*>* pListVehicle)
{
	int sumPos = velocity;
	//Verifica se haver� colis�o entre algum ve�culo.
	for (list<Vehicle_Block*>::iterator it = pListVehicle->begin(); it != pListVehicle->end(); it++){
		if ((*it)->GetLane() == this->GetVehicleBlock()->GetLane()
			&& (vehicleBlock->GetPosition()) <= ((*it)->GetPosition())
			&& (*it)->GetVehicle()->GetId() != this->GetId())
		{
			if ((vehicleBlock->GetPosition() + velocity) >((*it)->GetPosition() - (*it)->GetVehicle()->GetLength()))
			{
				sumPos = ((*it)->GetPosition() - (*it)->GetVehicle()->GetLength() - vehicleBlock->GetPosition()-1);
				if (sumPos < 0)
				{
					sumPos = 0;
				}
			}
			else
			{
				sumPos = 20;
			}
		}
	}
	return sumPos;
}