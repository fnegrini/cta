#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
using std::ofstream;
using std::ifstream;
using std::string;
using std::endl;
using std::cerr;
using std::ios;
using std::vector;

class InputFile
{
private:
	ifstream input;
	vector<string> result;
public:
	InputFile();
	~InputFile();

	void readFile();

	vector<string> getResult();
};

