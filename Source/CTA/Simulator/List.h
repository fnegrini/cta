///////////////////////////////////////////////////////////
//  List.h
//  Implementation of the Class List
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include <stdio.h>

template<class TL>
class List
{

public:
	template<class TE>
	class Element
	{

	public:
		Element() {

		}

		~Element() {

		}

		TE* GetElement(){

			return pElement;
		}

		Element<TE>* GetNext(){

			return pNext;
		}
		Element<TE>* GetPrevious(){

			return pPrevious;
		}
		void SetElement(TE* newVal){

			pElement = newVal;
		}
		void SetNext(Element<TE>* newVal){

			pNext = newVal;
		}
		void SetPrevious(Element<TE>* newVal){

			pPrevious = newVal;
		}

	private:
		TE* pElement;
		Element<TE>* pNext;
		Element<TE>* pPrevious;
	};


	List() {

	}

	virtual ~List() {
		clear();
	}

	Element<TL>* GetCurrent(){

		return pCurrent;
	}
	Element<TL>* GetFirst(){

		return pFirst;
	}
	int GetLength(){

		return length;
	}
	void SetCurrent(Element<TL>* newVal){

		pCurrent = newVal;
	}
	void SetFirst(Element<TL>* newVal){

		pFirst = newVal;
	}

	void push_back(TL* pElement)
	{
		if (NULL != pElement)
		{
			Element<TL>* pAux = new Element<TL>();
			pAux->SetElement(pElement);

			if (NULL == pFirst)
			{
				pFirst = pAux;
				pCurrent = pFirst;
			}
			else
			{
				pAux->SetPrevious(pCurrent);
				pAux->SetNext(NULL);
				pCurrent->SetNext(pAux);
				pCurrent = pCurrent->GetNext();
			}
			length++;
		}
	}

	void push_front(TL* pElement)
	{
		if (NULL != pElement)
		{
			Element<TL>* pAux = new Element<TL>();
			pAux->SetElement(pElement);

			if (NULL == pFirst)
			{
				pFirst = pAux;
				pCurrent = pFirst;
			}
			else
			{
				pAux->SetPrevious(NULL);
				pAux->SetNext(pFirst);
				pFirst->SetPrevious(pAux);
				pFirst = pFirst->GetPrevious();
			}
			length++;
		}
	}

	void clear()
	{
		Element<TL>* pAux = pFirst;
		while (pAux != NULL)
		{
			pAux = pFirst->GetNext();
			delete pFirst;
			pFirst = pAux;
		}
	}

private:
	int length;
	Element<TL>* pCurrent;
	Element<TL>* pFirst;

};
