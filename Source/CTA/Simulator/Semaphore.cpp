///////////////////////////////////////////////////////////
//  Semaphore.cpp
//  Implementation of the Class Semaphore
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Semaphore.h"
#include "Traffic_Light.h"
#include "Intersection.h"
#include "Block.h"
#include "GreenWaveType.h"
#include "GreenWavePropagation.h"

#include <iostream>
using namespace std;


Semaphore::Semaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating){
	id = pSemaphoreId;
	horizontalTrafficLight = pHorizontalTrafficLight;
	verticalTrafficLight = pVerticalTrafficLight;
	trafficFacilitating = pTrafficFacilitating;
	if (trafficFacilitating == 1)
	{
		SetGreenWaveType(Green_Wave_Horizontal);
	}
	else if (trafficFacilitating == 2)
	{
		SetGreenWaveType(Green_Wave_Vertical);
	}
	else
	{
		SetGreenWaveType(No_GreenWave);
	}
	
	SetGreenWavePropagation(No_Propagation);
}

Semaphore::~Semaphore(){

}


int Semaphore::GetCurrentCycleTime(){

	return currentCycleTime;
}


Traffic_Light* Semaphore::GetHorizontalTrafficLight(){

	return horizontalTrafficLight;
}


int Semaphore::GetId(){

	return id;
}


Intersection* Semaphore::GetIntersection(){

	return intersection;
}


Traffic_Light* Semaphore::GetVerticalTrafficLight(){

	return verticalTrafficLight;
}


void Semaphore::SetCurrentCycleTime(int newVal){

	currentCycleTime = newVal;
}


void Semaphore::SetHorizontalTrafficLight(Traffic_Light* newVal){

	horizontalTrafficLight = newVal;
}


void Semaphore::SetId(int newVal){

	id = newVal;
}


void Semaphore::SetIntersection(Intersection* newVal){

	intersection = newVal;
}


void Semaphore::SetVerticalTrafficLight(Traffic_Light* newVal){

	verticalTrafficLight = newVal;
}

void Semaphore::increaseCycleTime(){
	currentCycleTime += 1;
}

void Semaphore::SetSemaphoreState(int state){
	semaphoreState = state;
}

int Semaphore::GetSemaphoreState(){
	return semaphoreState;
}

void Semaphore::SetVerticalVehicleSensorState(int state){
	verticalVehicleSensorState = state;
	//cout << "setVerticalVehicleSensorState : " << state << " : " << currentCycleTime << endl;
}

int Semaphore::GetVerticalVehicleSensorState(){
	return verticalVehicleSensorState;
}

void Semaphore::SetHorizontalVehicleSensorState(int state){
	horizontalVehicleSensorState = state;
	//cout << "setHorizontalVehicleSensorState : " << state << " : " << currentCycleTime << endl;
}

int Semaphore::GetHorizontalVehicleSensorState(){
	return horizontalVehicleSensorState;
}

void Semaphore::SetTrafficFacilitating(int newValue)
{
	trafficFacilitating = newValue;
}

int Semaphore::GetTrafficFacilitating()
{
	return trafficFacilitating;
}

void Semaphore::SetSensorState(Vehicle_Sensor_State newValue)
{
	sensorState = newValue;
}

Vehicle_Sensor_State Semaphore::GetSensorState()
{
	return sensorState;
}

int Semaphore::GetNextSemaphoreCycleTime()
{
	if (intersection != NULL)
	{
		if (trafficFacilitating == 1)
		{
			if (intersection->GetNextHorizontalBlock() != NULL)
			{
				return intersection->GetNextHorizontalBlock()->GetTrafficLight()->GetSemaphore()->GetCurrentCycleTime();
			}
		}
		else if (trafficFacilitating == 2)
		{
			if (intersection->GetNextVerticalBlock() != NULL)
			{
				return intersection->GetNextVerticalBlock()->GetTrafficLight()->GetSemaphore()->GetCurrentCycleTime();
			}
		}
	}
	return currentCycleTime;
}

void Semaphore::operator++(int x)
{
	currentCycleTime += 1;
}

Semaphore* Semaphore::NextHorizontalSemaphore()
{
	if (intersection->GetNextHorizontalBlock() != NULL)
	{
		return intersection->GetNextHorizontalBlock()->GetIntersection()->GetSemaphore();
	}
	else
	{
		return NULL;
	}
}

Semaphore* Semaphore::NextVerticalSemaphore()
{
	if (intersection->GetNextVerticalBlock() != NULL)
	{
		return intersection->GetNextVerticalBlock()->GetIntersection()->GetSemaphore();
	}
	else
	{
		return NULL;
	}
}

Green_Wave_Type Semaphore::GetGreenWaveType()
{
	return GreenWaveType;
}

void Semaphore::SetGreenWaveType(Green_Wave_Type Value)
{
	GreenWaveType = Value;
}

Green_Wave_Propagation Semaphore::GetGreenWavePropagation()
{
	return GreenWavePropagation;
}

void Semaphore::SetGreenWavePropagation(Green_Wave_Propagation Value)
{
	GreenWavePropagation = Value;
}

void Semaphore::PropagateHorizontalGreenWave()
{
	Semaphore* Next_semaphore;

	if (GreenWaveType == Green_Wave_Horizontal)
	{
		Next_semaphore = NextHorizontalSemaphore();
		if (Next_semaphore != NULL)
		{
			Next_semaphore->SetGreenWavePropagation(Horizontal_Propagation);
		}

	}


}

void Semaphore::PropagateVerticalGreenWave()
{
	Semaphore* Next_semaphore;
	if (GreenWaveType == Green_Wave_Vertical)
	{
		// Propagation
		Next_semaphore = NextVerticalSemaphore();
		if (Next_semaphore != NULL)
		{
			Next_semaphore->SetGreenWavePropagation(Vertical_Propagation);
		}

	}
}
