///////////////////////////////////////////////////////////
//  CTA_Simulator.h
//  Implementation of the Class CTA_Simulator
//  Created on:      27-jul-2015 11:02:48
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Street.h"
#include "Logger.h"
#include "../AbstractFactory/AbstractFactory.h"
#include "../Thread.h"
#include "../Controller/Traffic_Lights_Controller.h"
#include "../Interface/Interface.h"
#include "Layout.h"


class CTA_Simulator : public Thread
{

public:
	static CTA_Simulator* getInstance();

	CTA_Simulator();
	~CTA_Simulator();

	int Getsteps();
	int GetStepTimeLength();
	void initialize(Traffic_Lights_Controller* controller, int tSteps, int tStepTimeLength, AbstractFactory* factory, Interface* interfaceCTA);
	void Setsteps(int newVal);
	void SetStepTimeLength(int newVal);
	void simulationStep();
	virtual void run();
	void runStreetSimulationStep(std::list <Street*> *pStreets);

	void log(string message);
	void logEvent(int eventId, int vehicleId);
	void logEvent(int eventId, int vehicleId, string street, int blockId, int lane, int position);

private:
	static CTA_Simulator *instance;
	static int simulationTime;

	list <Street*> horizontalStreets;
	int steps;
	int stepTimeLength;
	list <Street*> verticalStreets;
	Layout layout;

	Logger logger;
	
	Block* buildBlockObject(Street* targetStreet, Street* crossingStreet, int pctOfTurns);
	void initializeBlocksAndIntersections(list <Street*> * horizontalStreets, list <Street*> * verticalStreets, Traffic_Lights_Controller* controller, AbstractFactory* factory);
	void initializeHorizontalStreets(list <Street*> * pStreets);
	void initializeStreets();
	void initializeVerticalStreets(list <Street*> * pStreets);

};

