///////////////////////////////////////////////////////////
//  Block.h
//  Implementation of the Class Block
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Intersection.h"
#include "Traffic_Light.h"
#include "Vehicle.h"
#include "Traffic_Light_State.h"
#include "Vehicle_Block.h"
#include "Vehicle_Sensor.h"


#include <list>
using namespace std;

class Street;

class Block
{

public:
	Block(Street* pStreet, int pBlockId, char* pTrafficLightId, int pLength = 100);
	~Block();
	

	void addVehicle(Vehicle* pVehicle);
	void addVehicle(Vehicle* pVehicle, int position);
	//int GetFreeSpaceA();
	int GetId();
	Intersection* GetIntersection();
	int GetLength();
	int GetMaxCapacity();
	int GetPercentageOfTurns();
	Street* GetStreet();
	Traffic_Light* GetTrafficLight();
	list<Vehicle_Block*>* GetVehicleBlocks();
	Vehicle_Sensor* GetVehicleSensor();
	int GetMaxOccupancySpace();
	void removeVehicle(Vehicle_Block* pVehicleBlock);
	void SetFreeSpace(int newVal);
	void SetId(int newVal);
	void SetIntersection(Intersection* newVal);
	void SetLength(int newVal);
	void SetPercentageOfTurns(int newVal);
	void SetStreet(Street* newVal);
	void SetTrafficLight(Traffic_Light* newVal);
	void SetVehicleSensor(Vehicle_Sensor* newVal);
	void simulationStep();
	bool hasEnoughSpace(int vehicleLength);
	static int blockCounter;
	int GetCalcPctOfTurns();
	void SetCalcPctOfTurns(int newCalcPctOfTurns);

private:
	int freeSpace;
	int id;
	Intersection* intersection;
	int length;
	int maxCapacity;
	int percentageOfTurns;
	Street* street;
	Traffic_Light trafficLight;
	Vehicle_Sensor vehicleSensor;
	int insertAtLane;
	int vehicleCounter;
	list<Vehicle_Block*> vehicleBlocks;
	int calcPctOfTurns;

};

