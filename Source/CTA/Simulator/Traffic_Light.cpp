///////////////////////////////////////////////////////////
//  Traffic_Light.cpp
//  Implementation of the Class Traffic_Light
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Traffic_Light.h"
#include "Semaphore.h"
#include "Block.h"


Traffic_Light::Traffic_Light(Block* pBlock, const string pId){
	block = pBlock;
	id = pId;
	state = RED;
}



Traffic_Light::~Traffic_Light(){

}



string Traffic_Light::GetId(){

	return id;
}


Semaphore* Traffic_Light::GetSemaphore(){

	return semaphore;
}


Traffic_Light_State Traffic_Light::GetState(){

	return state;
}

Block* Traffic_Light::GetBlock(){
	return block;
}

void Traffic_Light::SetId(string newVal){
	id = newVal;
}


void Traffic_Light::SetSemaphore(Semaphore* newVal){

	semaphore = newVal;
}


void Traffic_Light::SetState(Traffic_Light_State newVal){

	state = newVal;
}

void Traffic_Light::SetBlock(Block* newVal){

	block = newVal;
}