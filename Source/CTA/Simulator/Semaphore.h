///////////////////////////////////////////////////////////
//  Semaphore.h
//  Implementation of the Class Semaphore
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once


class Traffic_Light;
class Intersection;
#include "Vehicle_Sensor_State.h"
#include "GreenWaveType.h"
#include "GreenWavePropagation.h"

class Semaphore
{

public:
	Semaphore(int pSemaphoreId, Traffic_Light* pHorizontalTrafficLight, Traffic_Light* pVerticalTrafficLight, const int pTrafficFacilitating = 0);
	virtual ~Semaphore();

	int GetCurrentCycleTime();
	Traffic_Light* GetHorizontalTrafficLight();
	int GetId();
	Intersection* GetIntersection();
	Traffic_Light* GetVerticalTrafficLight();
	void SetCurrentCycleTime(int newVal);
	void SetHorizontalTrafficLight(Traffic_Light* newVal);
	void SetId(int newVal);
	void SetIntersection(Intersection* newVal);
	void SetVerticalTrafficLight(Traffic_Light* newVal);
	virtual void increaseCycleTime();
	void SetTrafficFacilitating(int newValue);
	int GetTrafficFacilitating();
	void SetSensorState(Vehicle_Sensor_State newValue);
	Vehicle_Sensor_State GetSensorState();

	void SetSemaphoreState(int state);
	int GetSemaphoreState();
	virtual void SetVerticalVehicleSensorState(int state);
	int GetVerticalVehicleSensorState();
	virtual void SetHorizontalVehicleSensorState(int state);
	int GetHorizontalVehicleSensorState();
	int GetNextSemaphoreCycleTime();

	void operator++(int x);

	Green_Wave_Type GetGreenWaveType();
	virtual void SetGreenWaveType(Green_Wave_Type Value);

	Green_Wave_Propagation GetGreenWavePropagation();
	virtual void SetGreenWavePropagation(Green_Wave_Propagation Value);

	void PropagateHorizontalGreenWave();
	void PropagateVerticalGreenWave();

protected:
	int currentCycleTime;
	Traffic_Light* horizontalTrafficLight;
	int id;
	Intersection* intersection;
	Traffic_Light* verticalTrafficLight;
	int trafficFacilitating;
	Vehicle_Sensor_State sensorState;


	int semaphoreState;
	int horizontalVehicleSensorState;
	int verticalVehicleSensorState;

	Green_Wave_Type GreenWaveType;
	Green_Wave_Propagation GreenWavePropagation;

	Semaphore* NextHorizontalSemaphore();
	Semaphore* NextVerticalSemaphore();

};
