///////////////////////////////////////////////////////////
//  Block_List.h
//  Implementation of the Class Block_List
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "List.h"
#include "Block.h"
#include "Intersection.h"

class Block_List
{

public:
	Block_List();
	~Block_List();

	void addBlock(Block* pBlock);
	void addBlocksInverted(Block* pBlock);
	void clear();
	void doSimulationStep();
	Block* endBlock();
	Block* firstBlock();
	Block* getBlock(int pos);
	void configHorizontalIntersection();
	void configVerticalIntersection();

private:
	List<Block> listBlock;

};
