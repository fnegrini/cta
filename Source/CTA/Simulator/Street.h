///////////////////////////////////////////////////////////
//  Street.h
//  Implementation of the Class Street
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Block_List.h"
#include "Vehicle.h"
#include "Direction.h"
#include "Block.h"
#include <vector>
using namespace std;
#include <list>
using namespace std;

class Street
{

public:
	Street(const int pNoOfLanes = 2, const float pTrafficIntensity = 0.1, const bool pTrafficFacilitating = false, const string pId = "");
	virtual ~Street();

	void addBlock(Block* pBlock);
	void addVehicleFromEntryPoint();
	void addVehicleToEntryPoint(Vehicle* vehicle);
	void addVehicleToExitPoint(Vehicle* vehicle);
	void clearBlocks();
	void clearEntryPoint();
	void clearExitPoint();
	Direction GetDirection();
	string GetId();
	Block_List GetListBlocks();
	int GetNumberOfLanes();
	float GetTrafficIntensity();
	void removeVehicleFromEntryPoint(Vehicle* vehicle);
	void SetDirection(Direction newVal);
	void SetId(string newVal);
	void SetListBlocks(Block_List newVal);
	void SetNumberOfLanes(int newVal);
	void SetTrafficIntensity(float newVal);
	void simulationStep();
	void configHorizontalIntersection();
	void configVerticalIntersection();
	Block* GetBlock(int pos);
	void SetTrafficFacilitating(bool newValue);
	bool GetTrafficFacilitating();
private:
	Direction direction;
	list<Vehicle*> entryPoint;
	vector<Vehicle*> exitPoint;
	string id;
	Block_List listBlocks;
	int numberOfLanes;
	float trafficIntensity;
	float newVehiclePct;
	bool trafficFacilitating;
};
