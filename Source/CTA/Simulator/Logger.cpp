///////////////////////////////////////////////////////////
//  Logger.cpp
//  Implementation of the Class Logger
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Logger.h"

const int Logger::EV_VEHICLE_ENTER_BLOCK = 1;
const int Logger::EV_VEHICLE_STOP = 2;
const int Logger::EV_VEHICLE_ENTRY_POINT = 3;
const int Logger::EV_VEHICLE_EXIT_POINT = 4;
const int Logger::EV_VEHICLE_MOVE = 5;

Logger::Logger():LOG("simulation.log", ios::out)
{
	if (!(LOG)){
		cerr << "Log file could not be open" << endl;
		fflush(stdin);
		getchar();
	}
}

Logger::~Logger(){

}

void Logger::log(string message){
	if (LOG){
		LOG << message << endl;
	}
}

ofstream* Logger::getLogger(){
	return &LOG;
}

void Logger::operator<<(string msg)
{
	if (LOG){
		LOG << msg << endl;
	}
}