#include "rlCBTF8.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds3.h"
rlCBTF8::rlCBTF8() {
	countsbCBTF8 = 0;
	countImpsbCBTF8 = 0;
}
rlCBTF8::~rlCBTF8() {
}
void rlCBTF8::incCountsbCBTF8(){
	this->countsbCBTF8++;
	//isApproved();
	this->prprCBTFAtSeconds3->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF8::decCountsbCBTF8(){
	this->countsbCBTF8--;
}
void rlCBTF8::incCountImpsbCBTF8(){
	this->countImpsbCBTF8++;
	isApproved();
}
void rlCBTF8::decCountImpsbCBTF8(){
	this->countImpsbCBTF8--;
}
bool rlCBTF8::isApproved() {
	bool ret = false;
	if((countsbCBTF8 + countImpsbCBTF8) == 2) {
		this->semaphore_NOP->mtVTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
