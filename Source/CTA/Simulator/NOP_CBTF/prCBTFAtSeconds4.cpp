#include "prCBTFAtSeconds4.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF4.h"
#include "rlCBTF9.h"

prCBTFAtSeconds4::prCBTFAtSeconds4(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF4 *rlrlCBTF4, rlCBTF9 *rlrlCBTF9) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSecondsState = false;

	this->rlrlCBTF4 = rlrlCBTF4;
	this->rlrlCBTF4->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF9 = rlrlCBTF9;
	this->rlrlCBTF9->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSeconds4::~prCBTFAtSeconds4() {
}
void prCBTFAtSeconds4::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 5) {
		if (prSecondsState == false) {
			this->rlrlCBTF4->incCountImpsbCBTF4();
			this->rlrlCBTF9->incCountImpsbCBTF9();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds4::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 5) {
		if (prSecondsState == false) {
			this->rlrlCBTF4->incCountImpsbCBTF4();
			this->rlrlCBTF9->incCountImpsbCBTF9();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF4->decCountImpsbCBTF4();
			this->rlrlCBTF9->decCountImpsbCBTF9();
			prSecondsState = false;
		}
	}
}