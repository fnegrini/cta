#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF4;
class rlCBTF9;
class prCBTFAtSeconds4 {
public:
	prCBTFAtSeconds4(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF4 *rlrlCBTF4, rlCBTF9 *rlrlCBTF9);
	~prCBTFAtSeconds4(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF4 *rlrlCBTF4;
	rlCBTF9 *rlrlCBTF9;
};
