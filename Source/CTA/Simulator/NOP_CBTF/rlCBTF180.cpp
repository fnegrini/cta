#include "rlCBTF180.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtGWProp1.h"
#include "prCBTFAtGWType1.h"

rlCBTF180::rlCBTF180() {
	countsbCBTF180 = 0;
	countImpsbCBTF180 = 0;
}
rlCBTF180::~rlCBTF180() {
}
void rlCBTF180::incCountsbCBTF180(){
	this->countsbCBTF180++;
	//isApproved();
	this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtGWProp1->notifyprGreenWavePropagationState(this->semaphore_NOP->GetGreenWavePropagation());
	this->prprCBTFAtGWType1->notifyprGreenWaveTypeState(this->semaphore_NOP->GetGreenWaveType());

}
void rlCBTF180::decCountsbCBTF180(){
	this->countsbCBTF180--;
}
void rlCBTF180::incCountImpsbCBTF180(){
	this->countImpsbCBTF180++;
	isApproved();
}
void rlCBTF180::decCountImpsbCBTF180(){
	this->countImpsbCBTF180--;
}
bool rlCBTF180::isApproved() {
	bool ret = false;
	if((countsbCBTF180 + countImpsbCBTF180) == 5) {
		this->semaphore_NOP->mtVTLYCBTF();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
