#include "prCBTFAtVVSS2.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF16.h"
#include "rlCBTF14.h"

prCBTFAtVVSS2::prCBTFAtVVSS2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF16 *rlrlCBTF16, rlCBTF14 *rlrlCBTF14) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prVehicleSensorStateState = false;

	this->rlrlCBTF16 = rlrlCBTF16;
	this->rlrlCBTF16->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF14 = rlrlCBTF14;
	this->rlrlCBTF14->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtVVSS2::~prCBTFAtVVSS2() {
}
void prCBTFAtVVSS2::initprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 2) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF16->incCountImpsbCBTF16();
			this->rlrlCBTF14->incCountImpsbCBTF14();
			prVehicleSensorStateState = true;
		}
	}
}
void prCBTFAtVVSS2::notifyprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 2) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF16->incCountImpsbCBTF16();
			this->rlrlCBTF14->incCountImpsbCBTF14();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBTF16->decCountImpsbCBTF16();
			this->rlrlCBTF14->decCountImpsbCBTF14();
			prVehicleSensorStateState = false;
		}
	}
}