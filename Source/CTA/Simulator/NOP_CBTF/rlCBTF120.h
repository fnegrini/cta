#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds6;
class prCBTFAtGWProp1;
class prCBTFAtGWType1;

class rlCBTF120 {
public:
	rlCBTF120(void);
	~rlCBTF120(void);
	bool isApproved();
	int countsbCBTF120;
	int countImpsbCBTF120;
	void incCountsbCBTF120();
	void decCountsbCBTF120();
	void incCountImpsbCBTF120();
	void decCountImpsbCBTF120();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds6 *prprCBTFAtSeconds6;
	prCBTFAtGWProp1 *prprCBTFAtGWProp1;
	prCBTFAtGWType1 *prprCBTFAtGWType1;
};
