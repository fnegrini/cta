#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF10;
class prCBTFAtSemaphoreState9 {
public:
	prCBTFAtSemaphoreState9(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF10 *rlrlCBTF10);
	~prCBTFAtSemaphoreState9(void);
	bool prSemaphoreStateState;
	void initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF10 *rlrlCBTF10;
};
