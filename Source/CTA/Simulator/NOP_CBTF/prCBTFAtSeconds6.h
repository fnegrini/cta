#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF11;
class rlCBTF12;
class rlCBTF120;
class rlCBTF15;
class rlCBTF16;
class rlCBTF160;

class prCBTFAtSeconds6 {
public:
	prCBTFAtSeconds6(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF11 *rlrlCBTF11, rlCBTF12 *rlrlCBTF12, rlCBTF120 *rlrlCBTF120, rlCBTF15 *rlrlCBTF15, rlCBTF16 *rlrlCBTF16, rlCBTF160 *rlrlCBTF160);
	~prCBTFAtSeconds6(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF11 *rlrlCBTF11;
	rlCBTF12 *rlrlCBTF12;
	rlCBTF120 *rlrlCBTF120;
	rlCBTF15 *rlrlCBTF15;
	rlCBTF16 *rlrlCBTF16;
	rlCBTF160 *rlrlCBTF160;
};
