#include "rlCBTF140.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtGWProp2.h"
#include "prCBTFAtGWType2.h"

rlCBTF140::rlCBTF140() {
	countsbCBTF140 = 0;
	countImpsbCBTF140 = 0;
}
rlCBTF140::~rlCBTF140() {
}
void rlCBTF140::incCountsbCBTF140(){
	this->countsbCBTF140++;
	//isApproved();
	this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtGWType2->notifyprGreenWaveTypeState(this->semaphore_NOP->GetGreenWaveType());
	this->prprCBTFAtGWProp2->notifyprGreenWavePropagationState(this->semaphore_NOP->GetGreenWavePropagation());


}
void rlCBTF140::decCountsbCBTF140(){
	this->countsbCBTF140--;
}
void rlCBTF140::incCountImpsbCBTF140(){
	this->countImpsbCBTF140++;
	isApproved();
}
void rlCBTF140::decCountImpsbCBTF140(){
	this->countImpsbCBTF140--;
}
bool rlCBTF140::isApproved() {
	bool ret = false;
	if((countsbCBTF140 + countImpsbCBTF140) == 5) {
		this->semaphore_NOP->mtHTLYCBTF();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
