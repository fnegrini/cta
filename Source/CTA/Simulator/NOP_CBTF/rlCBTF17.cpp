#include "rlCBTF17.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtHVSS1.h"
rlCBTF17::rlCBTF17() {
	countsbCBTF17 = 0;
	countImpsbCBTF17 = 0;
}
rlCBTF17::~rlCBTF17() {
}
void rlCBTF17::incCountsbCBTF17(){
	this->countsbCBTF17++;
	//isApproved();
	this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtHVSS1->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atHVSS);
}
void rlCBTF17::decCountsbCBTF17(){
	this->countsbCBTF17--;
}
void rlCBTF17::incCountImpsbCBTF17(){
	this->countImpsbCBTF17++;
	isApproved();
}
void rlCBTF17::decCountImpsbCBTF17(){
	this->countImpsbCBTF17--;
}
bool rlCBTF17::isApproved() {
	bool ret = false;
	if((countsbCBTF17 + countImpsbCBTF17) == 4) {
		this->semaphore_NOP->mtVTLYCBTF();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
