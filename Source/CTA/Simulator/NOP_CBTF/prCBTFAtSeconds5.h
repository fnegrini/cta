#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF5;
class rlCBTF10;
class prCBTFAtSeconds5 {
public:
	prCBTFAtSeconds5(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF5 *rlrlCBTF5, rlCBTF10 *rlrlCBTF10);
	~prCBTFAtSeconds5(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF5 *rlrlCBTF5;
	rlCBTF10 *rlrlCBTF10;
};
