#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF2;
class rlCBTF11;
class rlCBTF12;
class rlCBTF120;
class rlCBTF13;
class rlCBTF14;
class rlCBTF140;

class prCBTFAtSemaphoreState0 {
public:
	prCBTFAtSemaphoreState0(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF2 *rlrlCBTF2, rlCBTF11 *rlrlCBTF11, rlCBTF12 *rlrlCBTF12, rlCBTF120 *rlrlCBTF120, rlCBTF13 *rlrlCBTF13, rlCBTF14 *rlrlCBTF14, rlCBTF140 *rlrlCBTF140);
	~prCBTFAtSemaphoreState0(void);
	bool prSemaphoreStateState;
	void initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF2 *rlrlCBTF2;
	rlCBTF11 *rlrlCBTF11;
	rlCBTF12 *rlrlCBTF12;
	rlCBTF120 *rlrlCBTF120;
	rlCBTF13 *rlrlCBTF13;
	rlCBTF14 *rlrlCBTF14;
	rlCBTF140 *rlrlCBTF140;

};
