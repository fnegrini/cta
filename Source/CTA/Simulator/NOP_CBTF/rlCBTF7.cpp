#include "rlCBTF7.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds2.h"
rlCBTF7::rlCBTF7() {
	countsbCBTF7 = 0;
	countImpsbCBTF7 = 0;
}
rlCBTF7::~rlCBTF7() {
}
void rlCBTF7::incCountsbCBTF7(){
	this->countsbCBTF7++;
	//isApproved();
	this->prprCBTFAtSeconds2->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF7::decCountsbCBTF7(){
	this->countsbCBTF7--;
}
void rlCBTF7::incCountImpsbCBTF7(){
	this->countImpsbCBTF7++;
	isApproved();
}
void rlCBTF7::decCountImpsbCBTF7(){
	this->countImpsbCBTF7--;
}
bool rlCBTF7::isApproved() {
	bool ret = false;
	if((countsbCBTF7 + countImpsbCBTF7) == 2) {
		this->semaphore_NOP->mtVTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
