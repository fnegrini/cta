#include "prCBTFAtHVSS1.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF11.h"
#include "rlCBTF17.h"

prCBTFAtHVSS1::prCBTFAtHVSS1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF11 *rlrlCBTF11, rlCBTF17 *rlrlCBTF17) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prVehicleSensorStateState = false;

	this->rlrlCBTF11 = rlrlCBTF11;
	this->rlrlCBTF11->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF17 = rlrlCBTF17;
	this->rlrlCBTF17->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtHVSS1::~prCBTFAtHVSS1() {
}
void prCBTFAtHVSS1::initprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF11->incCountImpsbCBTF11();
			this->rlrlCBTF17->incCountImpsbCBTF17();
			prVehicleSensorStateState = true;
		}
	}
}
void prCBTFAtHVSS1::notifyprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF11->incCountImpsbCBTF11();
			this->rlrlCBTF17->incCountImpsbCBTF17();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBTF11->decCountImpsbCBTF11();
			this->rlrlCBTF17->decCountImpsbCBTF17();
			prVehicleSensorStateState = false;
		}
	}
}