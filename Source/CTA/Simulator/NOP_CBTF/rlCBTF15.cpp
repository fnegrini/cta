#include "rlCBTF15.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtVVSS1.h"
rlCBTF15::rlCBTF15() {
	countsbCBTF15 = 0;
	countImpsbCBTF15 = 0;
}
rlCBTF15::~rlCBTF15() {
}
void rlCBTF15::incCountsbCBTF15(){
	this->countsbCBTF15++;
	//isApproved();
	this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtVVSS1->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atHVSS);
}
void rlCBTF15::decCountsbCBTF15(){
	this->countsbCBTF15--;
}
void rlCBTF15::incCountImpsbCBTF15(){
	this->countImpsbCBTF15++;
	isApproved();
}
void rlCBTF15::decCountImpsbCBTF15(){
	this->countImpsbCBTF15--;
}
bool rlCBTF15::isApproved() {
	bool ret = false;
	if((countsbCBTF15 + countImpsbCBTF15) == 3) {
		this->semaphore_NOP->mtVTLGCBTF();
		this->semaphore_NOP->mtPVGW();
		this->semaphore_NOP->mtRGWPROP();
		ret = true;
	} 
	return ret; 
 }
