#include "rlCBTF11.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtHVSS1.h"
rlCBTF11::rlCBTF11() {
	countsbCBTF11 = 0;
	countImpsbCBTF11 = 0;
}
rlCBTF11::~rlCBTF11() {
}
void rlCBTF11::incCountsbCBTF11(){
	this->countsbCBTF11++;
	//isApproved
	this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtHVSS1->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atVVSS);
}
void rlCBTF11::decCountsbCBTF11(){
	this->countsbCBTF11--;
}
void rlCBTF11::incCountImpsbCBTF11(){
	this->countImpsbCBTF11++;
	isApproved();
}
void rlCBTF11::decCountImpsbCBTF11(){
	this->countImpsbCBTF11--;
}
bool rlCBTF11::isApproved() {
	bool ret = false;
	if((countsbCBTF11 + countImpsbCBTF11) == 3) {
		this->semaphore_NOP->mtHTLGCBTF();
		this->semaphore_NOP->mtPHGW();
		this->semaphore_NOP->mtRGWPROP();

		ret = true;
	} 
	return ret; 
 }
