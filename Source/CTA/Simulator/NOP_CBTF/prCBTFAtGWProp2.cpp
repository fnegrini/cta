#include "prCBTFAtGWProp2.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF140.h"
#include "rlCBTF160.h"

prCBTFAtGWProp2::prCBTFAtGWProp2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF140 *rlrlCBTF140, rlCBTF160 *rlrlCBTF160) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prGreenWavePropagationState = false;

	this->rlrlCBTF140 = rlrlCBTF140;
	this->rlrlCBTF140->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF160 = rlrlCBTF160;
	this->rlrlCBTF160->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtGWProp2::~prCBTFAtGWProp2() {
}
void prCBTFAtGWProp2::initprGreenWavePropagationState(Green_Wave_Propagation Value) {
	if (Value == Vertical_Propagation) {
		if (prGreenWavePropagationState == false) {
			this->rlrlCBTF140->incCountImpsbCBTF140();
			this->rlrlCBTF160->incCountImpsbCBTF160();
			prGreenWavePropagationState = true;
		}
	}
}
void prCBTFAtGWProp2::notifyprGreenWavePropagationState(Green_Wave_Propagation Value) {
	if (Value == Vertical_Propagation) {
		if (prGreenWavePropagationState == false) {
			this->rlrlCBTF140->incCountImpsbCBTF140();
			this->rlrlCBTF160->incCountImpsbCBTF160();
			prGreenWavePropagationState = true;
		}
	}
	else {
		if (prGreenWavePropagationState == true) {
			this->rlrlCBTF140->decCountImpsbCBTF140();
			this->rlrlCBTF160->decCountImpsbCBTF160();
			prGreenWavePropagationState = false;
		}
	}
}