#include "prCBTFAtHVSS2.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF12.h"
#include "rlCBTF18.h"

prCBTFAtHVSS2::prCBTFAtHVSS2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF12 *rlrlCBTF12, rlCBTF18 *rlrlCBTF18) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prVehicleSensorStateState = false;

	this->rlrlCBTF12 = rlrlCBTF12;
	this->rlrlCBTF12->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF18 = rlrlCBTF18;
	this->rlrlCBTF18->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtHVSS2::~prCBTFAtHVSS2() {
}
void prCBTFAtHVSS2::initprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF12->incCountImpsbCBTF12();
			this->rlrlCBTF18->incCountImpsbCBTF18();
			prVehicleSensorStateState = true;
		}
	}
}
void prCBTFAtHVSS2::notifyprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF12->incCountImpsbCBTF12();
			this->rlrlCBTF18->incCountImpsbCBTF18();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBTF12->decCountImpsbCBTF12();
			this->rlrlCBTF18->decCountImpsbCBTF18();
			prVehicleSensorStateState = false;
		}
	}
}