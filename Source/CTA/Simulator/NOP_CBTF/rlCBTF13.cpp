#include "rlCBTF13.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtVVSS1.h"
rlCBTF13::rlCBTF13() {
	countsbCBTF13 = 0;
	countImpsbCBTF13 = 0;
}
rlCBTF13::~rlCBTF13() {
}
void rlCBTF13::incCountsbCBTF13(){
	this->countsbCBTF13++;
	//isApproved();
	this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtVVSS1->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atVVSS);
}
void rlCBTF13::decCountsbCBTF13(){
	this->countsbCBTF13--;
}
void rlCBTF13::incCountImpsbCBTF13(){
	this->countImpsbCBTF13++;
	isApproved();
}
void rlCBTF13::decCountImpsbCBTF13(){
	this->countImpsbCBTF13--;
}
bool rlCBTF13::isApproved() {
	bool ret = false;
	if((countsbCBTF13 + countImpsbCBTF13) == 4) {
		this->semaphore_NOP->mtHTLYCBTF();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
