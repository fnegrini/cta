#include "prCBTFAtSeconds7.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF13.h"
#include "rlCBTF14.h"
#include "rlCBTF140.h"
#include "rlCBTF17.h"
#include "rlCBTF18.h"
#include "rlCBTF180.h"

prCBTFAtSeconds7::prCBTFAtSeconds7(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF13 *rlrlCBTF13, rlCBTF14 *rlrlCBTF14, rlCBTF140 *rlrlCBTF140, rlCBTF17 *rlrlCBTF17, rlCBTF18 *rlrlCBTF18, rlCBTF180 *rlrlCBTF180) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSecondsState = false;

	this->rlrlCBTF13 = rlrlCBTF13;
	this->rlrlCBTF13->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF14 = rlrlCBTF14;
	this->rlrlCBTF14->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF140 = rlrlCBTF140;
	this->rlrlCBTF140->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF17 = rlrlCBTF17;
	this->rlrlCBTF17->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF18 = rlrlCBTF18;
	this->rlrlCBTF18->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF180 = rlrlCBTF180;
	this->rlrlCBTF180->semaphore_NOP = this->semaphore_NOP;

}
prCBTFAtSeconds7::~prCBTFAtSeconds7() {
}
void prCBTFAtSeconds7::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds >= 18) {
		if (prSecondsState == false) {
			this->rlrlCBTF13->incCountImpsbCBTF13();
			this->rlrlCBTF14->incCountImpsbCBTF14();
			this->rlrlCBTF140->incCountImpsbCBTF140();
			this->rlrlCBTF17->incCountImpsbCBTF17();
			this->rlrlCBTF18->incCountImpsbCBTF18();
			this->rlrlCBTF180->incCountImpsbCBTF180();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds7::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds >= 18) {
		if (prSecondsState == false) {
			this->rlrlCBTF13->incCountImpsbCBTF13();
			this->rlrlCBTF14->incCountImpsbCBTF14();
			this->rlrlCBTF140->incCountImpsbCBTF140();
			this->rlrlCBTF17->incCountImpsbCBTF17();
			this->rlrlCBTF18->incCountImpsbCBTF18();
			this->rlrlCBTF180->incCountImpsbCBTF180();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF13->decCountImpsbCBTF13();
			this->rlrlCBTF14->decCountImpsbCBTF14();
			this->rlrlCBTF140->decCountImpsbCBTF140();
			this->rlrlCBTF17->decCountImpsbCBTF17();
			this->rlrlCBTF18->decCountImpsbCBTF18();
			this->rlrlCBTF180->decCountImpsbCBTF180();
			prSecondsState = false;
		}
	}
}