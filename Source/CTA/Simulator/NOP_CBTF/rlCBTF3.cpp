#include "rlCBTF3.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds3.h"
rlCBTF3::rlCBTF3() {
	countsbCBTF3 = 0;
	countImpsbCBTF3 = 0;
}
rlCBTF3::~rlCBTF3() {
}
void rlCBTF3::incCountsbCBTF3(){
	this->countsbCBTF3++;
	//isApproved();
	this->prprCBTFAtSeconds3->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF3::decCountsbCBTF3(){
	this->countsbCBTF3--;
}
void rlCBTF3::incCountImpsbCBTF3(){
	this->countImpsbCBTF3++;
	isApproved();
}
void rlCBTF3::decCountImpsbCBTF3(){
	this->countImpsbCBTF3--;
}
bool rlCBTF3::isApproved() {
	bool ret = false;
	if((countsbCBTF3 + countImpsbCBTF3) == 2) {
		this->semaphore_NOP->mtHTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
