#include "rlCBTF4.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds4.h"
rlCBTF4::rlCBTF4() {
	countsbCBTF4 = 0;
	countImpsbCBTF4 = 0;
}
rlCBTF4::~rlCBTF4() {
}
void rlCBTF4::incCountsbCBTF4(){
	this->countsbCBTF4++;
	//isApproved();
	this->prprCBTFAtSeconds4->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF4::decCountsbCBTF4(){
	this->countsbCBTF4--;
}
void rlCBTF4::incCountImpsbCBTF4(){
	this->countImpsbCBTF4++;
	isApproved();
}
void rlCBTF4::decCountImpsbCBTF4(){
	this->countImpsbCBTF4--;
}
bool rlCBTF4::isApproved() {
	bool ret = false;
	if((countsbCBTF4 + countImpsbCBTF4) == 2) {
		this->semaphore_NOP->mtHTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
