#include "prCBTFAtGWProp1.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF120.h"
#include "rlCBTF180.h"


prCBTFAtGWProp1::prCBTFAtGWProp1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF120 *rlrlCBTF120, rlCBTF180 *rlrlCBTF180) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prGreenWavePropagationState = false;

	this->rlrlCBTF120 = rlrlCBTF120;
	this->rlrlCBTF120->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF180 = rlrlCBTF180;
	this->rlrlCBTF180->semaphore_NOP = this->semaphore_NOP;

}
prCBTFAtGWProp1::~prCBTFAtGWProp1() {
}
void prCBTFAtGWProp1::initprGreenWavePropagationState(Green_Wave_Propagation Value) {
	if (Value == Horizontal_Propagation) {
		if (prGreenWavePropagationState == false) {
			this->rlrlCBTF120->incCountImpsbCBTF120();
			this->rlrlCBTF180->incCountImpsbCBTF180();
			prGreenWavePropagationState = true;
		}
	}
}
void prCBTFAtGWProp1::notifyprGreenWavePropagationState(Green_Wave_Propagation Value) {
	if (Value == Horizontal_Propagation) {
		if (prGreenWavePropagationState == false) {
			this->rlrlCBTF120->incCountImpsbCBTF120();
			this->rlrlCBTF180->incCountImpsbCBTF180();
			prGreenWavePropagationState = true;
		}
	}
	else {
		if (prGreenWavePropagationState == true) {
			this->rlrlCBTF120->decCountImpsbCBTF120();
			this->rlrlCBTF180->decCountImpsbCBTF180();
			prGreenWavePropagationState = false;
		}
	}
}