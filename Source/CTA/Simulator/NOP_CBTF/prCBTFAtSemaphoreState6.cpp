#include "prCBTFAtSemaphoreState6.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF3.h"

prCBTFAtSemaphoreState6::prCBTFAtSemaphoreState6(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF3 *rlrlCBTF3) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF3 = rlrlCBTF3;
	this->rlrlCBTF3->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState6::~prCBTFAtSemaphoreState6() {
}
void prCBTFAtSemaphoreState6::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 6) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF3->incCountsbCBTF3();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState6::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 6) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF3->incCountsbCBTF3();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF3->decCountsbCBTF3();
			prSemaphoreStateState = false;
		}
	}
}