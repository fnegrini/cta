#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds7;
class prCBTFAtSeconds8;
class prCBTFAtHVSS2;
class rlCBTF18 {
public:
	rlCBTF18(void);
	~rlCBTF18(void);
	bool isApproved();
	int countsbCBTF18;
	int countImpsbCBTF18;
	void incCountsbCBTF18();
	void decCountsbCBTF18();
	void incCountImpsbCBTF18();
	void decCountImpsbCBTF18();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds7 *prprCBTFAtSeconds7;
	prCBTFAtSeconds8 *prprCBTFAtSeconds8;
	prCBTFAtHVSS2 *prprCBTFAtHVSS2;
};
