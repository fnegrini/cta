#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF11;
class rlCBTF17;
class prCBTFAtHVSS1 {
public:
	prCBTFAtHVSS1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF11 *rlrlCBTF11, rlCBTF17 *rlrlCBTF17);
	~prCBTFAtHVSS1(void);
	bool prVehicleSensorStateState;
	void initprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS);
	void notifyprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF11 *rlrlCBTF11;
	rlCBTF17 *rlrlCBTF17;
};
