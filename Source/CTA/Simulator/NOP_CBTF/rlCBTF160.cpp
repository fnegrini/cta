#include "rlCBTF160.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtGWProp2.h"
#include "prCBTFAtGWType2.h"

rlCBTF160::rlCBTF160() {
	countsbCBTF160 = 0;
	countImpsbCBTF160 = 0;
}
rlCBTF160::~rlCBTF160() {
}
void rlCBTF160::incCountsbCBTF160(){
	this->countsbCBTF160++;
	//isApproved();
	this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtGWType2->notifyprGreenWaveTypeState(this->semaphore_NOP->GetGreenWaveType());
	this->prprCBTFAtGWProp2->notifyprGreenWavePropagationState(this->semaphore_NOP->GetGreenWavePropagation());

}
void rlCBTF160::decCountsbCBTF160(){
	this->countsbCBTF160--;
}
void rlCBTF160::incCountImpsbCBTF160(){
	this->countImpsbCBTF160++;
	isApproved();
}
void rlCBTF160::decCountImpsbCBTF160(){
	this->countImpsbCBTF160--;
}
bool rlCBTF160::isApproved() {
	bool ret = false;
	if((countsbCBTF160 + countImpsbCBTF160) == 4) {
		this->semaphore_NOP->mtVTLGCBTF();
		this->semaphore_NOP->mtPVGW();
		this->semaphore_NOP->mtRGWPROP();
		ret = true;
	} 
	return ret; 
 }
