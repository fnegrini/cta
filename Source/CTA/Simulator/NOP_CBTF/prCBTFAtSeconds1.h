#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF1;
class rlCBTF6;
class prCBTFAtSeconds1 {
public:
	prCBTFAtSeconds1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF1 *rlrlCBTF1, rlCBTF6 *rlrlCBTF6);
	~prCBTFAtSeconds1(void);	
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);	
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF1 *rlrlCBTF1;
	rlCBTF6 *rlrlCBTF6;
};
