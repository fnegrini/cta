#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds6;
class prCBTFAtVVSS1;
class rlCBTF15 {
public:
	rlCBTF15(void);
	~rlCBTF15(void);
	bool isApproved();
	int countsbCBTF15;
	int countImpsbCBTF15;
	void incCountsbCBTF15();
	void decCountsbCBTF15();
	void incCountImpsbCBTF15();
	void decCountImpsbCBTF15();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds6 *prprCBTFAtSeconds6;
	prCBTFAtVVSS1 *prprCBTFAtVVSS1;
};
