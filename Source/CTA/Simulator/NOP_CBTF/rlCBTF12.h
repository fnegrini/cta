#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds6;
class prCBTFAtHVSS2;
class rlCBTF12 {
public:
	rlCBTF12(void);
	~rlCBTF12(void);
	bool isApproved();
	int countsbCBTF12;
	int countImpsbCBTF12;
	void incCountsbCBTF12();
	void decCountsbCBTF12();
	void incCountImpsbCBTF12();
	void decCountImpsbCBTF12();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds6 *prprCBTFAtSeconds6;
	prCBTFAtHVSS2 *prprCBTFAtHVSS2;
};
