#include "rlCBTF2.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds2.h"
rlCBTF2::rlCBTF2() {
	countsbCBTF2 = 0;
	countImpsbCBTF2 = 0;
}
rlCBTF2::~rlCBTF2() {
}
void rlCBTF2::incCountsbCBTF2(){
	this->countsbCBTF2++;
	//isApproved();
	this->prprCBTFAtSeconds2->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF2::decCountsbCBTF2(){
	this->countsbCBTF2--;
}
void rlCBTF2::incCountImpsbCBTF2(){
	this->countImpsbCBTF2++;
	isApproved();
}
void rlCBTF2::decCountImpsbCBTF2(){
	this->countImpsbCBTF2--;
}
bool rlCBTF2::isApproved() {
	bool ret = false;
	if((countsbCBTF2 + countImpsbCBTF2) == 2) {
		this->semaphore_NOP->mtHTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
