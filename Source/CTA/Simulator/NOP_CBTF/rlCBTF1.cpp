#include "rlCBTF1.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds1.h"
rlCBTF1::rlCBTF1() {
	countsbCBTF1 = 0;
	countImpsbCBTF1 = 0;
}
rlCBTF1::~rlCBTF1() {
}
void rlCBTF1::incCountsbCBTF1(){
	this->countsbCBTF1++;
	//isApproved();
	this->prprCBTFAtSeconds1->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF1::decCountsbCBTF1(){
	this->countsbCBTF1--;
}
void rlCBTF1::incCountImpsbCBTF1(){
	this->countImpsbCBTF1++;
	isApproved();
}
void rlCBTF1::decCountImpsbCBTF1(){
	this->countImpsbCBTF1--;
}
bool rlCBTF1::isApproved() {
	bool ret = false;
	if ((countsbCBTF1 + countImpsbCBTF1) == 2) {
		this->semaphore_NOP->mtHTLG();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
