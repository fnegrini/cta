#include "prCBTFAtSemaphoreState1.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF4.h"

prCBTFAtSemaphoreState1::prCBTFAtSemaphoreState1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF4 *rlrlCBTF4) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF4 = rlrlCBTF4;
	this->rlrlCBTF4->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState1::~prCBTFAtSemaphoreState1() {
}
void prCBTFAtSemaphoreState1::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF4->incCountsbCBTF4();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState1::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF4->incCountsbCBTF4();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF4->decCountsbCBTF4();
			prSemaphoreStateState = false;
		}
	}
}