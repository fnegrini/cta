#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF3;
class rlCBTF8;
class prCBTFAtSeconds3 {
public:
	prCBTFAtSeconds3(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF3 *rlrlCBTF3, rlCBTF8 *rlrlCBTF8);
	~prCBTFAtSeconds3(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF3 *rlrlCBTF3;
	rlCBTF8 *rlrlCBTF8;
};
