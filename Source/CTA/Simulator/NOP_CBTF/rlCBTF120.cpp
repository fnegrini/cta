#include "rlCBTF120.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtGWProp1.h"
#include "prCBTFAtGWType1.h"

rlCBTF120::rlCBTF120() {
	countsbCBTF120 = 0;
	countImpsbCBTF120 = 0;
}
rlCBTF120::~rlCBTF120() {
}
void rlCBTF120::incCountsbCBTF120(){
	this->countsbCBTF120++;
	//isApproved();
	this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtGWProp1->notifyprGreenWavePropagationState(this->semaphore_NOP->GetGreenWavePropagation());
	this->prprCBTFAtGWType1->notifyprGreenWaveTypeState(this->semaphore_NOP->GetGreenWaveType());

}
void rlCBTF120::decCountsbCBTF120(){
	this->countsbCBTF120--;
}
void rlCBTF120::incCountImpsbCBTF120(){
	this->countImpsbCBTF120++;
	isApproved();
}
void rlCBTF120::decCountImpsbCBTF120(){
	this->countImpsbCBTF120--;
}
bool rlCBTF120::isApproved() {
	bool ret = false;
	if((countsbCBTF120 + countImpsbCBTF120) == 4) {
		this->semaphore_NOP->mtHTLGCBTF();
		this->semaphore_NOP->mtPHGW();
		this->semaphore_NOP->mtRGWPROP();
		ret = true;
	} 
	return ret; 
 }
