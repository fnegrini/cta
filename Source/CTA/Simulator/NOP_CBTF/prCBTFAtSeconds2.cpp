#include "prCBTFAtSeconds2.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF2.h"
#include "rlCBTF7.h"

prCBTFAtSeconds2::prCBTFAtSeconds2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF2 *rlrlCBTF2, rlCBTF7 *rlrlCBTF7) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSecondsState = false;

	this->rlrlCBTF2 = rlrlCBTF2;
	this->rlrlCBTF2->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF7 = rlrlCBTF7;
	this->rlrlCBTF7->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSeconds2::~prCBTFAtSeconds2() {
}
void prCBTFAtSeconds2::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 38) {
		if (prSecondsState == false) {
			this->rlrlCBTF2->incCountImpsbCBTF2();
			this->rlrlCBTF7->incCountImpsbCBTF7();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds2::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 38) {
		if (prSecondsState == false) {
			this->rlrlCBTF2->incCountImpsbCBTF2();
			this->rlrlCBTF7->incCountImpsbCBTF7();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF2->decCountImpsbCBTF2();
			this->rlrlCBTF7->decCountImpsbCBTF7();
			prSecondsState = false;
		}
	}
}