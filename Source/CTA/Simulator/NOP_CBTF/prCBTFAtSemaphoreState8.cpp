#include "prCBTFAtSemaphoreState8.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF8.h"

prCBTFAtSemaphoreState8::prCBTFAtSemaphoreState8(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF8 *rlrlCBTF8) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF8 = rlrlCBTF8;
	this->rlrlCBTF8->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState8::~prCBTFAtSemaphoreState8() {
}
void prCBTFAtSemaphoreState8::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF8->incCountsbCBTF8();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState8::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF8->incCountsbCBTF8();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF8->decCountsbCBTF8();
			prSemaphoreStateState = false;
		}
	}
}