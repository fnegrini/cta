#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds6;
class prCBTFAtGWProp2;
class prCBTFAtGWType2;

class rlCBTF160 {
public:
	rlCBTF160(void);
	~rlCBTF160(void);
	bool isApproved();
	int countsbCBTF160;
	int countImpsbCBTF160;
	void incCountsbCBTF160();
	void decCountsbCBTF160();
	void incCountImpsbCBTF160();
	void decCountImpsbCBTF160();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds6 *prprCBTFAtSeconds6;
	prCBTFAtGWProp2 *prprCBTFAtGWProp2;
	prCBTFAtGWType2	*prprCBTFAtGWType2;
};
