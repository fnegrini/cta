#include "prCBTFAtSeconds3.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF3.h"
#include "rlCBTF8.h"

prCBTFAtSeconds3::prCBTFAtSeconds3(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF3 *rlrlCBTF3, rlCBTF8 *rlrlCBTF8) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSecondsState = false;

	this->rlrlCBTF3 = rlrlCBTF3;
	this->rlrlCBTF3->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF8 = rlrlCBTF8;
	this->rlrlCBTF8->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSeconds3::~prCBTFAtSeconds3() {
}
void prCBTFAtSeconds3::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 30) {
		if (prSecondsState == false) {
			this->rlrlCBTF3->incCountImpsbCBTF3();
			this->rlrlCBTF8->incCountImpsbCBTF8();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds3::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 30) {
		if (prSecondsState == false) {
			this->rlrlCBTF3->incCountImpsbCBTF3();
			this->rlrlCBTF8->incCountImpsbCBTF8();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF3->decCountImpsbCBTF3();
			this->rlrlCBTF8->decCountImpsbCBTF8();
			prSecondsState = false;
		}
	}
}