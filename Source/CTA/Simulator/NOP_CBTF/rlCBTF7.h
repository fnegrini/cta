#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds2;
class rlCBTF7 {
public:
	rlCBTF7(void);
	~rlCBTF7(void);
	bool isApproved();	
	int countsbCBTF7;
	int countImpsbCBTF7;
	void incCountsbCBTF7();
	void decCountsbCBTF7();
	void incCountImpsbCBTF7();
	void decCountImpsbCBTF7();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds2 *prprCBTFAtSeconds2;
};
