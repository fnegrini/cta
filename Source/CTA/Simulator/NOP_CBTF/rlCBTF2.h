#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds2;
class rlCBTF2 {
public:
	rlCBTF2(void);
	~rlCBTF2(void);
	bool isApproved();	
	int countsbCBTF2;
	int countImpsbCBTF2;
	void incCountsbCBTF2();
	void decCountsbCBTF2();
	void incCountImpsbCBTF2();
	void decCountImpsbCBTF2();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds2 *prprCBTFAtSeconds2;
};
