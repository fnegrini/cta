#pragma once
#include <iostream>
#include "../GreenWavePropagation.h"

using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF140;
class rlCBTF160;

class prCBTFAtGWProp2 {
public:
	prCBTFAtGWProp2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF140 *rlrlCBTF140, rlCBTF160 *rlrlCBTF160);
	~prCBTFAtGWProp2(void);
	bool prGreenWavePropagationState;
	void initprGreenWavePropagationState(Green_Wave_Propagation Value);
	void notifyprGreenWavePropagationState(Green_Wave_Propagation Value);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF140 *rlrlCBTF140;
	rlCBTF160 *rlrlCBTF160;
};
