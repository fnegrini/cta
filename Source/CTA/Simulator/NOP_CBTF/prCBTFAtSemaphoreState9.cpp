#include "prCBTFAtSemaphoreState9.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF10.h"

prCBTFAtSemaphoreState9::prCBTFAtSemaphoreState9(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF10 *rlrlCBTF10) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF10 = rlrlCBTF10;
	this->rlrlCBTF10->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState9::~prCBTFAtSemaphoreState9() {
}
void prCBTFAtSemaphoreState9::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 9) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF10->incCountsbCBTF10();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState9::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 9) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF10->incCountsbCBTF10();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF10->decCountsbCBTF10();
			prSemaphoreStateState = false;
		}
	}
}