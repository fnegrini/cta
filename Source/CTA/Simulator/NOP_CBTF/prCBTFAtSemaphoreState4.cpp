#include "prCBTFAtSemaphoreState4.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF9.h"

prCBTFAtSemaphoreState4::prCBTFAtSemaphoreState4(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF9 *rlrlCBTF9) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF9 = rlrlCBTF9;
	this->rlrlCBTF9->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState4::~prCBTFAtSemaphoreState4() {
}
void prCBTFAtSemaphoreState4::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 4) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF9->incCountsbCBTF9();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState4::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 4) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF9->incCountsbCBTF9();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF9->decCountsbCBTF9();
			prSemaphoreStateState = false;
		}
	}
}