#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds7;
class prCBTFAtSeconds8;
class prCBTFAtVVSS1;
class rlCBTF13 {
public:
	rlCBTF13(void);
	~rlCBTF13(void);
	bool isApproved();
	int countsbCBTF13;
	int countImpsbCBTF13;
	void incCountsbCBTF13();
	void decCountsbCBTF13();
	void incCountImpsbCBTF13();
	void decCountImpsbCBTF13();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds7 *prprCBTFAtSeconds7;
	prCBTFAtSeconds8 *prprCBTFAtSeconds8;
	prCBTFAtVVSS1 *prprCBTFAtVVSS1;
};
