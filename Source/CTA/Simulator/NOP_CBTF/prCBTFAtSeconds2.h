#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF2;
class rlCBTF7;
class prCBTFAtSeconds2 {
public:
	prCBTFAtSeconds2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF2 *rlrlCBTF2, rlCBTF7 *rlrlCBTF7);
	~prCBTFAtSeconds2(void);	
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF2 *rlrlCBTF2;
	rlCBTF7 *rlrlCBTF7;
};
