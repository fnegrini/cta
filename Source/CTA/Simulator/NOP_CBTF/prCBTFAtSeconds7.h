#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF13;
class rlCBTF14;
class rlCBTF140;
class rlCBTF17;
class rlCBTF18;
class rlCBTF180;

class prCBTFAtSeconds7 {
public:
	prCBTFAtSeconds7(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF13 *rlrlCBTF13, rlCBTF14 *rlrlCBTF14, rlCBTF140 *rlrlCBTF140, rlCBTF17 *rlrlCBTF17, rlCBTF18 *rlrlCBTF18, rlCBTF180 *rlrlCBTF180);
	~prCBTFAtSeconds7(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF13 *rlrlCBTF13;
	rlCBTF14 *rlrlCBTF14;
	rlCBTF140 *rlrlCBTF140;
	rlCBTF17 *rlrlCBTF17;
	rlCBTF18 *rlrlCBTF18;
	rlCBTF180 *rlrlCBTF180;
};
