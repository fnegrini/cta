#include "prCBTFAtSeconds1.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF1.h"
#include "rlCBTF6.h"

prCBTFAtSeconds1::prCBTFAtSeconds1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF1 *rlrlCBTF1, rlCBTF6 *rlrlCBTF6) {
	this->semaphore_NOP = semaphore_NOP_CBTF;
	
	prSecondsState = false;

	this->rlrlCBTF1 = rlrlCBTF1;
	this->rlrlCBTF1->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF6 = rlrlCBTF6;
	this->rlrlCBTF6->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSeconds1::~prCBTFAtSeconds1() {
}
void prCBTFAtSeconds1::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 2) {
		if (prSecondsState == false) {
			this->rlrlCBTF1->incCountImpsbCBTF1();
			this->rlrlCBTF6->incCountImpsbCBTF6();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds1::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 2) {
		if (prSecondsState == false) {
			this->rlrlCBTF1->incCountImpsbCBTF1();
			this->rlrlCBTF6->incCountImpsbCBTF6();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF1->decCountImpsbCBTF1();
			this->rlrlCBTF6->decCountImpsbCBTF6();
			prSecondsState = false;
		}
	}
}