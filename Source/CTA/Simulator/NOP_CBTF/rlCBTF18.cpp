#include "rlCBTF18.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtHVSS2.h"
rlCBTF18::rlCBTF18() {
	countsbCBTF18 = 0;
	countImpsbCBTF18 = 0;
}
rlCBTF18::~rlCBTF18() {
}
void rlCBTF18::incCountsbCBTF18(){
	this->countsbCBTF18++;
	//isApproved();
	this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtHVSS2->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atHVSS);
}
void rlCBTF18::decCountsbCBTF18(){
	this->countsbCBTF18--;
}
void rlCBTF18::incCountImpsbCBTF18(){
	this->countImpsbCBTF18++;
	isApproved();
}
void rlCBTF18::decCountImpsbCBTF18(){
	this->countImpsbCBTF18--;
}
bool rlCBTF18::isApproved() {
	bool ret = false;
	if((countsbCBTF18 + countImpsbCBTF18) == 4) {
		this->semaphore_NOP->mtVTLYCBTF();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
