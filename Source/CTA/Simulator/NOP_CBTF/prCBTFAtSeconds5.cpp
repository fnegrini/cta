#include "prCBTFAtSeconds5.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF5.h"
#include "rlCBTF10.h"

prCBTFAtSeconds5::prCBTFAtSeconds5(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF5 *rlrlCBTF5, rlCBTF10 *rlrlCBTF10) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSecondsState = false;

	this->rlrlCBTF5 = rlrlCBTF5;
	this->rlrlCBTF5->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF10 = rlrlCBTF10;
	this->rlrlCBTF10->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSeconds5::~prCBTFAtSeconds5() {
}
void prCBTFAtSeconds5::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 6) {
		if (prSecondsState == false) {
			this->rlrlCBTF5->incCountImpsbCBTF5();
			this->rlrlCBTF10->incCountImpsbCBTF10();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds5::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 6) {
		if (prSecondsState == false) {
			this->rlrlCBTF5->incCountImpsbCBTF5();
			this->rlrlCBTF10->incCountImpsbCBTF10();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF5->decCountImpsbCBTF5();
			this->rlrlCBTF10->decCountImpsbCBTF10();
			prSecondsState = false;
		}
	}
}