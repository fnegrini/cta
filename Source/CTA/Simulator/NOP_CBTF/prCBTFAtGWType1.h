#pragma once
#include <iostream>
#include "../GreenWaveType.h"

using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF120;
class rlCBTF180;

class prCBTFAtGWType1 {
public:
	prCBTFAtGWType1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF120 *rlrlCBTF120, rlCBTF180 *rlrlCBTF180);
	~prCBTFAtGWType1(void);
	bool prGreenWaveTypeState;
	void initprGreenWaveTypeState(Green_Wave_Type Value);
	void notifyprGreenWaveTypeState(Green_Wave_Type Value);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF120 *rlrlCBTF120;
	rlCBTF180 *rlrlCBTF180;
};
