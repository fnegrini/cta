#include "prCBTFAtSemaphoreState3.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF7.h"
#include "rlCBTF15.h"
#include "rlCBTF16.h"
#include "rlCBTF160.h"
#include "rlCBTF17.h"
#include "rlCBTF18.h"
#include "rlCBTF180.h"

prCBTFAtSemaphoreState3::prCBTFAtSemaphoreState3(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF7 *rlrlCBTF7, rlCBTF15 *rlrlCBTF15, rlCBTF16 *rlrlCBTF16, rlCBTF160 *rlrlCBTF160, rlCBTF17 *rlrlCBTF17, rlCBTF18 *rlrlCBTF18, rlCBTF180 *rlrlCBTF180) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF7 = rlrlCBTF7;
	this->rlrlCBTF7->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF15 = rlrlCBTF15;
	this->rlrlCBTF15->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF16 = rlrlCBTF16;
	this->rlrlCBTF16->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF160 = rlrlCBTF160;
	this->rlrlCBTF160->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF17 = rlrlCBTF17;
	this->rlrlCBTF17->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF18 = rlrlCBTF18;
	this->rlrlCBTF18->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF180 = rlrlCBTF180;
	this->rlrlCBTF180->semaphore_NOP = this->semaphore_NOP;

}
prCBTFAtSemaphoreState3::~prCBTFAtSemaphoreState3() {
}
void prCBTFAtSemaphoreState3::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 3) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF7->incCountsbCBTF7();
			this->rlrlCBTF15->incCountsbCBTF15();
			this->rlrlCBTF16->incCountsbCBTF16();
			this->rlrlCBTF160->incCountsbCBTF160();
			this->rlrlCBTF17->incCountsbCBTF17();
			this->rlrlCBTF18->incCountsbCBTF18();
			this->rlrlCBTF180->incCountsbCBTF180();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState3::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 3) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF7->incCountsbCBTF7();
			this->rlrlCBTF15->incCountsbCBTF15();
			this->rlrlCBTF16->incCountsbCBTF16();
			this->rlrlCBTF160->incCountsbCBTF160();
			this->rlrlCBTF17->incCountsbCBTF17();
			this->rlrlCBTF18->incCountsbCBTF18();
			this->rlrlCBTF180->incCountsbCBTF180();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF7->decCountsbCBTF7();
			this->rlrlCBTF15->decCountsbCBTF15();
			this->rlrlCBTF16->decCountsbCBTF16();
			this->rlrlCBTF160->decCountsbCBTF160();
			this->rlrlCBTF17->decCountsbCBTF17();
			this->rlrlCBTF18->decCountsbCBTF18();
			this->rlrlCBTF180->decCountsbCBTF180();
			prSemaphoreStateState = false;
		}
	}
}