#pragma once
#include <iostream>
#include "../GreenWavePropagation.h"

using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF120;
class rlCBTF180;

class prCBTFAtGWProp1 {
public:
	prCBTFAtGWProp1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF120 *rlrlCBTF120, rlCBTF180 *rlrlCBTF180);
	~prCBTFAtGWProp1(void);
	bool prGreenWavePropagationState;
	void initprGreenWavePropagationState(Green_Wave_Propagation Value);
	void notifyprGreenWavePropagationState(Green_Wave_Propagation Value);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF120 *rlrlCBTF120;
	rlCBTF180 *rlrlCBTF180;

};
