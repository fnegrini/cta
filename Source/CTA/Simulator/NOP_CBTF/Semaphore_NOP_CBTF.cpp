#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds1.h"
#include "prCBTFAtSeconds2.h"
#include "prCBTFAtSeconds3.h"
#include "prCBTFAtSeconds4.h"
#include "prCBTFAtSeconds5.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtSemaphoreState0.h"
#include "prCBTFAtSemaphoreState1.h"
#include "prCBTFAtSemaphoreState2.h"
#include "prCBTFAtSemaphoreState3.h"
#include "prCBTFAtSemaphoreState4.h"
#include "prCBTFAtSemaphoreState5.h"
#include "prCBTFAtSemaphoreState6.h"
#include "prCBTFAtSemaphoreState7.h"
#include "prCBTFAtSemaphoreState8.h"
#include "prCBTFAtSemaphoreState9.h"
#include "prCBTFAtHVSS1.h"
#include "prCBTFAtHVSS2.h"
#include "prCBTFAtVVSS1.h"
#include "prCBTFAtVVSS2.h"
#include "prCBTFAtVVSS2.h"
#include "prCBTFAtGWProp1.h"
#include "prCBTFAtGWProp2.h"
#include "prCBTFAtGWType1.h"
#include "prCBTFAtGWType2.h"
#include "rlCBTF1.h"
#include "rlCBTF2.h"
#include "rlCBTF3.h"
#include "rlCBTF4.h"
#include "rlCBTF5.h"
#include "rlCBTF6.h"
#include "rlCBTF7.h"
#include "rlCBTF8.h"
#include "rlCBTF9.h"
#include "rlCBTF10.h"
#include "rlCBTF11.h"
#include "rlCBTF12.h"
#include "rlCBTF120.h"
#include "rlCBTF13.h"
#include "rlCBTF14.h"
#include "rlCBTF140.h"
#include "rlCBTF15.h"
#include "rlCBTF16.h"
#include "rlCBTF160.h"
#include "rlCBTF17.h"
#include "rlCBTF18.h"
#include "rlCBTF180.h"
#include "../Traffic_Light.h"
Semaphore_NOP_CBTF::Semaphore_NOP_CBTF(int semaphoreId, Traffic_Light* horizontalTrafficLight, Traffic_Light* verticalTrafficLight, const int pTrafficFacilitating) :
Semaphore(semaphoreId, horizontalTrafficLight, verticalTrafficLight, pTrafficFacilitating)
{
	this->rlrlCBTF1 = new rlCBTF1();
	this->rlrlCBTF10 = new rlCBTF10();
	this->rlrlCBTF11 = new rlCBTF11();
	this->rlrlCBTF12 = new rlCBTF12();
	this->rlrlCBTF120 = new rlCBTF120();
	this->rlrlCBTF13 = new rlCBTF13();
	this->rlrlCBTF14 = new rlCBTF14();
	this->rlrlCBTF140 = new rlCBTF140();
	this->rlrlCBTF15 = new rlCBTF15();
	this->rlrlCBTF16 = new rlCBTF16();
	this->rlrlCBTF160 = new rlCBTF160();
	this->rlrlCBTF17 = new rlCBTF17();
	this->rlrlCBTF18 = new rlCBTF18();
	this->rlrlCBTF180 = new rlCBTF180();
	this->rlrlCBTF2 = new rlCBTF2();
	this->rlrlCBTF3 = new rlCBTF3();
	this->rlrlCBTF4 = new rlCBTF4();
	this->rlrlCBTF5 = new rlCBTF5();
	this->rlrlCBTF6 = new rlCBTF6();
	this->rlrlCBTF7 = new rlCBTF7();
	this->rlrlCBTF8 = new rlCBTF8();
	this->rlrlCBTF9 = new rlCBTF9();

	this->prprCBTFAtSeconds1 = new prCBTFAtSeconds1(this, rlrlCBTF1, rlrlCBTF6);
	this->prprCBTFAtSeconds2 = new prCBTFAtSeconds2(this, rlrlCBTF2, rlrlCBTF7);
	this->prprCBTFAtSeconds3 = new prCBTFAtSeconds3(this, rlrlCBTF3, rlrlCBTF8);
	this->prprCBTFAtSeconds4 = new prCBTFAtSeconds4(this, rlrlCBTF4, rlrlCBTF9);
	this->prprCBTFAtSeconds5 = new prCBTFAtSeconds5(this, rlrlCBTF5, rlrlCBTF10);
	this->prprCBTFAtSeconds6 = new prCBTFAtSeconds6(this, rlrlCBTF11, rlrlCBTF12, rlrlCBTF120, rlrlCBTF15, rlrlCBTF16, rlrlCBTF160);
	this->prprCBTFAtSeconds7 = new prCBTFAtSeconds7(this, rlrlCBTF13, rlrlCBTF14, rlrlCBTF140, rlrlCBTF17, rlrlCBTF18, rlrlCBTF180);
	this->prprCBTFAtSeconds8 = new prCBTFAtSeconds8(this, rlrlCBTF13, rlrlCBTF14, rlrlCBTF140, rlrlCBTF17, rlrlCBTF18, rlrlCBTF180);

	this->prprCBTFAtSemaphoreState0 = new prCBTFAtSemaphoreState0(this, rlrlCBTF2, rlrlCBTF11, rlrlCBTF12, rlrlCBTF120, rlrlCBTF13, rlrlCBTF14, rlrlCBTF140);
	this->prprCBTFAtSemaphoreState1 = new prCBTFAtSemaphoreState1(this, rlrlCBTF4);
	this->prprCBTFAtSemaphoreState2 = new prCBTFAtSemaphoreState2(this, rlrlCBTF6);
	this->prprCBTFAtSemaphoreState3 = new prCBTFAtSemaphoreState3(this, rlrlCBTF7, rlrlCBTF15, rlrlCBTF16, rlrlCBTF160, rlrlCBTF17, rlrlCBTF18, rlrlCBTF180);
	this->prprCBTFAtSemaphoreState4 = new prCBTFAtSemaphoreState4(this, rlrlCBTF9);
	this->prprCBTFAtSemaphoreState5 = new prCBTFAtSemaphoreState5(this, rlrlCBTF1);
	this->prprCBTFAtSemaphoreState6 = new prCBTFAtSemaphoreState6(this, rlrlCBTF3);
	this->prprCBTFAtSemaphoreState7 = new prCBTFAtSemaphoreState7(this, rlrlCBTF5);
	this->prprCBTFAtSemaphoreState8 = new prCBTFAtSemaphoreState8(this, rlrlCBTF8);
	this->prprCBTFAtSemaphoreState9 = new prCBTFAtSemaphoreState9(this, rlrlCBTF10);

	this->prprCBTFAtHVSS1 = new prCBTFAtHVSS1(this, rlrlCBTF11, rlrlCBTF17);
	this->prprCBTFAtHVSS2 = new prCBTFAtHVSS2(this, rlrlCBTF12, rlrlCBTF18);
	this->prprCBTFAtVVSS1 = new prCBTFAtVVSS1(this, rlrlCBTF15, rlrlCBTF13);
	this->prprCBTFAtVVSS2 = new prCBTFAtVVSS2(this, rlrlCBTF16, rlrlCBTF14);

	this->ptprCBTFAtGWProp1 = new prCBTFAtGWProp1(this, rlrlCBTF120, rlrlCBTF180);
	this->ptprCBTFAtGWType1 = new prCBTFAtGWType1(this, rlrlCBTF120, rlrlCBTF180);

	this->ptprCBTFAtGWProp2 = new prCBTFAtGWProp2(this, rlrlCBTF140, rlrlCBTF160);
	this->ptprCBTFAtGWType2 = new prCBTFAtGWType2(this, rlrlCBTF140, rlrlCBTF160);

	this->rlrlCBTF1->prprCBTFAtSeconds1 = this->prprCBTFAtSeconds1;
	this->rlrlCBTF2->prprCBTFAtSeconds2 = this->prprCBTFAtSeconds2;
	this->rlrlCBTF3->prprCBTFAtSeconds3 = this->prprCBTFAtSeconds3;
	this->rlrlCBTF4->prprCBTFAtSeconds4 = this->prprCBTFAtSeconds4;
	this->rlrlCBTF5->prprCBTFAtSeconds5 = this->prprCBTFAtSeconds5;
	this->rlrlCBTF6->prprCBTFAtSeconds1 = this->prprCBTFAtSeconds1;
	this->rlrlCBTF7->prprCBTFAtSeconds2 = this->prprCBTFAtSeconds2;
	this->rlrlCBTF8->prprCBTFAtSeconds3 = this->prprCBTFAtSeconds3;
	this->rlrlCBTF9->prprCBTFAtSeconds4 = this->prprCBTFAtSeconds4;
	this->rlrlCBTF10->prprCBTFAtSeconds5 = this->prprCBTFAtSeconds5;
	this->rlrlCBTF11->prprCBTFAtSeconds6 = this->prprCBTFAtSeconds6;
	this->rlrlCBTF12->prprCBTFAtSeconds6 = this->prprCBTFAtSeconds6;
	this->rlrlCBTF120->prprCBTFAtSeconds6 = this->prprCBTFAtSeconds6;
	this->rlrlCBTF13->prprCBTFAtSeconds7 = this->prprCBTFAtSeconds7;
	this->rlrlCBTF13->prprCBTFAtSeconds8 = this->prprCBTFAtSeconds8;
	this->rlrlCBTF14->prprCBTFAtSeconds7 = this->prprCBTFAtSeconds7;
	this->rlrlCBTF140->prprCBTFAtSeconds7 = this->prprCBTFAtSeconds7;
	this->rlrlCBTF14->prprCBTFAtSeconds8 = this->prprCBTFAtSeconds8;
	this->rlrlCBTF140->prprCBTFAtSeconds8 = this->prprCBTFAtSeconds8;
	this->rlrlCBTF15->prprCBTFAtSeconds6 = this->prprCBTFAtSeconds6;
	this->rlrlCBTF16->prprCBTFAtSeconds6 = this->prprCBTFAtSeconds6;
	this->rlrlCBTF160->prprCBTFAtSeconds6 = this->prprCBTFAtSeconds6;
	this->rlrlCBTF17->prprCBTFAtSeconds7 = this->prprCBTFAtSeconds7;
	this->rlrlCBTF17->prprCBTFAtSeconds8 = this->prprCBTFAtSeconds8;
	this->rlrlCBTF18->prprCBTFAtSeconds7 = this->prprCBTFAtSeconds7;
	this->rlrlCBTF180->prprCBTFAtSeconds7 = this->prprCBTFAtSeconds7;
	this->rlrlCBTF18->prprCBTFAtSeconds8 = this->prprCBTFAtSeconds8;
	this->rlrlCBTF180->prprCBTFAtSeconds8 = this->prprCBTFAtSeconds8;

	this->rlrlCBTF11->prprCBTFAtHVSS1 = this->prprCBTFAtHVSS1;
	this->rlrlCBTF12->prprCBTFAtHVSS2 = this->prprCBTFAtHVSS2;
	this->rlrlCBTF13->prprCBTFAtVVSS1 = this->prprCBTFAtVVSS1;
	this->rlrlCBTF14->prprCBTFAtVVSS2 = this->prprCBTFAtVVSS2;
	this->rlrlCBTF15->prprCBTFAtVVSS1 = this->prprCBTFAtVVSS1;
	this->rlrlCBTF16->prprCBTFAtVVSS2 = this->prprCBTFAtVVSS2;
	this->rlrlCBTF17->prprCBTFAtHVSS1 = this->prprCBTFAtHVSS1;
	this->rlrlCBTF18->prprCBTFAtHVSS2 = this->prprCBTFAtHVSS2;

	this->rlrlCBTF120->prprCBTFAtGWProp1 = this->ptprCBTFAtGWProp1;
	this->rlrlCBTF180->prprCBTFAtGWProp1 = this->ptprCBTFAtGWProp1;

	this->rlrlCBTF120->prprCBTFAtGWType1 = this->ptprCBTFAtGWType1;
	this->rlrlCBTF180->prprCBTFAtGWType1 = this->ptprCBTFAtGWType1;

	this->rlrlCBTF140->prprCBTFAtGWProp2 = this->ptprCBTFAtGWProp2;
	this->rlrlCBTF160->prprCBTFAtGWProp2 = this->ptprCBTFAtGWProp2;

	this->rlrlCBTF140->prprCBTFAtGWType2 = this->ptprCBTFAtGWType2;
	this->rlrlCBTF160->prprCBTFAtGWType2 = this->ptprCBTFAtGWType2;

	atSeconds = 0;
	atSemaphoreState = 5;
	atHVSS = 0;
	atVVSS = 0;

	this->prprCBTFAtSeconds1->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds2->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds3->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds4->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds5->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds6->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds7->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprCBTFAtSeconds8->initprSecondssemaphore_NOPatSeconds(this->atSeconds);

	this->prprCBTFAtSemaphoreState0->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState1->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState2->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState3->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState4->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState5->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState6->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState7->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState8->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprCBTFAtSemaphoreState9->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);

	this->prprCBTFAtHVSS1->initprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);
	this->prprCBTFAtHVSS2->initprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);

	this->prprCBTFAtVVSS1->initprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
	this->prprCBTFAtVVSS2->initprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
}
Semaphore_NOP_CBTF::~Semaphore_NOP_CBTF(void)
{
}
void Semaphore_NOP_CBTF::setatSeconds(int atSeconds) {
	if (this->atSeconds != atSeconds) {
		this->atSeconds = atSeconds;
		if (this->rlrlCBTF1->countsbCBTF1 == 1 || this->rlrlCBTF6->countsbCBTF6 == 1){
			this->prprCBTFAtSeconds1->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF2->countsbCBTF2 == 1 || this->rlrlCBTF7->countsbCBTF7 == 1){
			this->prprCBTFAtSeconds2->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF3->countsbCBTF3 == 1 || this->rlrlCBTF8->countsbCBTF8 == 1){
			this->prprCBTFAtSeconds3->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF4->countsbCBTF4 == 1 || this->rlrlCBTF9->countsbCBTF9 == 1){
			this->prprCBTFAtSeconds4->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF5->countsbCBTF5 == 1 || this->rlrlCBTF10->countsbCBTF10 == 1){
			this->prprCBTFAtSeconds5->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF11->countsbCBTF11 == 1 || this->rlrlCBTF12->countsbCBTF12 == 1 || this->rlrlCBTF120->countsbCBTF120 == 1
			|| this->rlrlCBTF15->countsbCBTF15 == 1 || this->rlrlCBTF16->countsbCBTF16 == 1 || this->rlrlCBTF160->countsbCBTF160 == 1){
			this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF13->countsbCBTF13 == 1 || this->rlrlCBTF14->countsbCBTF14 == 1 || this->rlrlCBTF140->countsbCBTF140 == 1
			|| this->rlrlCBTF17->countsbCBTF17 == 1 || this->rlrlCBTF18->countsbCBTF18 == 1 || this->rlrlCBTF180->countsbCBTF180 == 1){
			this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBTF13->countsbCBTF13 == 1 || this->rlrlCBTF14->countsbCBTF14 == 1 || this->rlrlCBTF140->countsbCBTF140 == 1
			|| this->rlrlCBTF17->countsbCBTF17 == 1 || this->rlrlCBTF18->countsbCBTF18 == 1 || this->rlrlCBTF180->countsbCBTF180 == 1){
			this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
	}
}
int Semaphore_NOP_CBTF::getatSeconds() {
	return this->atSeconds;
}
void Semaphore_NOP_CBTF::setatSemaphoreState(int atSemaphoreState) {
	if (this->atSemaphoreState != atSemaphoreState) {
		this->atSemaphoreState = atSemaphoreState;

		this->prprCBTFAtSemaphoreState0->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState1->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState2->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState3->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState4->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState5->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState6->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState7->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState8->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprCBTFAtSemaphoreState9->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	}
}
int Semaphore_NOP_CBTF::getatSemaphoreState() {
	return this->atSemaphoreState;
}
void Semaphore_NOP_CBTF::setatHVSS(int atHVSS) {
	if (this->atHVSS != atHVSS) {
		this->atHVSS = atHVSS;
		if (this->rlrlCBTF11->countsbCBTF11 == 1 || this->rlrlCBTF17->countsbCBTF17 == 1){
			this->prprCBTFAtHVSS1->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);
		}
		if (this->rlrlCBTF12->countsbCBTF12 == 1 || this->rlrlCBTF18->countsbCBTF18 == 1){
			this->prprCBTFAtHVSS2->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);
		}
	}
}
int Semaphore_NOP_CBTF::getatHVSS() {
	return this->atHVSS;
}
void Semaphore_NOP_CBTF::setatVVSS(int atVVSS) {
	if (this->atVVSS != atVVSS) {
		this->atVVSS = atVVSS;
		if (this->rlrlCBTF13->countsbCBTF13 == 1 || this->rlrlCBTF15->countsbCBTF15 == 1){
			this->prprCBTFAtVVSS1->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
		}
		if (this->rlrlCBTF14->countsbCBTF14 == 1 || this->rlrlCBTF16->countsbCBTF16 == 1){
			this->prprCBTFAtVVSS2->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
		}
	}
}
int Semaphore_NOP_CBTF::getatVVSS() {
	return this->atVVSS;
}
void Semaphore_NOP_CBTF::mtRT() {
	//cout << "mtResetTimer Semaphore_NOP_CBTF" << endl;
	setatSeconds(0);
	this->SetCurrentCycleTime(0);
}
void Semaphore_NOP_CBTF::mtHTLG() {
	//cout << "mtHorizontalTrafficLightGreen Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(0);
	this->GetHorizontalTrafficLight()->SetState(GREEN);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBTF::mtHTLY() {
	//cout << "mtHorizontalTrafficLightYellow Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(1);
	this->GetHorizontalTrafficLight()->SetState(YELLOW);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBTF::mtHTLR() {
	//cout << "mtHorizontalTrafficLightRed Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(2);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBTF::mtVTLG() {
	//cout << "mtVerticalTrafficLightGreen Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(3);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(GREEN);
}
void Semaphore_NOP_CBTF::mtVTLY() {
	//cout << "mtVerticalTrafficLightYellow Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(4);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(YELLOW);
}
void Semaphore_NOP_CBTF::mtVTLR() {
	//cout << "mtVerticalTrafficLightRed Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(5);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBTF::mtHTLGCBTF() {
	//cout << "mtHorizontalTrafficLightGreenCBTF Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(6);
	this->GetHorizontalTrafficLight()->SetState(GREEN);
	this->GetVerticalTrafficLight()->SetState(RED);

}
void Semaphore_NOP_CBTF::mtHTLYCBTF() {
	//cout << "mtHorizontalTrafficLightYellowCBTF Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(7);
	this->GetHorizontalTrafficLight()->SetState(YELLOW);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBTF::mtVTLGCBTF() {
	//cout << "mtVerticalTrafficLightGreenCBTF Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(8);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(GREEN);

}
void Semaphore_NOP_CBTF::mtVTLYCBTF() {
	//cout << "mtVerticalTrafficLightRedCBTF Semaphore_NOP_CBTF" << endl;
	setatSemaphoreState(9);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(YELLOW);
}
void Semaphore_NOP_CBTF::increaseCycleTime(){
	currentCycleTime += 1;
	setatSeconds(currentCycleTime);
}

void Semaphore_NOP_CBTF::mtRGWPROP() {
	this->SetGreenWavePropagation(No_Propagation);
}

void Semaphore_NOP_CBTF::mtPHGW() {
	this->PropagateHorizontalGreenWave();
}

void Semaphore_NOP_CBTF::mtPVGW() {
	this->PropagateVerticalGreenWave();
}

void Semaphore_NOP_CBTF::SetVerticalVehicleSensorState(int state){
	verticalVehicleSensorState = state;
	setatVVSS(verticalVehicleSensorState);
}
void Semaphore_NOP_CBTF::SetHorizontalVehicleSensorState(int state){
	horizontalVehicleSensorState = state;
	setatHVSS(horizontalVehicleSensorState);
}

void Semaphore_NOP_CBTF::SetGreenWaveType(Green_Wave_Type Value) {
	setatGWType(Value);
	Semaphore::SetGreenWaveType(Value);
}

void Semaphore_NOP_CBTF::SetGreenWavePropagation(Green_Wave_Propagation Value) {
	setatGwProp(Value);
	Semaphore::SetGreenWavePropagation(Value);
}

void Semaphore_NOP_CBTF::setatGWType(Green_Wave_Type Value) {
	if (GreenWaveType != Value) {
		ptprCBTFAtGWType1->notifyprGreenWaveTypeState(Value);
		ptprCBTFAtGWType2->notifyprGreenWaveTypeState(Value);
	}

}

void Semaphore_NOP_CBTF::setatGwProp(Green_Wave_Propagation Value) {
	if (GreenWavePropagation != Value) {
		this->ptprCBTFAtGWProp1->notifyprGreenWavePropagationState(Value);
		this->ptprCBTFAtGWProp2->notifyprGreenWavePropagationState(Value);
	}

}