#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds7;
class prCBTFAtSeconds8;
class prCBTFAtVVSS2;
class rlCBTF14 {
public:
	rlCBTF14(void);
	~rlCBTF14(void);
	bool isApproved();
	int countsbCBTF14;
	int countImpsbCBTF14;
	void incCountsbCBTF14();
	void decCountsbCBTF14();
	void incCountImpsbCBTF14();
	void decCountImpsbCBTF14();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds7 *prprCBTFAtSeconds7;
	prCBTFAtSeconds8 *prprCBTFAtSeconds8;
	prCBTFAtVVSS2 *prprCBTFAtVVSS2;
};
