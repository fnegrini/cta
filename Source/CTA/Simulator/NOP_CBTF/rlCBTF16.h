#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds6;
class prCBTFAtVVSS2;
class rlCBTF16 {
public:
	rlCBTF16(void);
	~rlCBTF16(void);
	bool isApproved();
	int countsbCBTF16;
	int countImpsbCBTF16;
	void incCountsbCBTF16();
	void decCountsbCBTF16();
	void incCountImpsbCBTF16();
	void decCountImpsbCBTF16();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds6 *prprCBTFAtSeconds6;
	prCBTFAtVVSS2 *prprCBTFAtVVSS2;
};
