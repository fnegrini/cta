#include "rlCBTF10.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds5.h"
rlCBTF10::rlCBTF10() {
	countsbCBTF10 = 0;
	countImpsbCBTF10 = 0;
}
rlCBTF10::~rlCBTF10() {
}
void rlCBTF10::incCountsbCBTF10(){
	this->countsbCBTF10++;
	//isApproved();
	this->prprCBTFAtSeconds5->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF10::decCountsbCBTF10(){
	this->countsbCBTF10--;
}
void rlCBTF10::incCountImpsbCBTF10(){
	this->countImpsbCBTF10++;
	isApproved();
}
void rlCBTF10::decCountImpsbCBTF10(){
	this->countImpsbCBTF10--;
}
bool rlCBTF10::isApproved() {
	bool ret = false;
	if((countsbCBTF10 + countImpsbCBTF10) == 2) {
		this->semaphore_NOP->mtVTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
