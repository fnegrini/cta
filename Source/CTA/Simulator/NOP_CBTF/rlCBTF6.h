#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds1;
class rlCBTF6 {
public:
	rlCBTF6(void);
	~rlCBTF6(void);
	bool isApproved();
	int countsbCBTF6;
	int countImpsbCBTF6;
	void incCountsbCBTF6();
	void decCountsbCBTF6();
	void incCountImpsbCBTF6();
	void decCountImpsbCBTF6();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds1 *prprCBTFAtSeconds1;
};
