#include "rlCBTF6.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds1.h"
rlCBTF6::rlCBTF6() {
	countsbCBTF6 = 0;
	countImpsbCBTF6 = 0;
}
rlCBTF6::~rlCBTF6() {
}
void rlCBTF6::incCountsbCBTF6(){
	this->countsbCBTF6++;
	//isApproved();
	this->prprCBTFAtSeconds1->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF6::decCountsbCBTF6(){
	this->countsbCBTF6--;
}
void rlCBTF6::incCountImpsbCBTF6(){
	this->countImpsbCBTF6++;
	isApproved();
}
void rlCBTF6::decCountImpsbCBTF6(){
	this->countImpsbCBTF6--;
}
bool rlCBTF6::isApproved() {
	bool ret = false;
	if((countsbCBTF6 + countImpsbCBTF6) == 2) {
		this->semaphore_NOP->mtVTLG();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
