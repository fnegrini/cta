#include "rlCBTF9.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds4.h"
rlCBTF9::rlCBTF9() {
	countsbCBTF9 = 0;
	countImpsbCBTF9 = 0;
}
rlCBTF9::~rlCBTF9() {
}
void rlCBTF9::incCountsbCBTF9(){
	this->countsbCBTF9++;
	//isApproved();
	this->prprCBTFAtSeconds4->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF9::decCountsbCBTF9(){
	this->countsbCBTF9--;
}
void rlCBTF9::incCountImpsbCBTF9(){
	this->countImpsbCBTF9++;
	isApproved();
}
void rlCBTF9::decCountImpsbCBTF9(){
	this->countImpsbCBTF9--;
}
bool rlCBTF9::isApproved() {
	bool ret = false;
	if((countsbCBTF9 + countImpsbCBTF9) == 2) {
		this->semaphore_NOP->mtVTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
