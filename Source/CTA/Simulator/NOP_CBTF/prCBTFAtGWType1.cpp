#include "prCBTFAtGWType1.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF120.h"
#include "rlCBTF180.h"


prCBTFAtGWType1::prCBTFAtGWType1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF120 *rlrlCBTF120, rlCBTF180 *rlrlCBTF180) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prGreenWaveTypeState = false;

	this->rlrlCBTF120 = rlrlCBTF120;
	this->rlrlCBTF120->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF180 = rlrlCBTF180;
	this->rlrlCBTF180->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtGWType1::~prCBTFAtGWType1() {
}
void prCBTFAtGWType1::initprGreenWaveTypeState(Green_Wave_Type Value) {
	if (Value == Green_Wave_Horizontal) {
		if (prGreenWaveTypeState == false) {
			this->rlrlCBTF120->incCountImpsbCBTF120();
			this->rlrlCBTF180->incCountImpsbCBTF180();
			prGreenWaveTypeState = true;
		}
	}
}
void prCBTFAtGWType1::notifyprGreenWaveTypeState(Green_Wave_Type Value) {
	if (Value == Green_Wave_Horizontal) {
		if (prGreenWaveTypeState == false) {
			this->rlrlCBTF120->incCountImpsbCBTF120();
			this->rlrlCBTF180->incCountImpsbCBTF180();
			prGreenWaveTypeState = true;
		}
	}
	else {
		if (prGreenWaveTypeState == true) {
			this->rlrlCBTF120->decCountImpsbCBTF120();
			this->rlrlCBTF180->decCountImpsbCBTF180();
			prGreenWaveTypeState = false;
		}
	}
}