#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds6;
class prCBTFAtHVSS1;
class rlCBTF11 {
public:
	rlCBTF11(void);
	~rlCBTF11(void);
	bool isApproved();
	int countsbCBTF11;
	int countImpsbCBTF11;
	void incCountsbCBTF11();
	void decCountsbCBTF11();
	void incCountImpsbCBTF11();
	void decCountImpsbCBTF11();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds6 *prprCBTFAtSeconds6;
	prCBTFAtHVSS1 *prprCBTFAtHVSS1;
};
