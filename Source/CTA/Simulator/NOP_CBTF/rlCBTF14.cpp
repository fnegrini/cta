#include "rlCBTF14.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds7.h"
#include "prCBTFAtSeconds8.h"
#include "prCBTFAtVVSS2.h"
rlCBTF14::rlCBTF14() {
	countsbCBTF14 = 0;
	countImpsbCBTF14 = 0;
}
rlCBTF14::~rlCBTF14() {
}
void rlCBTF14::incCountsbCBTF14(){
	this->countsbCBTF14++;
	//isApproved();
	this->prprCBTFAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtVVSS2->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atVVSS);
}
void rlCBTF14::decCountsbCBTF14(){
	this->countsbCBTF14--;
}
void rlCBTF14::incCountImpsbCBTF14(){
	this->countImpsbCBTF14++;
	isApproved();
}
void rlCBTF14::decCountImpsbCBTF14(){
	this->countImpsbCBTF14--;
}
bool rlCBTF14::isApproved() {
	bool ret = false;
	if((countsbCBTF14 + countImpsbCBTF14) == 4) {
		this->semaphore_NOP->mtHTLYCBTF();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
