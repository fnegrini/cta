#include "prCBTFAtVVSS1.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF15.h"
#include "rlCBTF13.h"

prCBTFAtVVSS1::prCBTFAtVVSS1(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF15 *rlrlCBTF15, rlCBTF13 *rlrlCBTF13) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prVehicleSensorStateState = false;

	this->rlrlCBTF15 = rlrlCBTF15;
	this->rlrlCBTF15->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF13 = rlrlCBTF13;
	this->rlrlCBTF13->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtVVSS1::~prCBTFAtVVSS1() {
}
void prCBTFAtVVSS1::initprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF15->incCountImpsbCBTF15();
			this->rlrlCBTF13->incCountImpsbCBTF13();			
			prVehicleSensorStateState = true;
		}
	}
}
void prCBTFAtVVSS1::notifyprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBTF15->incCountImpsbCBTF15();
			this->rlrlCBTF13->incCountImpsbCBTF13();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBTF15->decCountImpsbCBTF15();
			this->rlrlCBTF13->decCountImpsbCBTF13();
			prVehicleSensorStateState = false;
		}
	}
}