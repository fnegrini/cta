#include "prCBTFAtSemaphoreState7.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF5.h"

prCBTFAtSemaphoreState7::prCBTFAtSemaphoreState7(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF5 *rlrlCBTF5) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF5 = rlrlCBTF5;
	this->rlrlCBTF5->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState7::~prCBTFAtSemaphoreState7() {
}
void prCBTFAtSemaphoreState7::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 7) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF5->incCountsbCBTF5();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState7::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 7) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF5->incCountsbCBTF5();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF5->decCountsbCBTF5();
			prSemaphoreStateState = false;
		}
	}
}