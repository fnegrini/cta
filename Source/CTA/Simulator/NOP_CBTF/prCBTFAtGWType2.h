#pragma once
#include <iostream>
#include "../GreenWaveType.h"

using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF140;
class rlCBTF160;

class prCBTFAtGWType2 {
public:
	prCBTFAtGWType2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF140 *rlrlCBTF140, rlCBTF160 *rlrlCBTF160);
	~prCBTFAtGWType2(void);
	bool prGreenWaveTypeState;
	void initprGreenWaveTypeState(Green_Wave_Type Value);
	void notifyprGreenWaveTypeState(Green_Wave_Type Value);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF140 *rlrlCBTF140;
	rlCBTF160 *rlrlCBTF160;
};
