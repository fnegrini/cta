#include "rlCBTF16.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtVVSS2.h"
rlCBTF16::rlCBTF16() {
	countsbCBTF16 = 0;
	countImpsbCBTF16 = 0;
}
rlCBTF16::~rlCBTF16() {
}
void rlCBTF16::incCountsbCBTF16(){
	this->countsbCBTF16++;
	//isApproved();
	this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtVVSS2->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atHVSS);
}
void rlCBTF16::decCountsbCBTF16(){
	this->countsbCBTF16--;
}
void rlCBTF16::incCountImpsbCBTF16(){
	this->countImpsbCBTF16++;
	isApproved();
}
void rlCBTF16::decCountImpsbCBTF16(){
	this->countImpsbCBTF16--;
}
bool rlCBTF16::isApproved() {
	bool ret = false;
	if((countsbCBTF16 + countImpsbCBTF16) == 3) {
		this->semaphore_NOP->mtVTLGCBTF();
		this->semaphore_NOP->mtPVGW();
		this->semaphore_NOP->mtRGWPROP();
		ret = true;
	} 
	return ret; 
 }
