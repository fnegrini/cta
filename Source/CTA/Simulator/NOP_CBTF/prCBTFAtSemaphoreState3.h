#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class rlCBTF7;
class rlCBTF15;
class rlCBTF16;
class rlCBTF160;
class rlCBTF17;
class rlCBTF18;
class rlCBTF180;
class prCBTFAtSemaphoreState3 {
public:
	prCBTFAtSemaphoreState3(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF7 *rlrlCBTF7, rlCBTF15 *rlrlCBTF15, rlCBTF16 *rlrlCBTF16, rlCBTF160 *rlrlCBTF160, rlCBTF17 *rlrlCBTF17, rlCBTF18 *rlrlCBTF18, rlCBTF180 *rlrlCBTF180);
	~prCBTFAtSemaphoreState3(void);
	bool prSemaphoreStateState;
	void initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	Semaphore_NOP_CBTF * semaphore_NOP;
	rlCBTF7 *rlrlCBTF7;
	rlCBTF15 *rlrlCBTF15;
	rlCBTF16 *rlrlCBTF16;
	rlCBTF160 *rlrlCBTF160;
	rlCBTF17 *rlrlCBTF17;
	rlCBTF18 *rlrlCBTF18;
	rlCBTF180 *rlrlCBTF180;
};
