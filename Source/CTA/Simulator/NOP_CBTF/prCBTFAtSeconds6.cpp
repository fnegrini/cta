#include "prCBTFAtSeconds6.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF11.h"
#include "rlCBTF12.h"
#include "rlCBTF120.h"
#include "rlCBTF15.h"
#include "rlCBTF16.h"
#include "rlCBTF160.h"

prCBTFAtSeconds6::prCBTFAtSeconds6(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF11 *rlrlCBTF11, rlCBTF12 *rlrlCBTF12, rlCBTF120 *rlrlCBTF120, rlCBTF15 *rlrlCBTF15, rlCBTF16 *rlrlCBTF16, rlCBTF160 *rlrlCBTF160) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSecondsState = false;

	this->rlrlCBTF11 = rlrlCBTF11;
	this->rlrlCBTF11->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF12 = rlrlCBTF12;
	this->rlrlCBTF12->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF120 = rlrlCBTF120;
	this->rlrlCBTF120->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF15 = rlrlCBTF15;
	this->rlrlCBTF15->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF16 = rlrlCBTF16;
	this->rlrlCBTF16->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF160 = rlrlCBTF160;
	this->rlrlCBTF160->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSeconds6::~prCBTFAtSeconds6() {
}
void prCBTFAtSeconds6::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds <= 17) {
		if (prSecondsState == false) {
			this->rlrlCBTF11->incCountImpsbCBTF11();
			this->rlrlCBTF12->incCountImpsbCBTF12();
			this->rlrlCBTF120->incCountImpsbCBTF120();
			this->rlrlCBTF15->incCountImpsbCBTF15();
			this->rlrlCBTF16->incCountImpsbCBTF16();
			this->rlrlCBTF160->incCountImpsbCBTF160();
			prSecondsState = true;
		}
	}
}
void prCBTFAtSeconds6::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds <= 17) {
		if (prSecondsState == false) {
			this->rlrlCBTF11->incCountImpsbCBTF11();
			this->rlrlCBTF12->incCountImpsbCBTF12();
			this->rlrlCBTF120->incCountImpsbCBTF120();
			this->rlrlCBTF15->incCountImpsbCBTF15();
			this->rlrlCBTF16->incCountImpsbCBTF16();
			this->rlrlCBTF160->incCountImpsbCBTF160();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBTF11->decCountImpsbCBTF11();
			this->rlrlCBTF12->decCountImpsbCBTF12();
			this->rlrlCBTF120->decCountImpsbCBTF120();
			this->rlrlCBTF15->decCountImpsbCBTF15();
			this->rlrlCBTF16->decCountImpsbCBTF16();
			this->rlrlCBTF160->decCountImpsbCBTF160();
			prSecondsState = false;
		}
	}
}