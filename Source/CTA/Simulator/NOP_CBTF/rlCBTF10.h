#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds5;
class rlCBTF10 {
public:
	rlCBTF10(void);
	~rlCBTF10(void);
	bool isApproved();
	int countsbCBTF10;
	int countImpsbCBTF10;
	void incCountsbCBTF10();
	void decCountsbCBTF10();
	void incCountImpsbCBTF10();
	void decCountImpsbCBTF10();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds5 *prprCBTFAtSeconds5;
};
