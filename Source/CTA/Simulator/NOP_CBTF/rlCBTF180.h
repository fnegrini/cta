#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds7;
class prCBTFAtSeconds8;
class prCBTFAtGWProp1;
class prCBTFAtGWType1;

class rlCBTF180 {
public:
	rlCBTF180(void);
	~rlCBTF180(void);
	bool isApproved();
	int countsbCBTF180;
	int countImpsbCBTF180;
	void incCountsbCBTF180();
	void decCountsbCBTF180();
	void incCountImpsbCBTF180();
	void decCountImpsbCBTF180();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds7 *prprCBTFAtSeconds7;
	prCBTFAtSeconds8 *prprCBTFAtSeconds8;
	prCBTFAtGWProp1 *prprCBTFAtGWProp1;
	prCBTFAtGWType1 *prprCBTFAtGWType1;

};
