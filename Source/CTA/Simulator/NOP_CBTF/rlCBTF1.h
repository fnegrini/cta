#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds1;
class rlCBTF1 {
public:
	rlCBTF1(void);
	~rlCBTF1(void);
	bool isApproved();
	int countsbCBTF1;
	int countImpsbCBTF1;
	void incCountsbCBTF1();
	void decCountsbCBTF1();
	void incCountImpsbCBTF1();
	void decCountImpsbCBTF1();
	Semaphore_NOP_CBTF *semaphore_NOP;
	prCBTFAtSeconds1 *prprCBTFAtSeconds1;
};
