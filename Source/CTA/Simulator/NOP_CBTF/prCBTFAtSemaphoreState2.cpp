#include "prCBTFAtSemaphoreState2.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF6.h"

prCBTFAtSemaphoreState2::prCBTFAtSemaphoreState2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF6 *rlrlCBTF6) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF6 = rlrlCBTF6;
	this->rlrlCBTF6->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState2::~prCBTFAtSemaphoreState2() {
}
void prCBTFAtSemaphoreState2::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 2) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF6->incCountsbCBTF6();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState2::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 2) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF6->incCountsbCBTF6();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF6->decCountsbCBTF6();
			prSemaphoreStateState = false;
		}
	}
}