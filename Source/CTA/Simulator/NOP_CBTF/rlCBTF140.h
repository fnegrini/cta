#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds7;
class prCBTFAtSeconds8;
class prCBTFAtGWProp2;
class prCBTFAtGWType2;

class rlCBTF140 {
public:
	rlCBTF140(void);
	~rlCBTF140(void);
	bool isApproved();
	int countsbCBTF140;
	int countImpsbCBTF140;
	void incCountsbCBTF140();
	void decCountsbCBTF140();
	void incCountImpsbCBTF140();
	void decCountImpsbCBTF140();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds7 *prprCBTFAtSeconds7;
	prCBTFAtSeconds8 *prprCBTFAtSeconds8;
	prCBTFAtGWProp2 *prprCBTFAtGWProp2;
	prCBTFAtGWType2	*prprCBTFAtGWType2;

};
