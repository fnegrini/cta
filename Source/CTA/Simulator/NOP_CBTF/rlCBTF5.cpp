#include "rlCBTF5.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds5.h"
rlCBTF5::rlCBTF5() {
	countsbCBTF5 = 0;
	countImpsbCBTF5 = 0;
}
rlCBTF5::~rlCBTF5() {
}
void rlCBTF5::incCountsbCBTF5(){
	this->countsbCBTF5++;
	//isApproved();
	this->prprCBTFAtSeconds5->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBTF5::decCountsbCBTF5(){
	this->countsbCBTF5--;
}
void rlCBTF5::incCountImpsbCBTF5(){
	this->countImpsbCBTF5++;
	isApproved();
}
void rlCBTF5::decCountImpsbCBTF5(){
	this->countImpsbCBTF5--;
}
bool rlCBTF5::isApproved() {
	bool ret = false;
	if((countsbCBTF5 + countImpsbCBTF5) == 2) {
		this->semaphore_NOP->mtHTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
