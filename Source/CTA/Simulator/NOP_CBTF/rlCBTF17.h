#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds7;
class prCBTFAtSeconds8;
class prCBTFAtHVSS1;
class rlCBTF17 {
public:
	rlCBTF17(void);
	~rlCBTF17(void);
	bool isApproved();
	int countsbCBTF17;
	int countImpsbCBTF17;
	void incCountsbCBTF17();
	void decCountsbCBTF17();
	void incCountImpsbCBTF17();
	void decCountImpsbCBTF17();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds7 *prprCBTFAtSeconds7;
	prCBTFAtSeconds8 *prprCBTFAtSeconds8;
	prCBTFAtHVSS1 *prprCBTFAtHVSS1;
};
