#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds3;
class rlCBTF8 {
public:
	rlCBTF8(void);
	~rlCBTF8(void);
	bool isApproved();
	int countsbCBTF8;
	int countImpsbCBTF8;
	void incCountsbCBTF8();
	void decCountsbCBTF8();
	void incCountImpsbCBTF8();
	void decCountImpsbCBTF8();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds3 *prprCBTFAtSeconds3;
};
