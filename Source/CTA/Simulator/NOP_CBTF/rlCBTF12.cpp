#include "rlCBTF12.h"
#include "Semaphore_NOP_CBTF.h"
#include "prCBTFAtSeconds6.h"
#include "prCBTFAtHVSS2.h"
rlCBTF12::rlCBTF12() {
	countsbCBTF12 = 0;
	countImpsbCBTF12 = 0;
}
rlCBTF12::~rlCBTF12() {
}
void rlCBTF12::incCountsbCBTF12(){
	this->countsbCBTF12++;
	//isApproved();
	this->prprCBTFAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprCBTFAtHVSS2->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atVVSS);
}
void rlCBTF12::decCountsbCBTF12(){
	this->countsbCBTF12--;
}
void rlCBTF12::incCountImpsbCBTF12(){
	this->countImpsbCBTF12++;
	isApproved();
}
void rlCBTF12::decCountImpsbCBTF12(){
	this->countImpsbCBTF12--;
}
bool rlCBTF12::isApproved() {
	bool ret = false;
	if((countsbCBTF12 + countImpsbCBTF12) == 3) {
		this->semaphore_NOP->mtHTLGCBTF();
		this->semaphore_NOP->mtPHGW();
		this->semaphore_NOP->mtRGWPROP();
		ret = true;
	} 
	return ret; 
 }
