#include "prCBTFAtGWType2.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF140.h"
#include "rlCBTF160.h"

prCBTFAtGWType2::prCBTFAtGWType2(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF140 *rlrlCBTF140, rlCBTF160 *rlrlCBTF160) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prGreenWaveTypeState = false;

	this->rlrlCBTF140 = rlrlCBTF140;
	this->rlrlCBTF140->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBTF160 = rlrlCBTF160;
	this->rlrlCBTF160->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtGWType2::~prCBTFAtGWType2() {
}
void prCBTFAtGWType2::initprGreenWaveTypeState(Green_Wave_Type Value) {
	if (Value == Green_Wave_Vertical) {
		if (prGreenWaveTypeState == false) {
			this->rlrlCBTF140->incCountImpsbCBTF140();
			this->rlrlCBTF160->incCountImpsbCBTF160();
			prGreenWaveTypeState = true;
		}
	}
}
void prCBTFAtGWType2::notifyprGreenWaveTypeState(Green_Wave_Type Value) {
	if (Value == Green_Wave_Vertical) {
		if (prGreenWaveTypeState == false) {
			this->rlrlCBTF140->incCountImpsbCBTF140();
			this->rlrlCBTF160->incCountImpsbCBTF160();
			prGreenWaveTypeState = true;
		}
	}
	else {
		if (prGreenWaveTypeState == true) {
			this->rlrlCBTF140->decCountImpsbCBTF140();
			this->rlrlCBTF160->decCountImpsbCBTF160();
			prGreenWaveTypeState = false;
		}
	}
}