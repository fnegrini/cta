#include "prCBTFAtSemaphoreState0.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF2.h"
#include "rlCBTF11.h"
#include "rlCBTF12.h"
#include "rlCBTF13.h"
#include "rlCBTF120.h"
#include "rlCBTF13.h"
#include "rlCBTF14.h"
#include "rlCBTF140.h"

prCBTFAtSemaphoreState0::prCBTFAtSemaphoreState0(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF2 *rlrlCBTF2, rlCBTF11 *rlrlCBTF11, rlCBTF12 *rlrlCBTF12, rlCBTF120 *rlrlCBTF120, rlCBTF13 *rlrlCBTF13, rlCBTF14 *rlrlCBTF14, rlCBTF140 *rlrlCBTF140) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF2 = rlrlCBTF2;
	this->rlrlCBTF2->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF11 = rlrlCBTF11;
	this->rlrlCBTF11->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF12 = rlrlCBTF12;
	this->rlrlCBTF12->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF120 = rlrlCBTF120;
	this->rlrlCBTF120->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF13 = rlrlCBTF13;
	this->rlrlCBTF13->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF14 = rlrlCBTF14;
	this->rlrlCBTF14->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBTF140 = rlrlCBTF140;
	this->rlrlCBTF140->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState0::~prCBTFAtSemaphoreState0() {
}
void prCBTFAtSemaphoreState0::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 0) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF2->incCountsbCBTF2();	
			this->rlrlCBTF11->incCountsbCBTF11();
			this->rlrlCBTF12->incCountsbCBTF12();
			this->rlrlCBTF120->incCountsbCBTF120();
			this->rlrlCBTF13->incCountsbCBTF13();
			this->rlrlCBTF14->incCountsbCBTF14();
			this->rlrlCBTF140->incCountsbCBTF140();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState0::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 0) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF2->incCountsbCBTF2();
			this->rlrlCBTF11->incCountsbCBTF11();
			this->rlrlCBTF12->incCountsbCBTF12();
			this->rlrlCBTF120->incCountsbCBTF120();
			this->rlrlCBTF13->incCountsbCBTF13();
			this->rlrlCBTF14->incCountsbCBTF14();
			this->rlrlCBTF140->incCountsbCBTF140();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF2->decCountsbCBTF2();		
			this->rlrlCBTF11->decCountsbCBTF11();
			this->rlrlCBTF12->decCountsbCBTF12();
			this->rlrlCBTF120->decCountsbCBTF120();
			this->rlrlCBTF13->decCountsbCBTF13();
			this->rlrlCBTF14->decCountsbCBTF14();
			this->rlrlCBTF140->decCountsbCBTF140();
			prSemaphoreStateState = false;
		}
	}
}