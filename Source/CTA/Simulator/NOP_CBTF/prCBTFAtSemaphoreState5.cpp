#include "prCBTFAtSemaphoreState5.h"
#include "Semaphore_NOP_CBTF.h"
#include "rlCBTF1.h"

prCBTFAtSemaphoreState5::prCBTFAtSemaphoreState5(Semaphore_NOP_CBTF *semaphore_NOP_CBTF, rlCBTF1 *rlrlCBTF1) {
	this->semaphore_NOP = semaphore_NOP_CBTF;

	prSemaphoreStateState = false;

	this->rlrlCBTF1 = rlrlCBTF1;
	this->rlrlCBTF1->semaphore_NOP = this->semaphore_NOP;
}
prCBTFAtSemaphoreState5::~prCBTFAtSemaphoreState5() {
}
void prCBTFAtSemaphoreState5::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 5) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF1->incCountsbCBTF1();
			prSemaphoreStateState = true;
		}
	}
}
void prCBTFAtSemaphoreState5::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 5) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBTF1->incCountsbCBTF1();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBTF1->decCountsbCBTF1();
			prSemaphoreStateState = false;
		}
	}
}