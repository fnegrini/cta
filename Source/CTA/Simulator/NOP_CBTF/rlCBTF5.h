#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBTF;
class prCBTFAtSeconds5;
class rlCBTF5 {
public:
	rlCBTF5(void);
	~rlCBTF5(void);
	bool isApproved();
	int countsbCBTF5;
	int countImpsbCBTF5;
	void incCountsbCBTF5();
	void decCountsbCBTF5();
	void incCountImpsbCBTF5();
	void decCountImpsbCBTF5();
	Semaphore_NOP_CBTF * semaphore_NOP;
	prCBTFAtSeconds5 *prprCBTFAtSeconds5;
};
