///////////////////////////////////////////////////////////
//  Traffic_Light.h
//  Implementation of the Class Traffic_Light
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Light_State.h"

#include <string>
using namespace std;


class Block;
class Semaphore;

class Traffic_Light
{

public:
	Traffic_Light(Block* pBlock, const string pId = "");
	~Traffic_Light();

	string GetId();
	Semaphore* GetSemaphore();
	Traffic_Light_State GetState();
	Block* GetBlock();
	void SetId(string newVal);
	void SetSemaphore(Semaphore* newVal);
	void SetBlock(Block* newVal);
	void SetState(Traffic_Light_State newVal);

private:
	string id;
	Semaphore* semaphore;
	Traffic_Light_State state;
	Block* block;

};
