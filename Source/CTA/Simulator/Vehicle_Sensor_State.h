///////////////////////////////////////////////////////////
//  Vehicle_Sensor_State.h
//  Implementation of the Enumeration Vehicle_Sensor_State
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

enum Vehicle_Sensor_State
{
	FEW,
	MANY,
	FULL
};
