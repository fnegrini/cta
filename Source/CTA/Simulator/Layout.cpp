///////////////////////////////////////////////////////////
//  Layout.cpp
//  Implementation of the Class Layout
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Layout.h"

Layout::Layout():inputFile(){
	
	inputFile.readFile();
	config = inputFile.getResult();

	if (config.size() == 5)
	{
		numberOfHorizontalStreets = stoi(config[0]);
		numberOfVerticalStreets = stoi(config[1]);
		nuOfLanes = stoi(config[2]);
		trafficIntensity = stof(config[3]);
		pctOfTurns = stoi(config[4]);		
	}
	else
	{
		numberOfHorizontalStreets = 10;
		numberOfVerticalStreets = 10;
		nuOfLanes = 2;
		trafficIntensity = 0.5;
		pctOfTurns = 10;
	}

	int i, j;
	for (i = 0; i < numberOfHorizontalStreets; i++)
	{
		horizontalTrafficIntensity.push_back(trafficIntensity);
		numberOfLanesHorizontal.push_back(nuOfLanes);
	}
	for (j = 0; j < numberOfVerticalStreets; j++)
	{
		verticalTrafficIntensity.push_back(trafficIntensity);
		numberOfLanesVertical.push_back(nuOfLanes);
	}
	for (i = 0; i < numberOfHorizontalStreets; i++)
	{
		for (j = 0; j < numberOfVerticalStreets; j++)
		{
			horizontalPctOfTurns.push_back(pctOfTurns);
			verticalPctOfTurns.push_back(pctOfTurns);
		}
	}
}

Layout::~Layout(){
}

int Layout::GetNumberOfHorizontalStreets()
{
	return numberOfHorizontalStreets;
}

void Layout::SetNumberOfHorizontalStreets(int pN)
{
	numberOfHorizontalStreets = pN;
}

int Layout::GetNumberOfVerticalStreets()
{
	return numberOfVerticalStreets;
}

void Layout::SetNumberOfVerticalStreets(int pN)
{
	numberOfVerticalStreets = pN;
}

vector<int> Layout::GetNumberOfLanesHorizontal()
{
	return numberOfLanesHorizontal;
}

vector<int> Layout::GetNumberOfLanesVertical()
{
	return numberOfLanesVertical;
}

vector<float> Layout::GetHorizontalTrafficIntensity()
{
	return horizontalTrafficIntensity;
}

vector<float> Layout::GetVerticalTrafficIntensity()
{
	return verticalTrafficIntensity;
}

vector<int> Layout::GetHorizontalPctOfTurns()
{
	return horizontalPctOfTurns;
}
vector<int> Layout::GetVerticalPctOfTurns()
{
	return verticalPctOfTurns;
}

void Layout::readFile(){

}