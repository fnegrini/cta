#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP;
class rlVerticalTrafficLightGreen {
public:
	rlVerticalTrafficLightGreen(void);
	~rlVerticalTrafficLightGreen(void);
	bool isApproved();
	bool prSemaphoreState4State;
	void initprSemaphoreState4semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreState4semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	bool prAtSeconds4State;
	void initprAtSeconds4semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprAtSeconds4semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	int countsbVerticalTrafficLightGreen;
	int countImpsbVerticalTrafficLightGreen;
	Semaphore_NOP * semaphore_NOP;
};
