#include "rlVerticalTrafficLightRed.h"
#include "Semaphore_NOP.h"
rlVerticalTrafficLightRed::rlVerticalTrafficLightRed(){
	countsbVerticalTrafficLightRed = 0;
	countImpsbVerticalTrafficLightRed = 0;
	prSemaphoreState6State = false;
	prAtSeconds6State = false;
}
rlVerticalTrafficLightRed::~rlVerticalTrafficLightRed() {
}
void rlVerticalTrafficLightRed::initprSemaphoreState6semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 4) {
		if (prSemaphoreState6State == false) {
			countsbVerticalTrafficLightRed++;
			prSemaphoreState6State = true;
		}
	}
}
void rlVerticalTrafficLightRed::notifyprSemaphoreState6semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 4) {
		if (prSemaphoreState6State == false) {
			countsbVerticalTrafficLightRed++;
			prSemaphoreState6State = true;
		}
		
		this->notifyprAtSeconds6semaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
		//this->isApproved();
	}
	else {
		if (prSemaphoreState6State == true) {
			countsbVerticalTrafficLightRed--;
			prSemaphoreState6State = false;
		}
	}
}
void rlVerticalTrafficLightRed::initprAtSeconds6semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 90) {
		if (prAtSeconds6State == false) {
			countImpsbVerticalTrafficLightRed++;
			prAtSeconds6State = true;
		}
	}
}
void rlVerticalTrafficLightRed::notifyprAtSeconds6semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 90) {
		if (prAtSeconds6State == false) {
			countImpsbVerticalTrafficLightRed++;
			prAtSeconds6State = true;
		}
		//if(countsbVerticalTrafficLightRed == 1) { 
		this->isApproved();
		//} 
	}
	else {
		if (prAtSeconds6State == true) {
			countImpsbVerticalTrafficLightRed--;
			prAtSeconds6State = false;
		}
	}
}
bool rlVerticalTrafficLightRed::isApproved() {
	bool ret = false;
	if ((countsbVerticalTrafficLightRed + countImpsbVerticalTrafficLightRed) == 2) {
		this->semaphore_NOP->mtVerticalTrafficLightRED();
		this->semaphore_NOP->mtResetTimer();
		ret = true;
	}
	return ret;
}
