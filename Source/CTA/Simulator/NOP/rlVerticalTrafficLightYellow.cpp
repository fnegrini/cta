#include "rlVerticalTrafficLightYellow.h"
#include "Semaphore_NOP.h"
rlVerticalTrafficLightYellow::rlVerticalTrafficLightYellow() {
	countsbVerticalTrafficLightYellow = 0;
	countImpsbVerticalTrafficLightYellow = 0;
	prSemaphoreState5State = false;
	prAtSeconds5State = false;	
}
rlVerticalTrafficLightYellow::~rlVerticalTrafficLightYellow() {
}
void rlVerticalTrafficLightYellow::initprSemaphoreState5semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 3) {
		if(prSemaphoreState5State == false) {
		countsbVerticalTrafficLightYellow++;
		prSemaphoreState5State = true;
		}
	}
}
void rlVerticalTrafficLightYellow::notifyprSemaphoreState5semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 3) {
		if(prSemaphoreState5State == false) {
		countsbVerticalTrafficLightYellow++;
		prSemaphoreState5State = true;
		}
		//this->semaphore_NOP->atSecondsArray[5] = &this->notifyprAtSeconds5semaphore_NOPatSeconds;
		this->notifyprAtSeconds5semaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
		//this->isApproved();
	} else {
		if(prSemaphoreState5State == true) {
		countsbVerticalTrafficLightYellow--;
		prSemaphoreState5State = false;
		}
	} 
}
void rlVerticalTrafficLightYellow::initprAtSeconds5semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 85) {
		if(prAtSeconds5State == false) {
		countImpsbVerticalTrafficLightYellow++;
		prAtSeconds5State = true;
		}
	}
}
void rlVerticalTrafficLightYellow::notifyprAtSeconds5semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 85) {
		if(prAtSeconds5State == false) {
		countImpsbVerticalTrafficLightYellow++;
		prAtSeconds5State = true;
		}
		//if(countsbVerticalTrafficLightYellow == 1) { 
		this->isApproved();
		//} 
	} else {
		if(prAtSeconds5State == true) {
		countImpsbVerticalTrafficLightYellow--;
		prAtSeconds5State = false;
		}
	} 
}
bool rlVerticalTrafficLightYellow::isApproved() {
	bool ret = false;
	if((countsbVerticalTrafficLightYellow + countImpsbVerticalTrafficLightYellow) == 2) {
		this->semaphore_NOP->mtVerticalTrafficLightYELLOW();
		ret = true;
	} 
	return ret; 
 }
