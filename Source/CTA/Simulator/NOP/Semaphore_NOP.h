#pragma once
#include "../Semaphore.h"
#include <iostream>
using namespace std;
class rlHorizontalTrafficLightGreen;
class rlHorizontalTrafficLightYellow;
class rlHorizontalTrafficLightRed;
class rlVerticalTrafficLightGreen;
class rlVerticalTrafficLightYellow;
class rlVerticalTrafficLightRed;
class Semaphore_NOP : public Semaphore
{
public:
	Semaphore_NOP(int semaphoreId, Traffic_Light* horizontalTrafficLight, Traffic_Light* verticalTrafficLight);
	~Semaphore_NOP(void);
	int atSeconds;
	void setatSeconds(int atSeconds);
	int getatSeconds();
	int atSemaphoreState;
	void setatSemaphoreState(int atSemaphoreState);
	int getatSemaphoreState();
	void mtResetTimer();
	void mtHorizontalTrafficLightGREEN();
	void mtHorizontalTrafficLightYELLOW();
	void mtHorizontalTrafficLightRED();
	void mtVerticalTrafficLightGREEN();
	void mtVerticalTrafficLightYELLOW();
	void mtVerticalTrafficLightRED();
	rlHorizontalTrafficLightGreen * rlrlHorizontalTrafficLightGreen;
	rlHorizontalTrafficLightYellow * rlrlHorizontalTrafficLightYellow;
	rlHorizontalTrafficLightRed * rlrlHorizontalTrafficLightRed;
	rlVerticalTrafficLightGreen * rlrlVerticalTrafficLightGreen;
	rlVerticalTrafficLightYellow * rlrlVerticalTrafficLightYellow;
	rlVerticalTrafficLightRed * rlrlVerticalTrafficLightRed;
    void increaseCycleTime();
	int atSecondsArray[6];
	void (*ptrFunc)(int);
};
