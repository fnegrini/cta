#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP;
class rlHorizontalTrafficLightYellow {
public:
	rlHorizontalTrafficLightYellow(void);
	~rlHorizontalTrafficLightYellow(void);
	bool isApproved();
	bool prSemaphoreState2State;
	void initprSemaphoreState2semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreState2semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	bool prSeconds2State;
	void initprSeconds2semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSeconds2semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	int countsbHorizontalTrafficLightYellow;
	int countImpsbHorizontalTrafficLightYellow;
	Semaphore_NOP * semaphore_NOP;
};
