#include "rlHorizontalTrafficLightYellow.h"
#include "Semaphore_NOP.h"
rlHorizontalTrafficLightYellow::rlHorizontalTrafficLightYellow() {
	countsbHorizontalTrafficLightYellow = 0;
	countImpsbHorizontalTrafficLightYellow = 0;
	prSemaphoreState2State = false;
	prSeconds2State = false;
}
rlHorizontalTrafficLightYellow::~rlHorizontalTrafficLightYellow() {
}
void rlHorizontalTrafficLightYellow::initprSemaphoreState2semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 0) {
		if(prSemaphoreState2State == false) {
		countsbHorizontalTrafficLightYellow++;
		prSemaphoreState2State = true;
		}
	}
}
void rlHorizontalTrafficLightYellow::notifyprSemaphoreState2semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 0) {
		if(prSemaphoreState2State == false) {
		countsbHorizontalTrafficLightYellow++;
		prSemaphoreState2State = true;
		}
		//this->isApproved();
		//this->semaphore_NOP->atSecondsArray[2] = &this->notifyprSeconds2semaphore_NOPatSeconds;
		this->notifyprSeconds2semaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	} else {
		if(prSemaphoreState2State == true) {
		countsbHorizontalTrafficLightYellow--;
		prSemaphoreState2State = false;
		}
	} 
}
void rlHorizontalTrafficLightYellow::initprSeconds2semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 40) {
		if(prSeconds2State == false) {
		countImpsbHorizontalTrafficLightYellow++;
		prSeconds2State = true;
		}
	}
}
void rlHorizontalTrafficLightYellow::notifyprSeconds2semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 40) {
		if(prSeconds2State == false) {
		countImpsbHorizontalTrafficLightYellow++;
		prSeconds2State = true;
		}
		//if(countsbHorizontalTrafficLightYellow == 1) { 
		this->isApproved();
		//} 
	} else {
		if(prSeconds2State == true) {
		countImpsbHorizontalTrafficLightYellow--;
		prSeconds2State = false;
		}
	} 
}
bool rlHorizontalTrafficLightYellow::isApproved() {
	bool ret = false;
	if((countsbHorizontalTrafficLightYellow + countImpsbHorizontalTrafficLightYellow) == 2) {
		this->semaphore_NOP->mtHorizontalTrafficLightYELLOW();
		ret = true;
	} 
	return ret; 
 }
