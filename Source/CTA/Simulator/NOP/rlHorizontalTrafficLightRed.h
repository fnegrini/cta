#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP;
class rlHorizontalTrafficLightRed {
public:
	rlHorizontalTrafficLightRed(void);
	~rlHorizontalTrafficLightRed(void);
	bool isApproved();
	bool prSemaphoreState3State;
	void initprSemaphoreState3semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreState3semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	bool prAtSeconds3State;
	void initprAtSeconds3semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprAtSeconds3semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	int countsbHorizontalTrafficLightRed;
	int countImpsbHorizontalTrafficLightRed;
	Semaphore_NOP * semaphore_NOP;
};
