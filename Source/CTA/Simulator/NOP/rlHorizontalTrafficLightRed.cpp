#include "rlHorizontalTrafficLightRed.h"
#include "Semaphore_NOP.h"
rlHorizontalTrafficLightRed::rlHorizontalTrafficLightRed() {
	countsbHorizontalTrafficLightRed = 0;
	countImpsbHorizontalTrafficLightRed = 0;
	prSemaphoreState3State = false;
	prAtSeconds3State = false;
}
rlHorizontalTrafficLightRed::~rlHorizontalTrafficLightRed() {
}
void rlHorizontalTrafficLightRed::initprSemaphoreState3semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if(prSemaphoreState3State == false) {
		countsbHorizontalTrafficLightRed++;
		prSemaphoreState3State = true;
		}
	}
}
void rlHorizontalTrafficLightRed::notifyprSemaphoreState3semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if(prSemaphoreState3State == false) {
		countsbHorizontalTrafficLightRed++;
		prSemaphoreState3State = true;
		}
		//this->isApproved();
		//this->semaphore_NOP->atSecondsArray[1] = &this->notifyprAtSeconds3semaphore_NOPatSeconds;
		this->notifyprAtSeconds3semaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	} else {
		if(prSemaphoreState3State == true) {
		countsbHorizontalTrafficLightRed--;
		prSemaphoreState3State = false;
		}
	} 
}
void rlHorizontalTrafficLightRed::initprAtSeconds3semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 45) {
		if(prAtSeconds3State == false) {
		countImpsbHorizontalTrafficLightRed++;
		prAtSeconds3State = true;
		}
	}
}
void rlHorizontalTrafficLightRed::notifyprAtSeconds3semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 45) {
		if(prAtSeconds3State == false) {
		countImpsbHorizontalTrafficLightRed++;
		prAtSeconds3State = true;
		}
		//if(countsbHorizontalTrafficLightRed == 1) { 
		this->isApproved();
		//} 
	} else {
		if(prAtSeconds3State == true) {
		countImpsbHorizontalTrafficLightRed--;
		prAtSeconds3State = false;
		}
	} 
}
bool rlHorizontalTrafficLightRed::isApproved() {
	bool ret = false;
	if((countsbHorizontalTrafficLightRed + countImpsbHorizontalTrafficLightRed) == 2) {
		this->semaphore_NOP->mtHorizontalTrafficLightRED();
		ret = true;
	} 
	return ret; 
 }
