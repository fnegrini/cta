#include "rlVerticalTrafficLightGreen.h"
#include "Semaphore_NOP.h"
rlVerticalTrafficLightGreen::rlVerticalTrafficLightGreen() {
	countsbVerticalTrafficLightGreen = 0;
	countImpsbVerticalTrafficLightGreen = 0;
	prSemaphoreState4State = false;
	prAtSeconds4State = false;
}
rlVerticalTrafficLightGreen::~rlVerticalTrafficLightGreen() {
}
void rlVerticalTrafficLightGreen::initprSemaphoreState4semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 2) {
		if(prSemaphoreState4State == false) {
		countsbVerticalTrafficLightGreen++;
		prSemaphoreState4State = true;
		}
	}
}
void rlVerticalTrafficLightGreen::notifyprSemaphoreState4semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 2) {
		if(prSemaphoreState4State == false) {
		countsbVerticalTrafficLightGreen++;
		prSemaphoreState4State = true;
		}
		//this->isApproved();
		//this->semaphore_NOP->atSecondsArray[3] = &this->notifyprAtSeconds4semaphore_NOPatSeconds;
		this->notifyprAtSeconds4semaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	} else {
		if(prSemaphoreState4State == true) {
		countsbVerticalTrafficLightGreen--;
		prSemaphoreState4State = false;
		}
	} 
}
void rlVerticalTrafficLightGreen::initprAtSeconds4semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 47) {
		if(prAtSeconds4State == false) {
		countImpsbVerticalTrafficLightGreen++;
		prAtSeconds4State = true;
		}
	}
}
void rlVerticalTrafficLightGreen::notifyprAtSeconds4semaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 47) {
		if(prAtSeconds4State == false) {
		countImpsbVerticalTrafficLightGreen++;
		prAtSeconds4State = true;
		}
		//if(countsbVerticalTrafficLightGreen == 1) { 
		this->isApproved();
		//} 
	} else {
		if(prAtSeconds4State == true) {
		countImpsbVerticalTrafficLightGreen--;
		prAtSeconds4State = false;
		}
	} 
}
bool rlVerticalTrafficLightGreen::isApproved() {
	bool ret = false;
	if((countsbVerticalTrafficLightGreen + countImpsbVerticalTrafficLightGreen) == 2) {
		this->semaphore_NOP->mtVerticalTrafficLightGREEN();
		ret = true;
	} 
	return ret; 
 }
