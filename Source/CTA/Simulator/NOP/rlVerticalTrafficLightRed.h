#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP;
class rlVerticalTrafficLightRed{
public:
	rlVerticalTrafficLightRed(void);
	~rlVerticalTrafficLightRed(void);
	bool isApproved();
	bool prSemaphoreState6State;
	void initprSemaphoreState6semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreState6semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	bool prAtSeconds6State;
	void initprAtSeconds6semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprAtSeconds6semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	int countsbVerticalTrafficLightRed;
	int countImpsbVerticalTrafficLightRed;
	Semaphore_NOP * semaphore_NOP;
	void (rlVerticalTrafficLightRed::*endereco)(int);
};
