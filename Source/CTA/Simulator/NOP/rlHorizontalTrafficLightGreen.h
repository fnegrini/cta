#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP;
class rlHorizontalTrafficLightGreen {
public:
	rlHorizontalTrafficLightGreen(void);
	~rlHorizontalTrafficLightGreen(void);
	bool isApproved();
	bool prSemaphoreStateState;
	void initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	int countsbHorizontalTrafficLightGreen;
	int countImpsbHorizontalTrafficLightGreen;
	Semaphore_NOP * semaphore_NOP;
};
