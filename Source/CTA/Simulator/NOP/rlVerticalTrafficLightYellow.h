#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP;
class rlVerticalTrafficLightYellow {
public:
	rlVerticalTrafficLightYellow(void);
	~rlVerticalTrafficLightYellow(void);
	bool isApproved();
	bool prSemaphoreState5State;
	void initprSemaphoreState5semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreState5semaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	bool prAtSeconds5State;
	void initprAtSeconds5semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprAtSeconds5semaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	int countsbVerticalTrafficLightYellow;
	int countImpsbVerticalTrafficLightYellow;
	Semaphore_NOP * semaphore_NOP;
};
