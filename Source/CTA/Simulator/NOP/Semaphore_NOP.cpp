#include "Semaphore_NOP.h"
#include "rlHorizontalTrafficLightGreen.h"
#include "rlHorizontalTrafficLightYellow.h"
#include "rlHorizontalTrafficLightRed.h"
#include "rlVerticalTrafficLightGreen.h"
#include "rlVerticalTrafficLightYellow.h"
#include "rlVerticalTrafficLightRed.h"
#include "../Traffic_Light.h"
//#include "Rule.h"
Semaphore_NOP::Semaphore_NOP(int semaphoreId, Traffic_Light* horizontalTrafficLight, Traffic_Light* verticalTrafficLight) :
Semaphore(semaphoreId, horizontalTrafficLight, verticalTrafficLight)
{

	this->rlrlHorizontalTrafficLightGreen = new rlHorizontalTrafficLightGreen();
	this->rlrlHorizontalTrafficLightYellow = new rlHorizontalTrafficLightYellow();
	this->rlrlHorizontalTrafficLightRed = new rlHorizontalTrafficLightRed();
	this->rlrlVerticalTrafficLightGreen = new rlVerticalTrafficLightGreen();
	this->rlrlVerticalTrafficLightYellow = new rlVerticalTrafficLightYellow();
	this->rlrlVerticalTrafficLightRed = new rlVerticalTrafficLightRed();

	rlrlHorizontalTrafficLightGreen->semaphore_NOP = this;
	rlrlHorizontalTrafficLightYellow->semaphore_NOP = this;
	rlrlHorizontalTrafficLightRed->semaphore_NOP = this;
	rlrlVerticalTrafficLightGreen->semaphore_NOP = this;
	rlrlVerticalTrafficLightYellow->semaphore_NOP = this;
	rlrlVerticalTrafficLightYellow->semaphore_NOP = this;
	rlrlVerticalTrafficLightRed->semaphore_NOP = this;

	atSeconds = 0;
	atSemaphoreState = 5;
	this->rlrlHorizontalTrafficLightGreen->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->rlrlHorizontalTrafficLightYellow->initprSeconds2semaphore_NOPatSeconds(this->atSeconds);
	this->rlrlHorizontalTrafficLightRed->initprAtSeconds3semaphore_NOPatSeconds(this->atSeconds);
	this->rlrlVerticalTrafficLightGreen->initprAtSeconds4semaphore_NOPatSeconds(this->atSeconds);
	this->rlrlVerticalTrafficLightYellow->initprAtSeconds5semaphore_NOPatSeconds(this->atSeconds);
	this->rlrlVerticalTrafficLightRed->initprAtSeconds6semaphore_NOPatSeconds(this->atSeconds);
	this->rlrlHorizontalTrafficLightGreen->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->rlrlHorizontalTrafficLightYellow->initprSemaphoreState2semaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->rlrlHorizontalTrafficLightRed->initprSemaphoreState3semaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->rlrlVerticalTrafficLightGreen->initprSemaphoreState4semaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->rlrlVerticalTrafficLightYellow->initprSemaphoreState5semaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->rlrlVerticalTrafficLightRed->initprSemaphoreState6semaphore_NOPatSemaphoreState(this->atSemaphoreState);
}
Semaphore_NOP::~Semaphore_NOP(void)
{
}
void Semaphore_NOP::setatSeconds(int atSeconds) {
	if (this->atSeconds != atSeconds) {
		this->atSeconds = atSeconds;

		if (this->rlrlVerticalTrafficLightRed->countsbVerticalTrafficLightRed = 1){			
			this->rlrlVerticalTrafficLightRed->notifyprAtSeconds6semaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlVerticalTrafficLightYellow->countsbVerticalTrafficLightYellow = 1){
			this->rlrlVerticalTrafficLightYellow->notifyprAtSeconds5semaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlVerticalTrafficLightGreen->countsbVerticalTrafficLightGreen = 1){
			this->rlrlVerticalTrafficLightGreen->notifyprAtSeconds4semaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlHorizontalTrafficLightRed->countsbHorizontalTrafficLightRed = 1){
			this->rlrlHorizontalTrafficLightRed->notifyprAtSeconds3semaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlHorizontalTrafficLightYellow->countsbHorizontalTrafficLightYellow = 1){
			this->rlrlHorizontalTrafficLightYellow->notifyprSeconds2semaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlHorizontalTrafficLightGreen->countsbHorizontalTrafficLightGreen = 1){
			this->rlrlHorizontalTrafficLightGreen->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
	}
}
int Semaphore_NOP::getatSeconds() {
	return this->atSeconds;
}
void Semaphore_NOP::setatSemaphoreState(int atSemaphoreState) {
	if (this->atSemaphoreState != atSemaphoreState) {
		this->atSemaphoreState = atSemaphoreState;
		this->rlrlVerticalTrafficLightRed->notifyprSemaphoreState6semaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->rlrlVerticalTrafficLightYellow->notifyprSemaphoreState5semaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->rlrlVerticalTrafficLightGreen->notifyprSemaphoreState4semaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->rlrlHorizontalTrafficLightRed->notifyprSemaphoreState3semaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->rlrlHorizontalTrafficLightYellow->notifyprSemaphoreState2semaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->rlrlHorizontalTrafficLightGreen->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	}
}
int Semaphore_NOP::getatSemaphoreState() {
	return this->atSemaphoreState;
}
void Semaphore_NOP::mtResetTimer() {
	//cout << "mtResetTimer Semaphore_NOP" << endl;
	setatSeconds(0);
	this->SetCurrentCycleTime(0);
}
void Semaphore_NOP::mtHorizontalTrafficLightGREEN() {
	//cout << "mtHorizontalTrafficLightGREEN Semaphore_NOP" << endl;
	setatSemaphoreState(0);
	this->GetHorizontalTrafficLight()->SetState(GREEN);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP::mtHorizontalTrafficLightYELLOW() {
	//cout << "mtHorizontalTrafficLightYELLOW Semaphore_NOP" << endl;
	setatSemaphoreState(1);
	this->GetHorizontalTrafficLight()->SetState(YELLOW);
	this->GetVerticalTrafficLight()->SetState(RED);

}
void Semaphore_NOP::mtHorizontalTrafficLightRED() {
	//cout << "mtHorizontalTrafficLightRED Semaphore_NOP" << endl;
	setatSemaphoreState(2);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(RED);

}
void Semaphore_NOP::mtVerticalTrafficLightGREEN() {
	//cout << "mtVerticalTrafficLightGREEN Semaphore_NOP" << endl;
	setatSemaphoreState(3);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(GREEN);
}
void Semaphore_NOP::mtVerticalTrafficLightYELLOW() {
	//cout << "mtVerticalTrafficLightYELLOW Semaphore_NOP" << endl;
	setatSemaphoreState(4);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(YELLOW);
}
void Semaphore_NOP::mtVerticalTrafficLightRED() {
	//cout << "mtVerticalTrafficLightRED Semaphore_NOP" << endl;
	setatSemaphoreState(5);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(RED);

}

void Semaphore_NOP::increaseCycleTime(){
	currentCycleTime += 1;
	setatSeconds(currentCycleTime);
}