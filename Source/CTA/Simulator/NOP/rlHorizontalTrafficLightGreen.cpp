#include "rlHorizontalTrafficLightGreen.h"
#include "Semaphore_NOP.h"
rlHorizontalTrafficLightGreen::rlHorizontalTrafficLightGreen() {
	countsbHorizontalTrafficLightGreen = 0;
	countImpsbHorizontalTrafficLightGreen = 0;
	prSemaphoreStateState = false;
	prSecondsState = false;
}
rlHorizontalTrafficLightGreen::~rlHorizontalTrafficLightGreen() {
}
void rlHorizontalTrafficLightGreen::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 5) {
		if(prSemaphoreStateState == false) {
		countsbHorizontalTrafficLightGreen++;
		prSemaphoreStateState = true;
		}
	}
}
void rlHorizontalTrafficLightGreen::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 5) {
		if(prSemaphoreStateState == false) {
		countsbHorizontalTrafficLightGreen++;
		prSemaphoreStateState = true;
		}
		//this->isApproved();
		//this->semaphore_NOP->atSecondsArray[0] = &this->notifyprSecondssemaphore_NOPatSeconds;
		this->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	} else {
		if(prSemaphoreStateState == true) {
		countsbHorizontalTrafficLightGreen--;
		prSemaphoreStateState = false;
		}
	} 
}
void rlHorizontalTrafficLightGreen::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 2) {
		if(prSecondsState == false) {
		countImpsbHorizontalTrafficLightGreen++;
		prSecondsState = true;
		}
	}
}
void rlHorizontalTrafficLightGreen::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 2) {
		if(prSecondsState == false) {
		countImpsbHorizontalTrafficLightGreen++;
		prSecondsState = true;
		}
		//if(countsbHorizontalTrafficLightGreen == 1) { 
		this->isApproved();
		//} 
	} else {
		if(prSecondsState == true) {
		countImpsbHorizontalTrafficLightGreen--;
		prSecondsState = false;
		}
	} 
}
bool rlHorizontalTrafficLightGreen::isApproved() {
	bool ret = false;
	if((countsbHorizontalTrafficLightGreen + countImpsbHorizontalTrafficLightGreen) == 2) {
		this->semaphore_NOP->mtHorizontalTrafficLightGREEN();
		ret = true;
	} 
	return ret; 
 }
