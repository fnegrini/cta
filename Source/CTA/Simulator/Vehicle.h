///////////////////////////////////////////////////////////
//  Vehicle.h
//  Implementation of the Class Vehicle
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once
#include "Vehicle_Block.h"

#include <iostream>
#include <list>
using namespace std;

class Vehicle
{

public:
	Vehicle(int pId, const int tVelocity = 20,const int tLength = 4);
	virtual ~Vehicle();

	int GetId();
	int GetLength();
	int GetTravelledDistance();
	int GetVelocity();	
	bool GetIsNewInTheBlock();
	Vehicle_Block* GetVehicleBlock();
	void SetId(int newVal);
	void SetLength(int newVal);
	void SetTravelledDistance(int newVal);
	void SetVelocity(int newVal);	
	void SetVehicleBlock(Vehicle_Block* newVal);
	void SetIsNewInTheBlock(bool pIsNew);
	void move(list<Vehicle_Block*>* pListVehicle);
	static int getCount();

private:
	int id;
	int length;
	int travelledDistance;
	int velocity;
	Vehicle_Block *vehicleBlock;
	static int vehicleCounter;
	bool isNewInTheBlock;
	int MoveLenght(list<Vehicle_Block*>* pListVehicle);
};
