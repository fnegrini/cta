///////////////////////////////////////////////////////////
//  Vehicle_Sensor.h
//  Implementation of the Class Vehicle_Sensor
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Semaphore.h"
#include "Vehicle_Sensor_State.h"

class Block;

class Vehicle_Sensor
{

public:
	Vehicle_Sensor(Block* pBlock);
	virtual ~Vehicle_Sensor();

	Vehicle_Sensor_State GetVehicleSensorState();
	void notify(Semaphore* semaphore, int state);
	void SetVehicleSensorState(Vehicle_Sensor_State newVal);
	void update(float blockOccupancyRate);
	Block* GetBlock();

private:
	Vehicle_Sensor_State vehicleSensorState;
	Block* block;

};
