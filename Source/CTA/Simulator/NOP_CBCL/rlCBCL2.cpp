#include "rlCBCL2.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds2.h"
rlCBCL2::rlCBCL2() {
	countsbCBCL2 = 0;
	countImpsbCBCL2 = 0;
}
rlCBCL2::~rlCBCL2() {
}
void rlCBCL2::incCountsbCBCL2(){
	this->countsbCBCL2++;
	//isApproved();
	this->prprAtSeconds2->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL2::decCountsbCBCL2(){
	this->countsbCBCL2--;
}
void rlCBCL2::incCountImpsbCBCL2(){
	this->countImpsbCBCL2++;
	isApproved();
}
void rlCBCL2::decCountImpsbCBCL2(){
	this->countImpsbCBCL2--;
}
bool rlCBCL2::isApproved() {
	bool ret = false;
	if((countsbCBCL2 + countImpsbCBCL2) == 2) {
		this->semaphore_NOP->mtHTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
