#include "prAtSemaphoreState1.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL4.h"

prAtSemaphoreState1::prAtSemaphoreState1(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL4 *rlrlCBCL4) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL4 = rlrlCBCL4;
	this->rlrlCBCL4->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState1::~prAtSemaphoreState1() {
}
void prAtSemaphoreState1::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL4->incCountsbCBCL4();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState1::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL4->incCountsbCBCL4();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL4->decCountsbCBCL4();
			prSemaphoreStateState = false;
		}
	}
}