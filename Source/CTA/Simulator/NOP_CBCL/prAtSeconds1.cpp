#include "prAtSeconds1.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL1.h"
#include "rlCBCL6.h"

prAtSeconds1::prAtSeconds1(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL1 *rlrlCBCL1, rlCBCL6 *rlrlCBCL6) {
	this->semaphore_NOP = semaphore_NOP_CBCL;
	
	prSecondsState = false;

	this->rlrlCBCL1 = rlrlCBCL1;
	this->rlrlCBCL1->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL6 = rlrlCBCL6;
	this->rlrlCBCL6->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds1::~prAtSeconds1() {
}
void prAtSeconds1::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 2) {
		if (prSecondsState == false) {
			this->rlrlCBCL1->incCountImpsbCBCL1();
			this->rlrlCBCL6->incCountImpsbCBCL6();
			prSecondsState = true;
		}
	}
}
void prAtSeconds1::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 2) {
		if (prSecondsState == false) {
			this->rlrlCBCL1->incCountImpsbCBCL1();
			this->rlrlCBCL6->incCountImpsbCBCL6();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL1->decCountImpsbCBCL1();
			this->rlrlCBCL6->decCountImpsbCBCL6();
			prSecondsState = false;
		}
	}
}