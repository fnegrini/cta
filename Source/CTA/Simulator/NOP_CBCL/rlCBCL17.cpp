#include "rlCBCL17.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds7.h"
#include "prAtSeconds8.h"
#include "prAtHVSS1.h"
rlCBCL17::rlCBCL17() {
	countsbCBCL17 = 0;
	countImpsbCBCL17 = 0;
}
rlCBCL17::~rlCBCL17() {
}
void rlCBCL17::incCountsbCBCL17(){
	this->countsbCBCL17++;
	//isApproved();
	this->prprAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtHVSS1->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atHVSS);
}
void rlCBCL17::decCountsbCBCL17(){
	this->countsbCBCL17--;
}
void rlCBCL17::incCountImpsbCBCL17(){
	this->countImpsbCBCL17++;
	isApproved();
}
void rlCBCL17::decCountImpsbCBCL17(){
	this->countImpsbCBCL17--;
}
bool rlCBCL17::isApproved() {
	bool ret = false;
	if((countsbCBCL17 + countImpsbCBCL17) == 4) {
		this->semaphore_NOP->mtVTLYCBCL();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
