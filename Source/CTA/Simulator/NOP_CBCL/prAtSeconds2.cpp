#include "prAtSeconds2.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL2.h"
#include "rlCBCL7.h"

prAtSeconds2::prAtSeconds2(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL2 *rlrlCBCL2, rlCBCL7 *rlrlCBCL7) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSecondsState = false;

	this->rlrlCBCL2 = rlrlCBCL2;
	this->rlrlCBCL2->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL7 = rlrlCBCL7;
	this->rlrlCBCL7->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds2::~prAtSeconds2() {
}
void prAtSeconds2::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 38) {
		if (prSecondsState == false) {
			this->rlrlCBCL2->incCountImpsbCBCL2();
			this->rlrlCBCL7->incCountImpsbCBCL7();
			prSecondsState = true;
		}
	}
}
void prAtSeconds2::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 38) {
		if (prSecondsState == false) {
			this->rlrlCBCL2->incCountImpsbCBCL2();
			this->rlrlCBCL7->incCountImpsbCBCL7();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL2->decCountImpsbCBCL2();
			this->rlrlCBCL7->decCountImpsbCBCL7();
			prSecondsState = false;
		}
	}
}