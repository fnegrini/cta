#include "rlCBCL16.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds6.h"
#include "prAtVVSS2.h"
rlCBCL16::rlCBCL16() {
	countsbCBCL16 = 0;
	countImpsbCBCL16 = 0;
}
rlCBCL16::~rlCBCL16() {
}
void rlCBCL16::incCountsbCBCL16(){
	this->countsbCBCL16++;
	//isApproved();
	this->prprAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtVVSS2->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atHVSS);
}
void rlCBCL16::decCountsbCBCL16(){
	this->countsbCBCL16--;
}
void rlCBCL16::incCountImpsbCBCL16(){
	this->countImpsbCBCL16++;
	isApproved();
}
void rlCBCL16::decCountImpsbCBCL16(){
	this->countImpsbCBCL16--;
}
bool rlCBCL16::isApproved() {
	bool ret = false;
	if((countsbCBCL16 + countImpsbCBCL16) == 3) {
		this->semaphore_NOP->mtVTLGCBCL();
		ret = true;
	} 
	return ret; 
 }
