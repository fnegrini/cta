#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class prAtSeconds6;
class prAtVVSS2;
class rlCBCL16 {
public:
	rlCBCL16(void);
	~rlCBCL16(void);
	bool isApproved();
	int countsbCBCL16;
	int countImpsbCBCL16;
	void incCountsbCBCL16();
	void decCountsbCBCL16();
	void incCountImpsbCBCL16();
	void decCountImpsbCBCL16();
	Semaphore_NOP_CBCL * semaphore_NOP;
	prAtSeconds6 *prprAtSeconds6;
	prAtVVSS2 *prprAtVVSS2;
};
