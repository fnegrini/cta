#include "prAtSemaphoreState9.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL10.h"

prAtSemaphoreState9::prAtSemaphoreState9(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL10 *rlrlCBCL10) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL10 = rlrlCBCL10;
	this->rlrlCBCL10->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState9::~prAtSemaphoreState9() {
}
void prAtSemaphoreState9::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 9) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL10->incCountsbCBCL10();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState9::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 9) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL10->incCountsbCBCL10();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL10->decCountsbCBCL10();
			prSemaphoreStateState = false;
		}
	}
}