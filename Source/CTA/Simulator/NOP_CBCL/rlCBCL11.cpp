#include "rlCBCL11.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds6.h"
#include "prAtHVSS1.h"
rlCBCL11::rlCBCL11() {
	countsbCBCL11 = 0;
	countImpsbCBCL11 = 0;
}
rlCBCL11::~rlCBCL11() {
}
void rlCBCL11::incCountsbCBCL11(){
	this->countsbCBCL11++;
	//isApproved
	this->prprAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtHVSS1->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atVVSS);
}
void rlCBCL11::decCountsbCBCL11(){
	this->countsbCBCL11--;
}
void rlCBCL11::incCountImpsbCBCL11(){
	this->countImpsbCBCL11++;
	isApproved();
}
void rlCBCL11::decCountImpsbCBCL11(){
	this->countImpsbCBCL11--;
}
bool rlCBCL11::isApproved() {
	bool ret = false;
	if((countsbCBCL11 + countImpsbCBCL11) == 3) {
		this->semaphore_NOP->mtHTLGCBCL();
		ret = true;
	} 
	return ret; 
 }
