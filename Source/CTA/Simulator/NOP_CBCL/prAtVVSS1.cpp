#include "prAtVVSS1.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL15.h"
#include "rlCBCL13.h"

prAtVVSS1::prAtVVSS1(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL15 *rlrlCBCL15, rlCBCL13 *rlrlCBCL13) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prVehicleSensorStateState = false;

	this->rlrlCBCL15 = rlrlCBCL15;
	this->rlrlCBCL15->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL13 = rlrlCBCL13;
	this->rlrlCBCL13->semaphore_NOP = this->semaphore_NOP;
}
prAtVVSS1::~prAtVVSS1() {
}
void prAtVVSS1::initprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL15->incCountImpsbCBCL15();
			this->rlrlCBCL13->incCountImpsbCBCL13();			
			prVehicleSensorStateState = true;
		}
	}
}
void prAtVVSS1::notifyprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL15->incCountImpsbCBCL15();
			this->rlrlCBCL13->incCountImpsbCBCL13();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBCL15->decCountImpsbCBCL15();
			this->rlrlCBCL13->decCountImpsbCBCL13();
			prVehicleSensorStateState = false;
		}
	}
}