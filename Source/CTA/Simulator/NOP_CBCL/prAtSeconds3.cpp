#include "prAtSeconds3.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL3.h"
#include "rlCBCL8.h"

prAtSeconds3::prAtSeconds3(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL3 *rlrlCBCL3, rlCBCL8 *rlrlCBCL8) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSecondsState = false;

	this->rlrlCBCL3 = rlrlCBCL3;
	this->rlrlCBCL3->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBCL8 = rlrlCBCL8;
	this->rlrlCBCL8->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds3::~prAtSeconds3() {
}
void prAtSeconds3::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 30) {
		if (prSecondsState == false) {
			this->rlrlCBCL3->incCountImpsbCBCL3();
			this->rlrlCBCL8->incCountImpsbCBCL8();
			prSecondsState = true;
		}
	}
}
void prAtSeconds3::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 30) {
		if (prSecondsState == false) {
			this->rlrlCBCL3->incCountImpsbCBCL3();
			this->rlrlCBCL8->incCountImpsbCBCL8();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL3->decCountImpsbCBCL3();
			this->rlrlCBCL8->decCountImpsbCBCL8();
			prSecondsState = false;
		}
	}
}