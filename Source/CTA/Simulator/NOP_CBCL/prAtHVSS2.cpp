#include "prAtHVSS2.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL12.h"
#include "rlCBCL18.h"

prAtHVSS2::prAtHVSS2(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL12 *rlrlCBCL12, rlCBCL18 *rlrlCBCL18) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prVehicleSensorStateState = false;

	this->rlrlCBCL12 = rlrlCBCL12;
	this->rlrlCBCL12->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL18 = rlrlCBCL18;
	this->rlrlCBCL18->semaphore_NOP = this->semaphore_NOP;
}
prAtHVSS2::~prAtHVSS2() {
}
void prAtHVSS2::initprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL12->incCountImpsbCBCL12();
			this->rlrlCBCL18->incCountImpsbCBCL18();
			prVehicleSensorStateState = true;
		}
	}
}
void prAtHVSS2::notifyprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL12->incCountImpsbCBCL12();
			this->rlrlCBCL18->incCountImpsbCBCL18();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBCL12->decCountImpsbCBCL12();
			this->rlrlCBCL18->decCountImpsbCBCL18();
			prVehicleSensorStateState = false;
		}
	}
}