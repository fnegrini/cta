#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL5;
class rlCBCL10;
class prAtSeconds5 {
public:
	prAtSeconds5(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL5 *rlrlCBCL5, rlCBCL10 *rlrlCBCL10);
	~prAtSeconds5(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL5 *rlrlCBCL5;
	rlCBCL10 *rlrlCBCL10;
};
