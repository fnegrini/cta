#include "rlCBCL15.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds6.h"
#include "prAtVVSS1.h"
rlCBCL15::rlCBCL15() {
	countsbCBCL15 = 0;
	countImpsbCBCL15 = 0;
}
rlCBCL15::~rlCBCL15() {
}
void rlCBCL15::incCountsbCBCL15(){
	this->countsbCBCL15++;
	//isApproved();
	this->prprAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtVVSS1->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atHVSS);
}
void rlCBCL15::decCountsbCBCL15(){
	this->countsbCBCL15--;
}
void rlCBCL15::incCountImpsbCBCL15(){
	this->countImpsbCBCL15++;
	isApproved();
}
void rlCBCL15::decCountImpsbCBCL15(){
	this->countImpsbCBCL15--;
}
bool rlCBCL15::isApproved() {
	bool ret = false;
	if((countsbCBCL15 + countImpsbCBCL15) == 3) {
		this->semaphore_NOP->mtVTLGCBCL();
		ret = true;
	} 
	return ret; 
 }
