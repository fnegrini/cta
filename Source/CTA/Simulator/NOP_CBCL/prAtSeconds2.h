#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL2;
class rlCBCL7;
class prAtSeconds2 {
public:
	prAtSeconds2(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL2 *rlrlCBCL2, rlCBCL7 *rlrlCBCL7);
	~prAtSeconds2(void);	
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL2 *rlrlCBCL2;
	rlCBCL7 *rlrlCBCL7;
};
