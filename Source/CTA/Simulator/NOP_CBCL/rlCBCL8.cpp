#include "rlCBCL8.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds3.h"
rlCBCL8::rlCBCL8() {
	countsbCBCL8 = 0;
	countImpsbCBCL8 = 0;
}
rlCBCL8::~rlCBCL8() {
}
void rlCBCL8::incCountsbCBCL8(){
	this->countsbCBCL8++;
	//isApproved();
	this->prprAtSeconds3->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL8::decCountsbCBCL8(){
	this->countsbCBCL8--;
}
void rlCBCL8::incCountImpsbCBCL8(){
	this->countImpsbCBCL8++;
	isApproved();
}
void rlCBCL8::decCountImpsbCBCL8(){
	this->countImpsbCBCL8--;
}
bool rlCBCL8::isApproved() {
	bool ret = false;
	if((countsbCBCL8 + countImpsbCBCL8) == 2) {
		this->semaphore_NOP->mtVTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
