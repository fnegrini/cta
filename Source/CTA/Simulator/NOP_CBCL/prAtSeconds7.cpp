#include "prAtSeconds7.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL13.h"
#include "rlCBCL14.h"
#include "rlCBCL17.h"
#include "rlCBCL18.h"

prAtSeconds7::prAtSeconds7(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL13 *rlrlCBCL13, rlCBCL14 *rlrlCBCL14, rlCBCL17 *rlrlCBCL17, rlCBCL18 *rlrlCBCL18) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSecondsState = false;

	this->rlrlCBCL13 = rlrlCBCL13;
	this->rlrlCBCL13->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL14 = rlrlCBCL14;
	this->rlrlCBCL14->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL17 = rlrlCBCL17;
	this->rlrlCBCL17->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL18 = rlrlCBCL18;
	this->rlrlCBCL18->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds7::~prAtSeconds7() {
}
void prAtSeconds7::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds >= 18) {
		if (prSecondsState == false) {
			this->rlrlCBCL13->incCountImpsbCBCL13();
			this->rlrlCBCL14->incCountImpsbCBCL14();
			this->rlrlCBCL17->incCountImpsbCBCL17();
			this->rlrlCBCL18->incCountImpsbCBCL18();
			prSecondsState = true;
		}
	}
}
void prAtSeconds7::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds >= 18) {
		if (prSecondsState == false) {
			this->rlrlCBCL13->incCountImpsbCBCL13();
			this->rlrlCBCL14->incCountImpsbCBCL14();
			this->rlrlCBCL17->incCountImpsbCBCL17();
			this->rlrlCBCL18->incCountImpsbCBCL18();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL13->decCountImpsbCBCL13();
			this->rlrlCBCL14->decCountImpsbCBCL14();
			this->rlrlCBCL17->decCountImpsbCBCL17();
			this->rlrlCBCL18->decCountImpsbCBCL18();
			prSecondsState = false;
		}
	}
}