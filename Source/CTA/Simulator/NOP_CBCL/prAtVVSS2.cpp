#include "prAtVVSS2.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL16.h"
#include "rlCBCL14.h"

prAtVVSS2::prAtVVSS2(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL16 *rlrlCBCL16, rlCBCL14 *rlrlCBCL14) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prVehicleSensorStateState = false;

	this->rlrlCBCL16 = rlrlCBCL16;
	this->rlrlCBCL16->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL14 = rlrlCBCL14;
	this->rlrlCBCL14->semaphore_NOP = this->semaphore_NOP;
}
prAtVVSS2::~prAtVVSS2() {
}
void prAtVVSS2::initprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 2) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL16->incCountImpsbCBCL16();
			this->rlrlCBCL14->incCountImpsbCBCL14();
			prVehicleSensorStateState = true;
		}
	}
}
void prAtVVSS2::notifyprVehicleSensorStatesemaphore_NOPatVVSS(int semaphore_NOPatVVSS) {
	if (semaphore_NOPatVVSS == 2) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL16->incCountImpsbCBCL16();
			this->rlrlCBCL14->incCountImpsbCBCL14();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBCL16->decCountImpsbCBCL16();
			this->rlrlCBCL14->decCountImpsbCBCL14();
			prVehicleSensorStateState = false;
		}
	}
}