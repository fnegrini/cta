#include "rlCBCL3.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds3.h"
rlCBCL3::rlCBCL3() {
	countsbCBCL3 = 0;
	countImpsbCBCL3 = 0;
}
rlCBCL3::~rlCBCL3() {
}
void rlCBCL3::incCountsbCBCL3(){
	this->countsbCBCL3++;
	//isApproved();
	this->prprAtSeconds3->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL3::decCountsbCBCL3(){
	this->countsbCBCL3--;
}
void rlCBCL3::incCountImpsbCBCL3(){
	this->countImpsbCBCL3++;
	isApproved();
}
void rlCBCL3::decCountImpsbCBCL3(){
	this->countImpsbCBCL3--;
}
bool rlCBCL3::isApproved() {
	bool ret = false;
	if((countsbCBCL3 + countImpsbCBCL3) == 2) {
		this->semaphore_NOP->mtHTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
