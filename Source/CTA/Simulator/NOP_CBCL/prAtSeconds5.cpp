#include "prAtSeconds5.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL5.h"
#include "rlCBCL10.h"

prAtSeconds5::prAtSeconds5(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL5 *rlrlCBCL5, rlCBCL10 *rlrlCBCL10) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSecondsState = false;

	this->rlrlCBCL5 = rlrlCBCL5;
	this->rlrlCBCL5->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL10 = rlrlCBCL10;
	this->rlrlCBCL10->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds5::~prAtSeconds5() {
}
void prAtSeconds5::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 6) {
		if (prSecondsState == false) {
			this->rlrlCBCL5->incCountImpsbCBCL5();
			this->rlrlCBCL10->incCountImpsbCBCL10();
			prSecondsState = true;
		}
	}
}
void prAtSeconds5::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 6) {
		if (prSecondsState == false) {
			this->rlrlCBCL5->incCountImpsbCBCL5();
			this->rlrlCBCL10->incCountImpsbCBCL10();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL5->decCountImpsbCBCL5();
			this->rlrlCBCL10->decCountImpsbCBCL10();
			prSecondsState = false;
		}
	}
}