#include "prAtSemaphoreState3.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL7.h"
#include "rlCBCL15.h"
#include "rlCBCL16.h"
#include "rlCBCL17.h"
#include "rlCBCL18.h"

prAtSemaphoreState3::prAtSemaphoreState3(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL7 *rlrlCBCL7, rlCBCL15 *rlrlCBCL15, rlCBCL16 *rlrlCBCL16, rlCBCL17 *rlrlCBCL17, rlCBCL18 *rlrlCBCL18) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL7 = rlrlCBCL7;
	this->rlrlCBCL7->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBCL15 = rlrlCBCL15;
	this->rlrlCBCL15->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBCL16 = rlrlCBCL16;
	this->rlrlCBCL16->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBCL17 = rlrlCBCL17;
	this->rlrlCBCL17->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBCL18 = rlrlCBCL18;
	this->rlrlCBCL18->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState3::~prAtSemaphoreState3() {
}
void prAtSemaphoreState3::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 3) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL7->incCountsbCBCL7();
			this->rlrlCBCL15->incCountsbCBCL15();
			this->rlrlCBCL16->incCountsbCBCL16();
			this->rlrlCBCL17->incCountsbCBCL17();
			this->rlrlCBCL18->incCountsbCBCL18();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState3::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 3) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL7->incCountsbCBCL7();
			this->rlrlCBCL15->incCountsbCBCL15();
			this->rlrlCBCL16->incCountsbCBCL16();
			this->rlrlCBCL17->incCountsbCBCL17();
			this->rlrlCBCL18->incCountsbCBCL18();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL7->decCountsbCBCL7();
			this->rlrlCBCL15->decCountsbCBCL15();
			this->rlrlCBCL16->decCountsbCBCL16();
			this->rlrlCBCL17->decCountsbCBCL17();
			this->rlrlCBCL18->decCountsbCBCL18();
			prSemaphoreStateState = false;
		}
	}
}