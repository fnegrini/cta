#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL4;
class rlCBCL9;
class prAtSeconds4 {
public:
	prAtSeconds4(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL4 *rlrlCBCL4, rlCBCL9 *rlrlCBCL9);
	~prAtSeconds4(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL4 *rlrlCBCL4;
	rlCBCL9 *rlrlCBCL9;
};
