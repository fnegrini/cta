#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL1;
class rlCBCL6;
class prAtSeconds1 {
public:
	prAtSeconds1(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL1 *rlrlCBCL1, rlCBCL6 *rlrlCBCL6);
	~prAtSeconds1(void);	
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);	
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL1 *rlrlCBCL1;
	rlCBCL6 *rlrlCBCL6;
};
