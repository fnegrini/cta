#include "prAtSemaphoreState4.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL9.h"

prAtSemaphoreState4::prAtSemaphoreState4(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL9 *rlrlCBCL9) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL9 = rlrlCBCL9;
	this->rlrlCBCL9->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState4::~prAtSemaphoreState4() {
}
void prAtSemaphoreState4::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 4) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL9->incCountsbCBCL9();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState4::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 4) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL9->incCountsbCBCL9();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL9->decCountsbCBCL9();
			prSemaphoreStateState = false;
		}
	}
}