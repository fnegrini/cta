#include "prAtSemaphoreState5.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL1.h"

prAtSemaphoreState5::prAtSemaphoreState5(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL1 *rlrlCBCL1) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL1 = rlrlCBCL1;
	this->rlrlCBCL1->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState5::~prAtSemaphoreState5() {
}
void prAtSemaphoreState5::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 5) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL1->incCountsbCBCL1();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState5::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 5) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL1->incCountsbCBCL1();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL1->decCountsbCBCL1();
			prSemaphoreStateState = false;
		}
	}
}