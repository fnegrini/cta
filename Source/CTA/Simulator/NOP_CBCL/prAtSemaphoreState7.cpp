#include "prAtSemaphoreState7.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL5.h"

prAtSemaphoreState7::prAtSemaphoreState7(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL5 *rlrlCBCL5) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL5 = rlrlCBCL5;
	this->rlrlCBCL5->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState7::~prAtSemaphoreState7() {
}
void prAtSemaphoreState7::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 7) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL5->incCountsbCBCL5();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState7::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 7) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL5->incCountsbCBCL5();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL5->decCountsbCBCL5();
			prSemaphoreStateState = false;
		}
	}
}