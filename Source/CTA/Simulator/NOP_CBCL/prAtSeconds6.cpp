#include "prAtSeconds6.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL11.h"
#include "rlCBCL12.h"
#include "rlCBCL15.h"
#include "rlCBCL16.h"

prAtSeconds6::prAtSeconds6(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL11 *rlrlCBCL11, rlCBCL12 *rlrlCBCL12, rlCBCL15 *rlrlCBCL15, rlCBCL16 *rlrlCBCL16) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSecondsState = false;

	this->rlrlCBCL11 = rlrlCBCL11;
	this->rlrlCBCL11->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL12 = rlrlCBCL12;
	this->rlrlCBCL12->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL15 = rlrlCBCL15;
	this->rlrlCBCL15->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL16 = rlrlCBCL16;
	this->rlrlCBCL16->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds6::~prAtSeconds6() {
}
void prAtSeconds6::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds <= 17) {
		if (prSecondsState == false) {
			this->rlrlCBCL11->incCountImpsbCBCL11();
			this->rlrlCBCL12->incCountImpsbCBCL12();
			this->rlrlCBCL15->incCountImpsbCBCL15();
			this->rlrlCBCL16->incCountImpsbCBCL16();
			prSecondsState = true;
		}
	}
}
void prAtSeconds6::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds <= 17) {
		if (prSecondsState == false) {
			this->rlrlCBCL11->incCountImpsbCBCL11();
			this->rlrlCBCL12->incCountImpsbCBCL12();
			this->rlrlCBCL15->incCountImpsbCBCL15();
			this->rlrlCBCL16->incCountImpsbCBCL16();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL11->decCountImpsbCBCL11();
			this->rlrlCBCL12->decCountImpsbCBCL12();
			this->rlrlCBCL15->decCountImpsbCBCL15();
			this->rlrlCBCL16->decCountImpsbCBCL16();
			prSecondsState = false;
		}
	}
}