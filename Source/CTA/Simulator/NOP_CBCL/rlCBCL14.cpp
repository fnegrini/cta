#include "rlCBCL14.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds7.h"
#include "prAtSeconds8.h"
#include "prAtVVSS2.h"
rlCBCL14::rlCBCL14() {
	countsbCBCL14 = 0;
	countImpsbCBCL14 = 0;
}
rlCBCL14::~rlCBCL14() {
}
void rlCBCL14::incCountsbCBCL14(){
	this->countsbCBCL14++;
	//isApproved();
	this->prprAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtVVSS2->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atVVSS);
}
void rlCBCL14::decCountsbCBCL14(){
	this->countsbCBCL14--;
}
void rlCBCL14::incCountImpsbCBCL14(){
	this->countImpsbCBCL14++;
	isApproved();
}
void rlCBCL14::decCountImpsbCBCL14(){
	this->countImpsbCBCL14--;
}
bool rlCBCL14::isApproved() {
	bool ret = false;
	if((countsbCBCL14 + countImpsbCBCL14) == 4) {
		this->semaphore_NOP->mtHTLYCBCL();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
