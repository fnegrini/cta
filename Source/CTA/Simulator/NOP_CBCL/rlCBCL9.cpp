#include "rlCBCL9.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds4.h"
rlCBCL9::rlCBCL9() {
	countsbCBCL9 = 0;
	countImpsbCBCL9 = 0;
}
rlCBCL9::~rlCBCL9() {
}
void rlCBCL9::incCountsbCBCL9(){
	this->countsbCBCL9++;
	//isApproved();
	this->prprAtSeconds4->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL9::decCountsbCBCL9(){
	this->countsbCBCL9--;
}
void rlCBCL9::incCountImpsbCBCL9(){
	this->countImpsbCBCL9++;
	isApproved();
}
void rlCBCL9::decCountImpsbCBCL9(){
	this->countImpsbCBCL9--;
}
bool rlCBCL9::isApproved() {
	bool ret = false;
	if((countsbCBCL9 + countImpsbCBCL9) == 2) {
		this->semaphore_NOP->mtVTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
