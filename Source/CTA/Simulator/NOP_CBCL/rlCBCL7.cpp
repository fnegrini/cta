#include "rlCBCL7.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds2.h"
rlCBCL7::rlCBCL7() {
	countsbCBCL7 = 0;
	countImpsbCBCL7 = 0;
}
rlCBCL7::~rlCBCL7() {
}
void rlCBCL7::incCountsbCBCL7(){
	this->countsbCBCL7++;
	//isApproved();
	this->prprAtSeconds2->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL7::decCountsbCBCL7(){
	this->countsbCBCL7--;
}
void rlCBCL7::incCountImpsbCBCL7(){
	this->countImpsbCBCL7++;
	isApproved();
}
void rlCBCL7::decCountImpsbCBCL7(){
	this->countImpsbCBCL7--;
}
bool rlCBCL7::isApproved() {
	bool ret = false;
	if((countsbCBCL7 + countImpsbCBCL7) == 2) {
		this->semaphore_NOP->mtVTLY();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
