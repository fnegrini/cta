#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL2;
class rlCBCL11;
class rlCBCL12;
class rlCBCL13;
class rlCBCL14;
class prAtSemaphoreState0 {
public:
	prAtSemaphoreState0(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL2 *rlrlCBCL2, rlCBCL11 *rlrlCBCL11, rlCBCL12 *rlrlCBCL12, rlCBCL13 *rlrlCBCL13, rlCBCL14 *rlrlCBCL14);
	~prAtSemaphoreState0(void);
	bool prSemaphoreStateState;
	void initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL2 *rlrlCBCL2;
	rlCBCL11 *rlrlCBCL11;
	rlCBCL12 *rlrlCBCL12;
	rlCBCL13 *rlrlCBCL13;
	rlCBCL14 *rlrlCBCL14;
};
