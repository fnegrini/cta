#include "rlCBCL12.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds6.h"
#include "prAtHVSS2.h"
rlCBCL12::rlCBCL12() {
	countsbCBCL12 = 0;
	countImpsbCBCL12 = 0;
}
rlCBCL12::~rlCBCL12() {
}
void rlCBCL12::incCountsbCBCL12(){
	this->countsbCBCL12++;
	//isApproved();
	this->prprAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtHVSS2->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atVVSS);
}
void rlCBCL12::decCountsbCBCL12(){
	this->countsbCBCL12--;
}
void rlCBCL12::incCountImpsbCBCL12(){
	this->countImpsbCBCL12++;
	isApproved();
}
void rlCBCL12::decCountImpsbCBCL12(){
	this->countImpsbCBCL12--;
}
bool rlCBCL12::isApproved() {
	bool ret = false;
	if((countsbCBCL12 + countImpsbCBCL12) == 3) {
		this->semaphore_NOP->mtHTLGCBCL();
		ret = true;
	} 
	return ret; 
 }
