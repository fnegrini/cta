#include "prAtSeconds4.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL4.h"
#include "rlCBCL9.h"

prAtSeconds4::prAtSeconds4(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL4 *rlrlCBCL4, rlCBCL9 *rlrlCBCL9) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSecondsState = false;

	this->rlrlCBCL4 = rlrlCBCL4;
	this->rlrlCBCL4->semaphore_NOP = this->semaphore_NOP;

	this->rlrlCBCL9 = rlrlCBCL9;
	this->rlrlCBCL9->semaphore_NOP = this->semaphore_NOP;
}
prAtSeconds4::~prAtSeconds4() {
}
void prAtSeconds4::initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 5) {
		if (prSecondsState == false) {
			this->rlrlCBCL4->incCountImpsbCBCL4();
			this->rlrlCBCL9->incCountImpsbCBCL9();
			prSecondsState = true;
		}
	}
}
void prAtSeconds4::notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds) {
	if (semaphore_NOPatSeconds == 5) {
		if (prSecondsState == false) {
			this->rlrlCBCL4->incCountImpsbCBCL4();
			this->rlrlCBCL9->incCountImpsbCBCL9();
			prSecondsState = true;
		}
	}
	else {
		if (prSecondsState == true) {
			this->rlrlCBCL4->decCountImpsbCBCL4();
			this->rlrlCBCL9->decCountImpsbCBCL9();
			prSecondsState = false;
		}
	}
}