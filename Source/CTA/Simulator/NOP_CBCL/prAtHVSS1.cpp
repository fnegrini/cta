#include "prAtHVSS1.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL11.h"
#include "rlCBCL17.h"

prAtHVSS1::prAtHVSS1(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL11 *rlrlCBCL11, rlCBCL17 *rlrlCBCL17) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prVehicleSensorStateState = false;

	this->rlrlCBCL11 = rlrlCBCL11;
	this->rlrlCBCL11->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL17 = rlrlCBCL17;
	this->rlrlCBCL17->semaphore_NOP = this->semaphore_NOP;
}
prAtHVSS1::~prAtHVSS1() {
}
void prAtHVSS1::initprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL11->incCountImpsbCBCL11();
			this->rlrlCBCL17->incCountImpsbCBCL17();
			prVehicleSensorStateState = true;
		}
	}
}
void prAtHVSS1::notifyprVehicleSensorStatesemaphore_NOPatHVSS(int semaphore_NOPatHVSS) {
	if (semaphore_NOPatHVSS == 1) {
		if (prVehicleSensorStateState == false) {
			this->rlrlCBCL11->incCountImpsbCBCL11();
			this->rlrlCBCL17->incCountImpsbCBCL17();
			prVehicleSensorStateState = true;
		}
	}
	else {
		if (prVehicleSensorStateState == true) {
			this->rlrlCBCL11->decCountImpsbCBCL11();
			this->rlrlCBCL17->decCountImpsbCBCL17();
			prVehicleSensorStateState = false;
		}
	}
}