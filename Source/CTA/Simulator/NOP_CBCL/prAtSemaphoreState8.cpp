#include "prAtSemaphoreState8.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL8.h"

prAtSemaphoreState8::prAtSemaphoreState8(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL8 *rlrlCBCL8) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL8 = rlrlCBCL8;
	this->rlrlCBCL8->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState8::~prAtSemaphoreState8() {
}
void prAtSemaphoreState8::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL8->incCountsbCBCL8();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState8::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 1) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL8->incCountsbCBCL8();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL8->decCountsbCBCL8();
			prSemaphoreStateState = false;
		}
	}
}