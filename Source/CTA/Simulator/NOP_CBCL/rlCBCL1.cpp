#include "rlCBCL1.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds1.h"
rlCBCL1::rlCBCL1() {
	countsbCBCL1 = 0;
	countImpsbCBCL1 = 0;
}
rlCBCL1::~rlCBCL1() {
}
void rlCBCL1::incCountsbCBCL1(){
	this->countsbCBCL1++;
	//isApproved();
	this->prprAtSeconds1->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL1::decCountsbCBCL1(){
	this->countsbCBCL1--;
}
void rlCBCL1::incCountImpsbCBCL1(){
	this->countImpsbCBCL1++;
	isApproved();
}
void rlCBCL1::decCountImpsbCBCL1(){
	this->countImpsbCBCL1--;
}
bool rlCBCL1::isApproved() {
	bool ret = false;
	if ((countsbCBCL1 + countImpsbCBCL1) == 2) {
		this->semaphore_NOP->mtHTLG();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
