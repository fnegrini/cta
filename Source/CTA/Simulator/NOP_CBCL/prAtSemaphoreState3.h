#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL7;
class rlCBCL15;
class rlCBCL16;
class rlCBCL17;
class rlCBCL18;
class prAtSemaphoreState3 {
public:
	prAtSemaphoreState3(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL7 *rlrlCBCL7, rlCBCL15 *rlrlCBCL15, rlCBCL16 *rlrlCBCL16, rlCBCL17 *rlrlCBCL17, rlCBCL18 *rlrlCBCL18);
	~prAtSemaphoreState3(void);
	bool prSemaphoreStateState;
	void initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	void notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL7 *rlrlCBCL7;
	rlCBCL15 *rlrlCBCL15;
	rlCBCL16 *rlrlCBCL16;
	rlCBCL17 *rlrlCBCL17;
	rlCBCL18 *rlrlCBCL18;
};
