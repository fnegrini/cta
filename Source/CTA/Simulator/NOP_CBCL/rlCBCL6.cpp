#include "rlCBCL6.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds1.h"
rlCBCL6::rlCBCL6() {
	countsbCBCL6 = 0;
	countImpsbCBCL6 = 0;
}
rlCBCL6::~rlCBCL6() {
}
void rlCBCL6::incCountsbCBCL6(){
	this->countsbCBCL6++;
	//isApproved();
	this->prprAtSeconds1->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL6::decCountsbCBCL6(){
	this->countsbCBCL6--;
}
void rlCBCL6::incCountImpsbCBCL6(){
	this->countImpsbCBCL6++;
	isApproved();
}
void rlCBCL6::decCountImpsbCBCL6(){
	this->countImpsbCBCL6--;
}
bool rlCBCL6::isApproved() {
	bool ret = false;
	if((countsbCBCL6 + countImpsbCBCL6) == 2) {
		this->semaphore_NOP->mtVTLG();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
