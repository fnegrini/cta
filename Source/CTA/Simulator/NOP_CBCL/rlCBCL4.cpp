#include "rlCBCL4.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds4.h"
rlCBCL4::rlCBCL4() {
	countsbCBCL4 = 0;
	countImpsbCBCL4 = 0;
}
rlCBCL4::~rlCBCL4() {
}
void rlCBCL4::incCountsbCBCL4(){
	this->countsbCBCL4++;
	//isApproved();
	this->prprAtSeconds4->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL4::decCountsbCBCL4(){
	this->countsbCBCL4--;
}
void rlCBCL4::incCountImpsbCBCL4(){
	this->countImpsbCBCL4++;
	isApproved();
}
void rlCBCL4::decCountImpsbCBCL4(){
	this->countImpsbCBCL4--;
}
bool rlCBCL4::isApproved() {
	bool ret = false;
	if((countsbCBCL4 + countImpsbCBCL4) == 2) {
		this->semaphore_NOP->mtHTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
