#include "prAtSemaphoreState2.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL6.h"

prAtSemaphoreState2::prAtSemaphoreState2(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL6 *rlrlCBCL6) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL6 = rlrlCBCL6;
	this->rlrlCBCL6->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState2::~prAtSemaphoreState2() {
}
void prAtSemaphoreState2::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 2) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL6->incCountsbCBCL6();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState2::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 2) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL6->incCountsbCBCL6();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL6->decCountsbCBCL6();
			prSemaphoreStateState = false;
		}
	}
}