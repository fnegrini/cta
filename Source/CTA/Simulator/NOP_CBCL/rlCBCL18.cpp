#include "rlCBCL18.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds7.h"
#include "prAtSeconds8.h"
#include "prAtHVSS2.h"
rlCBCL18::rlCBCL18() {
	countsbCBCL18 = 0;
	countImpsbCBCL18 = 0;
}
rlCBCL18::~rlCBCL18() {
}
void rlCBCL18::incCountsbCBCL18(){
	this->countsbCBCL18++;
	//isApproved();
	this->prprAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtHVSS2->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->semaphore_NOP->atHVSS);
}
void rlCBCL18::decCountsbCBCL18(){
	this->countsbCBCL18--;
}
void rlCBCL18::incCountImpsbCBCL18(){
	this->countImpsbCBCL18++;
	isApproved();
}
void rlCBCL18::decCountImpsbCBCL18(){
	this->countImpsbCBCL18--;
}
bool rlCBCL18::isApproved() {
	bool ret = false;
	if((countsbCBCL18 + countImpsbCBCL18) == 4) {
		this->semaphore_NOP->mtVTLYCBCL();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
