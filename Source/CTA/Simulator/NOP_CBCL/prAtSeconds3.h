#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL3;
class rlCBCL8;
class prAtSeconds3 {
public:
	prAtSeconds3(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL3 *rlrlCBCL3, rlCBCL8 *rlrlCBCL8);
	~prAtSeconds3(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL3 *rlrlCBCL3;
	rlCBCL8 *rlrlCBCL8;
};
