#include "prAtSemaphoreState0.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL2.h"
#include "rlCBCL11.h"
#include "rlCBCL12.h"
#include "rlCBCL13.h"
#include "rlCBCL14.h"

prAtSemaphoreState0::prAtSemaphoreState0(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL2 *rlrlCBCL2, rlCBCL11 *rlrlCBCL11, rlCBCL12 *rlrlCBCL12, rlCBCL13 *rlrlCBCL13, rlCBCL14 *rlrlCBCL14) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL2 = rlrlCBCL2;
	this->rlrlCBCL2->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL11 = rlrlCBCL11;
	this->rlrlCBCL11->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL12 = rlrlCBCL12;
	this->rlrlCBCL12->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL13 = rlrlCBCL13;
	this->rlrlCBCL13->semaphore_NOP = this->semaphore_NOP;
	this->rlrlCBCL14 = rlrlCBCL14;
	this->rlrlCBCL14->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState0::~prAtSemaphoreState0() {
}
void prAtSemaphoreState0::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 0) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL2->incCountsbCBCL2();	
			this->rlrlCBCL11->incCountsbCBCL11();
			this->rlrlCBCL12->incCountsbCBCL12();
			this->rlrlCBCL13->incCountsbCBCL13();
			this->rlrlCBCL14->incCountsbCBCL14();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState0::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 0) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL2->incCountsbCBCL2();
			this->rlrlCBCL11->incCountsbCBCL11();
			this->rlrlCBCL12->incCountsbCBCL12();
			this->rlrlCBCL13->incCountsbCBCL13();
			this->rlrlCBCL14->incCountsbCBCL14();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL2->decCountsbCBCL2();		
			this->rlrlCBCL11->decCountsbCBCL11();
			this->rlrlCBCL12->decCountsbCBCL12();
			this->rlrlCBCL13->decCountsbCBCL13();
			this->rlrlCBCL14->decCountsbCBCL14();
			prSemaphoreStateState = false;
		}
	}
}