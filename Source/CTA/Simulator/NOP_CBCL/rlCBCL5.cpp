#include "rlCBCL5.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds5.h"
rlCBCL5::rlCBCL5() {
	countsbCBCL5 = 0;
	countImpsbCBCL5 = 0;
}
rlCBCL5::~rlCBCL5() {
}
void rlCBCL5::incCountsbCBCL5(){
	this->countsbCBCL5++;
	//isApproved();
	this->prprAtSeconds5->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL5::decCountsbCBCL5(){
	this->countsbCBCL5--;
}
void rlCBCL5::incCountImpsbCBCL5(){
	this->countImpsbCBCL5++;
	isApproved();
}
void rlCBCL5::decCountImpsbCBCL5(){
	this->countImpsbCBCL5--;
}
bool rlCBCL5::isApproved() {
	bool ret = false;
	if((countsbCBCL5 + countImpsbCBCL5) == 2) {
		this->semaphore_NOP->mtHTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
