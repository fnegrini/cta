#include "rlCBCL10.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds5.h"
rlCBCL10::rlCBCL10() {
	countsbCBCL10 = 0;
	countImpsbCBCL10 = 0;
}
rlCBCL10::~rlCBCL10() {
}
void rlCBCL10::incCountsbCBCL10(){
	this->countsbCBCL10++;
	//isApproved();
	this->prprAtSeconds5->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
}
void rlCBCL10::decCountsbCBCL10(){
	this->countsbCBCL10--;
}
void rlCBCL10::incCountImpsbCBCL10(){
	this->countImpsbCBCL10++;
	isApproved();
}
void rlCBCL10::decCountImpsbCBCL10(){
	this->countImpsbCBCL10--;
}
bool rlCBCL10::isApproved() {
	bool ret = false;
	if((countsbCBCL10 + countImpsbCBCL10) == 2) {
		this->semaphore_NOP->mtVTLR();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
