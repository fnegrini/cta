#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds1.h"
#include "prAtSeconds2.h"
#include "prAtSeconds3.h"
#include "prAtSeconds4.h"
#include "prAtSeconds5.h"
#include "prAtSeconds6.h"
#include "prAtSeconds7.h"
#include "prAtSeconds8.h"
#include "prAtSemaphoreState0.h"
#include "prAtSemaphoreState1.h"
#include "prAtSemaphoreState2.h"
#include "prAtSemaphoreState3.h"
#include "prAtSemaphoreState4.h"
#include "prAtSemaphoreState5.h"
#include "prAtSemaphoreState6.h"
#include "prAtSemaphoreState7.h"
#include "prAtSemaphoreState8.h"
#include "prAtSemaphoreState9.h"
#include "prAtHVSS1.h"
#include "prAtHVSS2.h"
#include "prAtVVSS1.h"
#include "prAtVVSS2.h"
#include "rlCBCL1.h"
#include "rlCBCL2.h"
#include "rlCBCL3.h"
#include "rlCBCL4.h"
#include "rlCBCL5.h"
#include "rlCBCL6.h"
#include "rlCBCL7.h"
#include "rlCBCL8.h"
#include "rlCBCL9.h"
#include "rlCBCL10.h"
#include "rlCBCL11.h"
#include "rlCBCL12.h"
#include "rlCBCL13.h"
#include "rlCBCL14.h"
#include "rlCBCL15.h"
#include "rlCBCL16.h"
#include "rlCBCL17.h"
#include "rlCBCL18.h"
#include "../Traffic_Light.h"
Semaphore_NOP_CBCL::Semaphore_NOP_CBCL(int semaphoreId, Traffic_Light* horizontalTrafficLight, Traffic_Light* verticalTrafficLight) :
Semaphore(semaphoreId, horizontalTrafficLight, verticalTrafficLight)
{
	this->rlrlCBCL1 = new rlCBCL1();
	this->rlrlCBCL10 = new rlCBCL10();
	this->rlrlCBCL11 = new rlCBCL11();
	this->rlrlCBCL12 = new rlCBCL12();
	this->rlrlCBCL13 = new rlCBCL13();
	this->rlrlCBCL14 = new rlCBCL14();
	this->rlrlCBCL15 = new rlCBCL15();
	this->rlrlCBCL16 = new rlCBCL16();
	this->rlrlCBCL17 = new rlCBCL17();
	this->rlrlCBCL18 = new rlCBCL18();
	this->rlrlCBCL2 = new rlCBCL2();
	this->rlrlCBCL3 = new rlCBCL3();
	this->rlrlCBCL4 = new rlCBCL4();
	this->rlrlCBCL5 = new rlCBCL5();
	this->rlrlCBCL6 = new rlCBCL6();
	this->rlrlCBCL7 = new rlCBCL7();
	this->rlrlCBCL8 = new rlCBCL8();
	this->rlrlCBCL9 = new rlCBCL9();

	this->prprAtSeconds1 = new prAtSeconds1(this, rlrlCBCL1, rlrlCBCL6);
	this->prprAtSeconds2 = new prAtSeconds2(this, rlrlCBCL2, rlrlCBCL7);
	this->prprAtSeconds3 = new prAtSeconds3(this, rlrlCBCL3, rlrlCBCL8);
	this->prprAtSeconds4 = new prAtSeconds4(this, rlrlCBCL4, rlrlCBCL9);
	this->prprAtSeconds5 = new prAtSeconds5(this, rlrlCBCL5, rlrlCBCL10);
	this->prprAtSeconds6 = new prAtSeconds6(this, rlrlCBCL11, rlrlCBCL12, rlrlCBCL15, rlrlCBCL16);
	this->prprAtSeconds7 = new prAtSeconds7(this, rlrlCBCL13, rlrlCBCL14, rlrlCBCL17, rlrlCBCL18);
	this->prprAtSeconds8 = new prAtSeconds8(this, rlrlCBCL13, rlrlCBCL14, rlrlCBCL17, rlrlCBCL18);
	this->prprAtSemaphoreState0 = new prAtSemaphoreState0(this, rlrlCBCL2, rlrlCBCL11, rlrlCBCL12, rlrlCBCL13, rlrlCBCL14);
	this->prprAtSemaphoreState1 = new prAtSemaphoreState1(this, rlrlCBCL4);
	this->prprAtSemaphoreState2 = new prAtSemaphoreState2(this, rlrlCBCL6);
	this->prprAtSemaphoreState3 = new prAtSemaphoreState3(this, rlrlCBCL7, rlrlCBCL15, rlrlCBCL16, rlrlCBCL17, rlrlCBCL18);
	this->prprAtSemaphoreState4 = new prAtSemaphoreState4(this, rlrlCBCL9);
	this->prprAtSemaphoreState5 = new prAtSemaphoreState5(this, rlrlCBCL1);
	this->prprAtSemaphoreState6 = new prAtSemaphoreState6(this, rlrlCBCL3);
	this->prprAtSemaphoreState7 = new prAtSemaphoreState7(this, rlrlCBCL5);
	this->prprAtSemaphoreState8 = new prAtSemaphoreState8(this, rlrlCBCL8);
	this->prprAtSemaphoreState9 = new prAtSemaphoreState9(this, rlrlCBCL10);
	this->prprAtHVSS1 = new prAtHVSS1(this, rlrlCBCL11, rlrlCBCL17);
	this->prprAtHVSS2 = new prAtHVSS2(this, rlrlCBCL12, rlrlCBCL18);
	this->prprAtVVSS1 = new prAtVVSS1(this, rlrlCBCL15, rlrlCBCL13);
	this->prprAtVVSS2 = new prAtVVSS2(this, rlrlCBCL16, rlrlCBCL14);

	this->rlrlCBCL1->prprAtSeconds1 = this->prprAtSeconds1;
	this->rlrlCBCL2->prprAtSeconds2 = this->prprAtSeconds2;
	this->rlrlCBCL3->prprAtSeconds3 = this->prprAtSeconds3;
	this->rlrlCBCL4->prprAtSeconds4 = this->prprAtSeconds4;
	this->rlrlCBCL5->prprAtSeconds5 = this->prprAtSeconds5;
	this->rlrlCBCL6->prprAtSeconds1 = this->prprAtSeconds1;
	this->rlrlCBCL7->prprAtSeconds2 = this->prprAtSeconds2;
	this->rlrlCBCL8->prprAtSeconds3 = this->prprAtSeconds3;
	this->rlrlCBCL9->prprAtSeconds4 = this->prprAtSeconds4;
	this->rlrlCBCL10->prprAtSeconds5 = this->prprAtSeconds5;
	this->rlrlCBCL11->prprAtSeconds6 = this->prprAtSeconds6;
	this->rlrlCBCL12->prprAtSeconds6 = this->prprAtSeconds6;
	this->rlrlCBCL13->prprAtSeconds7 = this->prprAtSeconds7;
	this->rlrlCBCL13->prprAtSeconds8 = this->prprAtSeconds8;
	this->rlrlCBCL14->prprAtSeconds7 = this->prprAtSeconds7;
	this->rlrlCBCL14->prprAtSeconds8 = this->prprAtSeconds8;
	this->rlrlCBCL15->prprAtSeconds6 = this->prprAtSeconds6;
	this->rlrlCBCL16->prprAtSeconds6 = this->prprAtSeconds6;
	this->rlrlCBCL17->prprAtSeconds7 = this->prprAtSeconds7;
	this->rlrlCBCL17->prprAtSeconds8 = this->prprAtSeconds8;
	this->rlrlCBCL18->prprAtSeconds7 = this->prprAtSeconds7;
	this->rlrlCBCL18->prprAtSeconds8 = this->prprAtSeconds8;

	this->rlrlCBCL11->prprAtHVSS1 = this->prprAtHVSS1;
	this->rlrlCBCL12->prprAtHVSS2 = this->prprAtHVSS2;
	this->rlrlCBCL13->prprAtVVSS1 = this->prprAtVVSS1;
	this->rlrlCBCL14->prprAtVVSS2 = this->prprAtVVSS2;
	this->rlrlCBCL15->prprAtVVSS1 = this->prprAtVVSS1;
	this->rlrlCBCL16->prprAtVVSS2 = this->prprAtVVSS2;
	this->rlrlCBCL17->prprAtHVSS1 = this->prprAtHVSS1;
	this->rlrlCBCL18->prprAtHVSS2 = this->prprAtHVSS2;


	atSeconds = 0;
	atSemaphoreState = 5;
	atHVSS = 0;
	atVVSS = 0;

	this->prprAtSeconds1->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds2->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds3->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds4->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds5->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds6->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds7->initprSecondssemaphore_NOPatSeconds(this->atSeconds);
	this->prprAtSeconds8->initprSecondssemaphore_NOPatSeconds(this->atSeconds);

	this->prprAtSemaphoreState0->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState1->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState2->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState3->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState4->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState5->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState6->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState7->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState8->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	this->prprAtSemaphoreState9->initprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);

	this->prprAtHVSS1->initprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);
	this->prprAtHVSS2->initprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);

	this->prprAtVVSS1->initprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
	this->prprAtVVSS2->initprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
}
Semaphore_NOP_CBCL::~Semaphore_NOP_CBCL(void)
{
}
void Semaphore_NOP_CBCL::setatSeconds(int atSeconds) {
	if (this->atSeconds != atSeconds) {
		this->atSeconds = atSeconds;
		if (this->rlrlCBCL1->countsbCBCL1 == 1 || this->rlrlCBCL6->countsbCBCL6 == 1){
			this->prprAtSeconds1->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL2->countsbCBCL2 == 1 || this->rlrlCBCL7->countsbCBCL7 == 1){
			this->prprAtSeconds2->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL3->countsbCBCL3 == 1 || this->rlrlCBCL8->countsbCBCL8 == 1){
			this->prprAtSeconds3->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL4->countsbCBCL4 == 1 || this->rlrlCBCL9->countsbCBCL9 == 1){
			this->prprAtSeconds4->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL5->countsbCBCL5 == 1 || this->rlrlCBCL10->countsbCBCL10 == 1){
			this->prprAtSeconds5->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL11->countsbCBCL11 == 1 || this->rlrlCBCL12->countsbCBCL12 == 1
			|| this->rlrlCBCL15->countsbCBCL15 == 1 || this->rlrlCBCL16->countsbCBCL16 == 1){
			this->prprAtSeconds6->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL13->countsbCBCL13 == 1 || this->rlrlCBCL14->countsbCBCL14 == 1
			|| this->rlrlCBCL17->countsbCBCL17 == 1 || this->rlrlCBCL18->countsbCBCL18 == 1){
			this->prprAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
		if (this->rlrlCBCL13->countsbCBCL13 == 1 || this->rlrlCBCL14->countsbCBCL14 == 1
			|| this->rlrlCBCL17->countsbCBCL17 == 1 || this->rlrlCBCL18->countsbCBCL18 == 1){
			this->prprAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->atSeconds);
		}
	}
}
int Semaphore_NOP_CBCL::getatSeconds() {
	return this->atSeconds;
}
void Semaphore_NOP_CBCL::setatSemaphoreState(int atSemaphoreState) {
	if (this->atSemaphoreState != atSemaphoreState) {
		this->atSemaphoreState = atSemaphoreState;

		this->prprAtSemaphoreState0->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState1->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState2->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState3->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState4->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState5->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState6->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState7->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState8->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
		this->prprAtSemaphoreState9->notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(this->atSemaphoreState);
	}
}
int Semaphore_NOP_CBCL::getatSemaphoreState() {
	return this->atSemaphoreState;
}
void Semaphore_NOP_CBCL::setatHVSS(int atHVSS) {
	if (this->atHVSS != atHVSS) {
		this->atHVSS = atHVSS;
		if (this->rlrlCBCL11->countsbCBCL11 == 1 || this->rlrlCBCL17->countsbCBCL17 == 1){
			this->prprAtHVSS1->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);
		}
		if (this->rlrlCBCL12->countsbCBCL12 == 1 || this->rlrlCBCL18->countsbCBCL18 == 1){
			this->prprAtHVSS2->notifyprVehicleSensorStatesemaphore_NOPatHVSS(this->atHVSS);
		}
	}
}
int Semaphore_NOP_CBCL::getatHVSS() {
	return this->atHVSS;
}
void Semaphore_NOP_CBCL::setatVVSS(int atVVSS) {
	if (this->atVVSS != atVVSS) {
		this->atVVSS = atVVSS;
		if (this->rlrlCBCL15->countsbCBCL15 == 1 || this->rlrlCBCL13->countsbCBCL13 == 1){
			this->prprAtVVSS1->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
		}
		if (this->rlrlCBCL16->countsbCBCL16 == 1 || this->rlrlCBCL14->countsbCBCL14 == 1){
			this->prprAtVVSS2->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->atVVSS);
		}
	}
}
int Semaphore_NOP_CBCL::getatVVSS() {
	return this->atVVSS;
}
void Semaphore_NOP_CBCL::mtRT() {
	//cout << "mtResetTimer Semaphore_NOP_CBCL" << endl;
	setatSeconds(0);
	this->SetCurrentCycleTime(0);
}
void Semaphore_NOP_CBCL::mtHTLG() {
	//cout << "mtHorizontalTrafficLightGreen Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(0);
	this->GetHorizontalTrafficLight()->SetState(GREEN);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBCL::mtHTLY() {
	//cout << "mtHorizontalTrafficLightYellow Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(1);
	this->GetHorizontalTrafficLight()->SetState(YELLOW);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBCL::mtHTLR() {
	//cout << "mtHorizontalTrafficLightRed Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(2);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBCL::mtVTLG() {
	//cout << "mtVerticalTrafficLightGreen Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(3);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(GREEN);
}
void Semaphore_NOP_CBCL::mtVTLY() {
	//cout << "mtVerticalTrafficLightYellow Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(4);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(YELLOW);
}
void Semaphore_NOP_CBCL::mtVTLR() {
	//cout << "mtVerticalTrafficLightRed Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(5);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBCL::mtHTLGCBCL() {
	//cout << "mtHorizontalTrafficLightGreenCBCL Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(6);
	this->GetHorizontalTrafficLight()->SetState(GREEN);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBCL::mtHTLYCBCL() {
	//cout << "mtHorizontalTrafficLightYellowCBCL Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(7);
	this->GetHorizontalTrafficLight()->SetState(YELLOW);
	this->GetVerticalTrafficLight()->SetState(RED);
}
void Semaphore_NOP_CBCL::mtVTLGCBCL() {
	//cout << "mtVerticalTrafficLightGreenCBCL Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(8);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(GREEN);
}
void Semaphore_NOP_CBCL::mtVTLYCBCL() {
	//cout << "mtVerticalTrafficLightRedCBCL Semaphore_NOP_CBCL" << endl;
	setatSemaphoreState(9);
	this->GetHorizontalTrafficLight()->SetState(RED);
	this->GetVerticalTrafficLight()->SetState(YELLOW);
}
void Semaphore_NOP_CBCL::increaseCycleTime(){
	currentCycleTime += 1;
	setatSeconds(currentCycleTime);
}

void Semaphore_NOP_CBCL::SetVerticalVehicleSensorState(int state){
	verticalVehicleSensorState = state;
	setatVVSS(verticalVehicleSensorState);
}
void Semaphore_NOP_CBCL::SetHorizontalVehicleSensorState(int state){
	horizontalVehicleSensorState = state;
	setatHVSS(horizontalVehicleSensorState);
}