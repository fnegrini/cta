#include "rlCBCL13.h"
#include "Semaphore_NOP_CBCL.h"
#include "prAtSeconds7.h"
#include "prAtSeconds8.h"
#include "prAtVVSS1.h"
rlCBCL13::rlCBCL13() {
	countsbCBCL13 = 0;
	countImpsbCBCL13 = 0;
}
rlCBCL13::~rlCBCL13() {
}
void rlCBCL13::incCountsbCBCL13(){
	this->countsbCBCL13++;
	//isApproved();
	this->prprAtSeconds7->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtSeconds8->notifyprSecondssemaphore_NOPatSeconds(this->semaphore_NOP->atSeconds);
	this->prprAtVVSS1->notifyprVehicleSensorStatesemaphore_NOPatVVSS(this->semaphore_NOP->atVVSS);
}
void rlCBCL13::decCountsbCBCL13(){
	this->countsbCBCL13--;
}
void rlCBCL13::incCountImpsbCBCL13(){
	this->countImpsbCBCL13++;
	isApproved();
}
void rlCBCL13::decCountImpsbCBCL13(){
	this->countImpsbCBCL13--;
}
bool rlCBCL13::isApproved() {
	bool ret = false;
	if((countsbCBCL13 + countImpsbCBCL13) == 4) {
		this->semaphore_NOP->mtHTLYCBCL();
		this->semaphore_NOP->mtRT();
		ret = true;
	} 
	return ret; 
 }
