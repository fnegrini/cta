#pragma once
#include <iostream>
using namespace std;
class Semaphore_NOP_CBCL;
class rlCBCL11;
class rlCBCL12;
class rlCBCL15;
class rlCBCL16;
class prAtSeconds6 {
public:
	prAtSeconds6(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL11 *rlrlCBCL11, rlCBCL12 *rlrlCBCL12, rlCBCL15 *rlrlCBCL15, rlCBCL16 *rlrlCBCL16);
	~prAtSeconds6(void);
	bool prSecondsState;
	void initprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	void notifyprSecondssemaphore_NOPatSeconds(int semaphore_NOPatSeconds);
	Semaphore_NOP_CBCL * semaphore_NOP;
	rlCBCL11 *rlrlCBCL11;
	rlCBCL12 *rlrlCBCL12;
	rlCBCL15 *rlrlCBCL15;
	rlCBCL16 *rlrlCBCL16;
};
