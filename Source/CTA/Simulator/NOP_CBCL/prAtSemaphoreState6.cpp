#include "prAtSemaphoreState6.h"
#include "Semaphore_NOP_CBCL.h"
#include "rlCBCL3.h"

prAtSemaphoreState6::prAtSemaphoreState6(Semaphore_NOP_CBCL *semaphore_NOP_CBCL, rlCBCL3 *rlrlCBCL3) {
	this->semaphore_NOP = semaphore_NOP_CBCL;

	prSemaphoreStateState = false;

	this->rlrlCBCL3 = rlrlCBCL3;
	this->rlrlCBCL3->semaphore_NOP = this->semaphore_NOP;
}
prAtSemaphoreState6::~prAtSemaphoreState6() {
}
void prAtSemaphoreState6::initprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 6) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL3->incCountsbCBCL3();
			prSemaphoreStateState = true;
		}
	}
}
void prAtSemaphoreState6::notifyprSemaphoreStatesemaphore_NOPatSemaphoreState(int semaphore_NOPatSemaphoreState) {
	if (semaphore_NOPatSemaphoreState == 6) {
		if (prSemaphoreStateState == false) {
			this->rlrlCBCL3->incCountsbCBCL3();
			prSemaphoreStateState = true;
		}
	}
	else {
		if (prSemaphoreStateState == true) {
			this->rlrlCBCL3->decCountsbCBCL3();
			prSemaphoreStateState = false;
		}
	}
}