///////////////////////////////////////////////////////////
//  Vehicle_Block.cpp
//  Implementation of the Class Vehicle_Block
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Vehicle_Block.h"
#include "Vehicle.h"
#include "Block.h"

Vehicle_Block::Vehicle_Block(){

}



Vehicle_Block::~Vehicle_Block(){

}

Block* Vehicle_Block::GetBlock(){

	return block;
}


int Vehicle_Block::GetLane(){

	return lane;
}


Vehicle* Vehicle_Block::GetVehicle(){

	return vehicle;
}

int Vehicle_Block::GetPosition(){

	return position;
}

void Vehicle_Block::SetBlock(Block *newVal){

	block = newVal;
}


void Vehicle_Block::SetLane(int newVal){

	lane = newVal;
}


void Vehicle_Block::SetVehicle(Vehicle *newVal){

	vehicle = newVal;
}

void Vehicle_Block::SetPosition(int newVal){

	position = newVal;
}