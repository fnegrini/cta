///////////////////////////////////////////////////////////
//  Intersection.cpp
//  Implementation of the Class Intersection
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Intersection.h"
#include "Semaphore.h"
#include "Block.h"


Intersection::Intersection(){

}



Intersection::~Intersection(){

}





Block* Intersection::GetNextHorizontalBlock(){

	return nextHorizontalBlock;
}


Block* Intersection::GetNextVerticalBlock(){

	return nextVerticalBlock;
}


Semaphore* Intersection::GetSemaphore(){

	return semaphore;
}


void Intersection::SetNextHorizontalBlock(Block* newVal){

	nextHorizontalBlock = newVal;
}


void Intersection::SetNextVerticalBlock(Block* newVal){

	nextVerticalBlock = newVal;
}


void Intersection::SetSemaphore(Semaphore* newVal){

	semaphore = newVal;
}