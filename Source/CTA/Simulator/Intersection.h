///////////////////////////////////////////////////////////
//  Intersection.h
//  Implementation of the Class Intersection
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

class Block;
class Semaphore;

class Intersection
{

public:
	Intersection();
	virtual ~Intersection();

	Block* GetNextHorizontalBlock();
	Block* GetNextVerticalBlock();
	Semaphore* GetSemaphore();
	void SetNextHorizontalBlock(Block* newVal);
	void SetNextVerticalBlock(Block* newVal);
	void SetSemaphore(Semaphore* newVal);

private:
	Block* nextHorizontalBlock;
	Block* nextVerticalBlock;
	Semaphore* semaphore;

};
