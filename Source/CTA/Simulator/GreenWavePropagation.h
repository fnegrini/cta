#pragma once

enum Green_Wave_Propagation
{
	No_Propagation,
	Horizontal_Propagation,
	Vertical_Propagation
};