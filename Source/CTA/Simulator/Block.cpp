///////////////////////////////////////////////////////////
//  Block.cpp
//  Implementation of the Class Block
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Block.h"
#include "Street.h"

int Block::blockCounter = 1;

Block::Block(Street* pStreet, int pBlockId, char* pTrafficLightId, int pLength)
	:trafficLight(this, pTrafficLightId), vehicleSensor(this)
{
	id = pBlockId;
	length = pLength;
	street = pStreet;
	freeSpace = GetMaxOccupancySpace();
	insertAtLane = 0;
	vehicleCounter = 0;
	calcPctOfTurns = 0;

	blockCounter++;
}

Block::~Block()
{

}

/*
Adiciona ve�culos no bloco
*/
void Block::addVehicle(Vehicle* pVehicle){

	if (hasEnoughSpace(pVehicle->GetLength())){


		freeSpace -= pVehicle->GetLength();
		insertAtLane = (insertAtLane + 1) % street->GetNumberOfLanes();
		vehicleCounter++;
		Vehicle_Block* pos = new Vehicle_Block();

		pos->SetLane(insertAtLane);
		pos->SetVehicle(pVehicle);
		pos->SetBlock(this);
		pVehicle->SetVehicleBlock(pos);
		vehicleBlocks.push_back(pos);

	}
}

void Block::addVehicle(Vehicle* pVehicle, int position){

	if (hasEnoughSpace(pVehicle->GetLength())){


		freeSpace -= pVehicle->GetLength();
		insertAtLane = (insertAtLane + 1) % street->GetNumberOfLanes();
		vehicleCounter++;
		Vehicle_Block* pos = new Vehicle_Block();

		pos->SetLane(insertAtLane);
		pos->SetVehicle(pVehicle);
		pos->SetBlock(this);
		pos->SetPosition(position);

		pVehicle->SetVehicleBlock(pos);
		vehicleBlocks.push_back(pos);

	}
}

int Block::GetId(){
	return id;
}


Intersection* Block::GetIntersection(){
	return intersection;
}


int Block::GetLength(){
	return length;
}


int Block::GetMaxCapacity(){
	return maxCapacity;
}


int Block::GetPercentageOfTurns(){
	return percentageOfTurns;
}


Street* Block::GetStreet(){
	return street;
}


Traffic_Light* Block::GetTrafficLight(){
	return &trafficLight;
}


Vehicle_Sensor* Block::GetVehicleSensor(){
	return &vehicleSensor;
}

int Block::GetMaxOccupancySpace(){
	return length * street->GetNumberOfLanes();
}

void Block::SetFreeSpace(int newVal){
	freeSpace = newVal;
}


void Block::SetId(int newVal){
	id = newVal;
}


void Block::SetIntersection(Intersection* newVal){
	intersection = newVal;
}


void Block::SetLength(int newVal){
	length = newVal;
}


void Block::SetPercentageOfTurns(int newVal){
	percentageOfTurns = newVal;
}


void Block::SetStreet(Street* newVal){
	street = newVal;
}

/*
Executa um passo da simula��o, movimenta os v�culos que est�o no bloco.
*/
void Block::simulationStep()
{

	if (trafficLight.GetState() == RED){
		float occupancyRate = (GetMaxOccupancySpace() - freeSpace) * 100.0 / GetMaxOccupancySpace();
		vehicleSensor.update(occupancyRate);
	}

	list<Vehicle_Block*> listAux = vehicleBlocks;
	
	for (list<Vehicle_Block*>::iterator it = listAux.begin(); it != listAux.end(); it++){
		Vehicle_Block* vb = (*it);
		if (NULL != vb){
			vb->GetVehicle()->move(&vehicleBlocks);
		}
	}

}

/*
Remove ve�culos do bloco, e incrementa a qtde de ve�culos que passaram
*/
void Block::removeVehicle(Vehicle_Block* pVehicleBlock){
	freeSpace += pVehicleBlock->GetVehicle()->GetLength();
	vehicleBlocks.remove(pVehicleBlock);
	if (calcPctOfTurns < 100)
	{
		calcPctOfTurns += percentageOfTurns;
	}
	else
	{
		calcPctOfTurns = 0;
	}
}

/*
Verifica se h� espa�o suficiente no bloco
*/
bool Block::hasEnoughSpace(int vehicleLength){
	return freeSpace >= vehicleLength ? true : false;
}

list<Vehicle_Block*>* Block::GetVehicleBlocks()
{
	return &vehicleBlocks;
}

int Block::GetCalcPctOfTurns()
{
	return calcPctOfTurns;
}

void Block::SetCalcPctOfTurns(int newCalcPctOfTurns)
{
	calcPctOfTurns = newCalcPctOfTurns;
}