///////////////////////////////////////////////////////////
//  Layout.h
//  Implementation of the Class Layout
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once
#include <vector>
using namespace std;
#include "InputFile.h"

class Layout
{

public:

	Layout();
	virtual ~Layout();

	void readFile();

	int GetNumberOfHorizontalStreets();
	void SetNumberOfHorizontalStreets(int pN);

	int GetNumberOfVerticalStreets();
	void SetNumberOfVerticalStreets(int pN);

	vector<int> GetNumberOfLanesHorizontal();
	vector<int> GetNumberOfLanesVertical();

	vector<float> GetHorizontalTrafficIntensity();
	vector<float> GetVerticalTrafficIntensity();

	vector<int> GetHorizontalPctOfTurns();
	vector<int> GetVerticalPctOfTurns();
	
private:
	int numberOfHorizontalStreets;
	int numberOfVerticalStreets;
	int nuOfLanes;
	float trafficIntensity;
	int pctOfTurns;

	vector<int> numberOfLanesHorizontal;
	vector<int> numberOfLanesVertical;

	vector<float> horizontalTrafficIntensity;
	vector<float> verticalTrafficIntensity;

	vector<int> horizontalPctOfTurns;
	vector<int> verticalPctOfTurns;

	InputFile inputFile;
	vector<string> config;
};
