///////////////////////////////////////////////////////////
//  Traffic_Light_State.h
//  Implementation of the Enumeration Traffic_Light_State
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

enum Traffic_Light_State
{
	GREEN,
	YELLOW,
	RED
};
