///////////////////////////////////////////////////////////
//  Block_List.cpp
//  Implementation of the Class Block_List
//  Created on:      27-jul-2015 11:02:46
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Block_List.h"


Block_List::Block_List(){

}



Block_List::~Block_List(){

}





void Block_List::addBlock(Block* pBlock){
	listBlock.push_back(pBlock);
}


void Block_List::addBlocksInverted(Block* pBlock){
	listBlock.push_front(pBlock);
}


void Block_List::clear(){
	listBlock.clear();
}


Block* Block_List::endBlock(){
	return  listBlock.GetCurrent()->GetElement();
}


Block* Block_List::firstBlock(){

	return  listBlock.GetFirst()->GetElement();
}


Block* Block_List::getBlock(int pos){
	List<Block>::Element<Block>* pAux = listBlock.GetFirst();
	for (int i = 0; i < pos; i++)
	{
		if (NULL != pAux->GetNext())
		{
			pAux = pAux->GetNext();
		}
	}
	return  pAux->GetElement();
}

void Block_List::doSimulationStep(){
	List<Block>::Element<Block>* pAux = listBlock.GetFirst();
	while (NULL != pAux){
		pAux->GetElement()->simulationStep();
		pAux = pAux->GetNext();
	}
}

void Block_List::configHorizontalIntersection()
{
	List<Block>::Element<Block>* pAux = listBlock.GetFirst();
	while (pAux != NULL && pAux->GetNext() != NULL)
	{
		Block* block = pAux->GetElement();
		Block* nextBlock = pAux->GetNext()->GetElement();
		block->GetIntersection()->SetNextHorizontalBlock(nextBlock);
		pAux = pAux->GetNext();
	}
}

void Block_List::configVerticalIntersection()
{
	List<Block>::Element<Block>* pAux = listBlock.GetFirst();
	while (pAux != NULL && pAux->GetNext() != NULL)
	{
		Block* block = pAux->GetElement();
		Block* nextBlock = pAux->GetNext()->GetElement();
		block->GetIntersection()->SetNextVerticalBlock(nextBlock);
		pAux = pAux->GetNext();
	}
}