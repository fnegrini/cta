#pragma once

enum Green_Wave_Type
{
	No_GreenWave,
	Green_Wave_Horizontal,
	Green_Wave_Vertical
};
