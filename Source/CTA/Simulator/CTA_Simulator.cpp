///////////////////////////////////////////////////////////
//  CTA_Simulator.cpp
//  Implementation of the Class CTA_Simulator
//  Created on:      27-jul-2015 11:02:48
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "CTA_Simulator.h"
#include "Block_List.h"

CTA_Simulator* CTA_Simulator::instance = new CTA_Simulator();
int CTA_Simulator::simulationTime = 0;

CTA_Simulator::CTA_Simulator()
:layout(),logger(){

}

CTA_Simulator::~CTA_Simulator(){
}

CTA_Simulator* CTA_Simulator::getInstance(){
	if (instance == NULL){
		instance = new CTA_Simulator();
	}
	return instance;
}

int CTA_Simulator::Getsteps(){

	return steps;
}


int CTA_Simulator::GetStepTimeLength(){

	return stepTimeLength;
}

/*
Inicializa Ruas, Blocos, intersecções e a interface
*/
void CTA_Simulator::initialize(Traffic_Lights_Controller* controller, int pSteps, int pStepTimeLength, AbstractFactory* factory, Interface* interfaceCTA){
	steps = pSteps;
	stepTimeLength = pStepTimeLength;
	initializeStreets();
	initializeBlocksAndIntersections(&horizontalStreets, &verticalStreets, controller, factory);	
	interfaceCTA->initialize(&horizontalStreets, &verticalStreets);
}

/*
Inicializa blocos e intersecções
*/
void CTA_Simulator::initializeBlocksAndIntersections(list <Street*> * horizontalStreets, 
	list <Street*> * verticalStreets,
	Traffic_Lights_Controller* controller, AbstractFactory* factory){
	std::list<Street*>::const_iterator hit, vit;
	int counter = 1;
	for (hit = horizontalStreets->begin(); hit != horizontalStreets->end(); ++hit){
		//for each street creates as many blocks as many crossing street it has
		for (vit = verticalStreets->begin(); vit != verticalStreets->end(); ++vit){

			Street* horizontalStreet = (*hit);
			Street* verticalStreet = (*vit);

			//TODO set percentage randomly
			//constructs the horizontal block
			Block* hBlock = buildBlockObject(horizontalStreet, verticalStreet, layout.GetHorizontalPctOfTurns()[counter-1]);
			horizontalStreet->addBlock(hBlock);

			//constructs the vertical block
			Block* vBlock = buildBlockObject(verticalStreet, horizontalStreet, layout.GetVerticalPctOfTurns()[counter - 1]);
			verticalStreet->addBlock(vBlock);			

			int tflState = 0;
			//constructs the Semaphore and the Intersection
			if (horizontalStreet->GetTrafficFacilitating())
			{
				tflState = 1;
			}
			else if (verticalStreet->GetTrafficFacilitating())
			{
				tflState = 2;
			}

            Semaphore* semaphore = factory->CreateSemaphore(counter++, hBlock->GetTrafficLight(), vBlock->GetTrafficLight(), tflState);
			hBlock->GetTrafficLight()->SetSemaphore(semaphore);
			vBlock->GetTrafficLight()->SetSemaphore(semaphore);
			
			Intersection* intersection = new Intersection();
			intersection->SetSemaphore(semaphore);

			hBlock->SetIntersection(intersection);
			vBlock->SetIntersection(intersection);

			semaphore->SetIntersection(intersection);

			controller->addSemaphore(semaphore);
		}
	}

	//Configura intesecções
	for (hit = horizontalStreets->begin(); hit != horizontalStreets->end(); ++hit)
	{
		Street* horizontalStreet = (*hit);
		horizontalStreet->configHorizontalIntersection();
	}
	for (vit = verticalStreets->begin(); vit != verticalStreets->end(); ++vit)
	{
		Street* verticalStreet = (*vit);
		verticalStreet->configVerticalIntersection();
	}
}

/*
Cria um bloco
*/
Block* CTA_Simulator::buildBlockObject(Street* targetStreet, Street* crossingStreet, int pctOfTurns){
	//constructs  block
	string id(targetStreet->GetId());
	id.append(crossingStreet->GetId());
	Block* block = new Block(targetStreet, Block::blockCounter, (char*)id.c_str());
	block->SetPercentageOfTurns(pctOfTurns);
	return block;
}


/*
Inicializa ruas
*/
void CTA_Simulator::initializeStreets(){
	cout << "Initializing the streets..." << endl;
	initializeHorizontalStreets(&horizontalStreets);
	initializeVerticalStreets(&verticalStreets);
}

/*
Inicia as ruas Hotizontais
*/
void CTA_Simulator::initializeHorizontalStreets(list <Street*> * pStreets){
	cout << "Initializing the horizontal streets..." << endl;
	for (int i = 0; i < layout.GetNumberOfHorizontalStreets(); i++){
		//id for the streets starting from the char A
		string id = "";
		id += 65 + i;
		//TODO set lanes
		int noOfLanes = 2;
		Street* st = new Street(layout.GetNumberOfLanesHorizontal()[i], layout.GetHorizontalTrafficIntensity()[i], false, (char*)id.c_str());
		Direction direction = WEST;
		if (i % 2 == 1){
			direction = EAST;
		}
		st->SetDirection(direction);
		pStreets->push_back(st);
	}
	cout << layout.GetNumberOfHorizontalStreets() << " horizontal streets created" << endl;
}

/*
Inicia as ruas Verticais
*/
void CTA_Simulator::initializeVerticalStreets(list <Street*> * pStreets){
	cout << "Initializing the vertical streets..." << endl;
	for (int i = 1; i <= layout.GetNumberOfVerticalStreets(); i++){
		//id for the streets starting from the char 1
		std::string id = "";
		id+= 48 + i;
		//TODO set lanes
		int noOfLanes = 2;
		bool Facilitating = false;
		if ((i >= 0) && (i<= 99)) {
			Facilitating = true;
		}
		Street* st = new Street(layout.GetNumberOfLanesVertical()[i - 1], layout.GetHorizontalTrafficIntensity()[i - 1], Facilitating, (char*)id.c_str());
		Direction direction = SOUTH;
		if (i % 2 == 1){
			direction = NORTH;
		}
		st->SetDirection(direction);
		pStreets->push_back(st);
	}
	cout << layout.GetNumberOfVerticalStreets() << " vertical streets created" << endl;
}

/*
Execução da Thread que faz a simulação do mundo
*/
void CTA_Simulator::run(){
	for (int i = 1; i <= steps; i++){
		lock();
		simulationStep();
		unlock();
		Sleep(stepTimeLength);
	}
}


void CTA_Simulator::Setsteps(int newVal){

	steps = newVal;
}


void CTA_Simulator::SetStepTimeLength(int newVal){

	stepTimeLength = newVal;
}

/*
Executa um passo da simulaçao do mundo
*/
void CTA_Simulator::simulationStep(){
	simulationTime++;
	runStreetSimulationStep(&verticalStreets);
	runStreetSimulationStep(&horizontalStreets);
}

/*
Faz a simulação da lista de ruas passada por parametro
*/
void CTA_Simulator::runStreetSimulationStep(std::list <Street*> *pStreets){
	std::list<Street*>::const_iterator it;
	for (it = pStreets->begin(); it != pStreets->end(); ++it){
		Street* st = (*it);
		st->simulationStep();
	}
}

/*
Gera log a partir da string passada como parametro
*/
void CTA_Simulator::log(string message){
	logger << message;
}

/*
Gera um evento de log
*/
void CTA_Simulator::logEvent(int eventId, int vehicleId){
	stringstream ss;
	ss << simulationTime << ":"
		<< eventId << ":"
		<< vehicleId;
	log(ss.str());
}

void CTA_Simulator::logEvent(int eventId, int vehicleId,string street, int blockId, int lane, int position){
	stringstream ss;
	ss << simulationTime << ":"
		<< eventId << ":"
		<< vehicleId << ":"
		<< street << ":"
		<< blockId << ":"
		<< lane << ":"
		<< position;
	log(ss.str());
}
