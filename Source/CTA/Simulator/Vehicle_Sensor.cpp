///////////////////////////////////////////////////////////
//  Vehicle_Sensor.cpp
//  Implementation of the Class Vehicle_Sensor
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Vehicle_Sensor.h"
#include "Block.h"


Vehicle_Sensor::Vehicle_Sensor(Block* pBlock){
	block = pBlock;
}



Vehicle_Sensor::~Vehicle_Sensor(){

}

Vehicle_Sensor_State Vehicle_Sensor::GetVehicleSensorState(){

	return vehicleSensorState;
}


void Vehicle_Sensor::notify(Semaphore* semaphore, int state){
	if (semaphore->GetHorizontalTrafficLight() == this->GetBlock()->GetTrafficLight()
		&& semaphore->GetHorizontalVehicleSensorState() != state){
		semaphore->SetHorizontalVehicleSensorState(state);
	}
	else if (semaphore->GetVerticalTrafficLight() == this->GetBlock()->GetTrafficLight()
		&& semaphore->GetVerticalVehicleSensorState() != state){
		semaphore->SetVerticalVehicleSensorState(state);
	}
}


void Vehicle_Sensor::SetVehicleSensorState(Vehicle_Sensor_State newVal){

	vehicleSensorState = newVal;
}


void Vehicle_Sensor::update(float blockOccupancyRate){
	if (blockOccupancyRate<60){
		SetVehicleSensorState(FEW);
		notify(this->GetBlock()->GetTrafficLight()->GetSemaphore(), 0);
	}
	else if (blockOccupancyRate >= 60 && blockOccupancyRate < 100){
		SetVehicleSensorState(MANY);
		notify(this->GetBlock()->GetTrafficLight()->GetSemaphore(), 1);
	}
	else{
		notify(this->GetBlock()->GetTrafficLight()->GetSemaphore(), 2);
	}
}

Block* Vehicle_Sensor::GetBlock(){
	return block;
}