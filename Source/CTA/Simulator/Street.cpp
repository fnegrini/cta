///////////////////////////////////////////////////////////
//  Street.cpp
//  Implementation of the Class Street
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Street.h"
#include "CTA_Simulator.h"


Street::Street(const int pNoOfLanes, const float pTrafficIntensity, const bool pTrafficFacilitating, const string pId){
	numberOfLanes = pNoOfLanes;
	trafficIntensity = pTrafficIntensity;
	id = pId;
	newVehiclePct = 0.0;
	trafficFacilitating = pTrafficFacilitating;
}

Street::~Street(){
}

/*
Adiciona bloco na rua, de acordo com sua dire��o
*/
void Street::addBlock(Block* pBlock){
	if (direction == SOUTH || direction == EAST){
		listBlocks.addBlock(pBlock);
	}
	else{
		//IF NORTH or WEST the list is done with the order inverted
		listBlocks.addBlocksInverted(pBlock);
	}
}

/*
Adiciona o ve�culos criados no primeiro bloco
*/
void Street::addVehicleFromEntryPoint(){
	if (!entryPoint.empty()){
		Block* firstBlock = listBlocks.firstBlock();

		std::list<Vehicle*> pAux = entryPoint;
		std::list<Vehicle*>::const_iterator it;
		for (it = pAux.begin(); it != pAux.end(); it++){
			Vehicle* v = (*it);
			firstBlock->addVehicle(v);
			removeVehicleFromEntryPoint(v);
		}
	}
}

/*
Adiciona ve�culo no ponto de entrada
*/
void Street::addVehicleToEntryPoint(Vehicle* vehicle){
	entryPoint.push_back(vehicle);
}

/*
Adiciona ve�culo no ponto de Sa�da
*/
void Street::addVehicleToExitPoint(Vehicle* vehicle)
{
	exitPoint.push_back(vehicle);
}

/*
Limpa lista de blocos, ou seja, retira os blocos da rua.
*/
void Street::clearBlocks(){
	listBlocks.clear();
}

/*
Limpa lista de ve�culos no ponto de entrada
*/
void Street::clearEntryPoint(){
	entryPoint.clear();
}

/*
Limpa lista de ve�culos no ponto de entrada
*/
void Street::clearExitPoint(){
	exitPoint.clear();
}

Direction Street::GetDirection(){

	return direction;
}


string Street::GetId(){

	return id;
}


Block_List Street::GetListBlocks(){

	return listBlocks;
}


int Street::GetNumberOfLanes(){

	return numberOfLanes;
}

float Street::GetTrafficIntensity(){

	return trafficIntensity;
}

/*
Remove ve�culos do ponto de entrada
*/
void Street::removeVehicleFromEntryPoint(Vehicle* vehicle){
	entryPoint.remove(vehicle);
}


void Street::SetDirection(Direction newVal){

	direction = newVal;
}


void Street::SetId(string newVal){

	id = newVal;
}


void Street::SetListBlocks(Block_List newVal){

	listBlocks = newVal;
}


void Street::SetNumberOfLanes(int newVal){

	numberOfLanes = newVal;
}

void Street::SetTrafficIntensity(float newVal){

	trafficIntensity = newVal;
}

/*
Faz a simula��o dos elementos da rua.
*/
void Street::simulationStep(){
	
	listBlocks.doSimulationStep();
	//if (direction == SOUTH)
	//{
		newVehiclePct = newVehiclePct + trafficIntensity;
		while (newVehiclePct >= 1.0){
			Vehicle* v = new Vehicle(Vehicle::getCount());
			addVehicleToEntryPoint(v);
			newVehiclePct = newVehiclePct - 1;
			CTA_Simulator::getInstance()->logEvent(Logger::EV_VEHICLE_ENTRY_POINT, v->GetId());
		}
		addVehicleFromEntryPoint();
	//}
}

void Street::configHorizontalIntersection()
{
	listBlocks.configHorizontalIntersection();
}
void Street::configVerticalIntersection()
{
	listBlocks.configVerticalIntersection();
}

Block* Street::GetBlock(int pos){
	return  listBlocks.getBlock(pos);
}

void Street::SetTrafficFacilitating(bool newValue)
{
	trafficFacilitating = newValue;
}

bool Street::GetTrafficFacilitating()
{
	return trafficFacilitating;
}

