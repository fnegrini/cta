///////////////////////////////////////////////////////////
//  Logger.h
//  Implementation of the Class Logger
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
using std::ofstream;
using std::ifstream;
using std::string;
using std::endl;
using std::cerr;
using std::ios;

class Logger
{

private:
	ofstream LOG;

public:

	const static int EV_VEHICLE_ENTER_BLOCK; //event triggered when the vehicle enters a block
	const static int EV_VEHICLE_STOP; //event triggered when the vehicle stops
	const static int EV_VEHICLE_ENTRY_POINT; //event triggered when the vehicle entries the simulation
	const static int EV_VEHICLE_EXIT_POINT; //event triggered when the vehicle exits the simulation
	const static int EV_VEHICLE_MOVE; //event triggered when the vehicle moves within a given block

	Logger();
	virtual ~Logger();
	void log(string msg);
	ofstream* getLogger();
	void operator<<(string msg);
};
