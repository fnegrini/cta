///////////////////////////////////////////////////////////
//  Direction.h
//  Implementation of the Enumeration Direction
//  Created on:      27-jul-2015 11:02:48
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

enum Direction
{
	NORTH,
	EAST,
	SOUTH,
	WEST
};