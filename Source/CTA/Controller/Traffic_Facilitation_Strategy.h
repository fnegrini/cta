///////////////////////////////////////////////////////////
//  Traffic_Facilitation_Strategy.h
//  Implementation of the Class Traffic_Facilitation_Strategy
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Control_Strategy.h"

class Traffic_Facilitation_Strategy : public Traffic_Control_Strategy
{

public:
	Traffic_Facilitation_Strategy();
	virtual ~Traffic_Facilitation_Strategy();

	void control(std::list <Semaphore*>* pSemaphores);

};
