///////////////////////////////////////////////////////////
//  Congestion_Level_Strategy_NOP.h
//  Implementation of the Class Congestion_Level_Strategy_NOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Control_Strategy.h"

class Congestion_Level_Strategy_NOP : public Traffic_Control_Strategy
{

public:
	Congestion_Level_Strategy_NOP();
	virtual ~Congestion_Level_Strategy_NOP();

	void control(std::list <Semaphore*>* pSemaphores);

};
