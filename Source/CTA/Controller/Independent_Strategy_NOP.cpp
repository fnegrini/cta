///////////////////////////////////////////////////////////
//  Independent_Strategy_NOP.cpp
//  Implementation of the Class Independent_Strategy_NOP
//  Created on:      27-jul-2015 11:02:49
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Independent_Strategy_NOP.h"


Independent_Strategy_NOP::Independent_Strategy_NOP(){

}



Independent_Strategy_NOP::~Independent_Strategy_NOP(){

}

void Independent_Strategy_NOP::control(std::list <Semaphore*>* pSemaphores){
	std::list<Semaphore*>::const_iterator it;
	for (it = pSemaphores->begin(); it != pSemaphores->end(); ++it){
		Semaphore* semaphore = (*it);
		semaphore->increaseCycleTime();
	}
}