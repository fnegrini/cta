///////////////////////////////////////////////////////////
//  Traffic_Facilitation_Strategy_NOP.cpp
//  Implementation of the Class Traffic_Facilitation_Strategy_NOP
//  Created on:      30-jul-2015 15:06:15
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Traffic_Facilitation_Strategy_NOP.h"


Traffic_Facilitation_Strategy_NOP::Traffic_Facilitation_Strategy_NOP(){

}



Traffic_Facilitation_Strategy_NOP::~Traffic_Facilitation_Strategy_NOP(){

}





void Traffic_Facilitation_Strategy_NOP::control(std::list <Semaphore*>* pSemaphores){
	std::list<Semaphore*>::const_iterator it;
	for (it = pSemaphores->begin(); it != pSemaphores->end(); ++it) {
		Semaphore* semaphore = (*it);
		semaphore->increaseCycleTime();
	}

}