///////////////////////////////////////////////////////////
//  Independent_Strategy.cpp
//  Implementation of the Class Independent_Strategy
//  Created on:      27-jul-2015 11:02:48
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Independent_Strategy.h"
#include "../Simulator/Traffic_Light_State.h"
#include "../Simulator/Semaphore.h"
#include "../Simulator/Traffic_Light.h"

#include<iostream>
using namespace std;


Independent_Strategy::Independent_Strategy(){

}



Independent_Strategy::~Independent_Strategy(){

}

void Independent_Strategy::control(std::list <Semaphore*>* pSemaphores){
	std::list<Semaphore*>::const_iterator it;
	for (it = pSemaphores->begin(); it != pSemaphores->end(); ++it){
		Semaphore* semaphore = (*it);
		//cout << "Tempo: " << semaphore->GetCurrentCycleTime()<< endl;
		(*semaphore)++;

		if (semaphore->GetCurrentCycleTime() == 2){
			semaphore->GetHorizontalTrafficLight()->SetState(GREEN);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			//cout << "mtHorizontalTrafficLightGREEN Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 38){
			semaphore->GetHorizontalTrafficLight()->SetState(YELLOW);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			//cout << "mtHorizontalTrafficLightYELLOW Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 45){
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			//cout << "mtHorizontalTrafficLightRED Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 47){
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(GREEN);
			//cout << "mtVerticalTrafficLightGREEN Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 85){
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(YELLOW);
			//cout << "mtVerticalTrafficLightYELLOW Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 90){
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightRED Semaphore_NOP" << endl;
		}
	}
}