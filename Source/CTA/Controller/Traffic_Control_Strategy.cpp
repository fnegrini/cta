///////////////////////////////////////////////////////////
//  Traffic_Control_Strategy.cpp
//  Implementation of the Class Traffic_Control_Strategy
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Traffic_Control_Strategy.h"
#include "../Simulator/Semaphore.h"
#include "../Simulator/Traffic_Light_State.h"
#include "../Simulator/Traffic_Light.h"


Traffic_Control_Strategy::Traffic_Control_Strategy(){

}



Traffic_Control_Strategy::~Traffic_Control_Strategy(){

}





void Traffic_Control_Strategy::control(std::list <Semaphore*>* pSemaphores){

}


void Traffic_Control_Strategy::initialize(std::list <Semaphore*> * pSemaphores){
	std::list<Semaphore*>::const_iterator it;
	for (it = pSemaphores->begin(); it != pSemaphores->end(); ++it){
		Semaphore* semaphore = (*it);
		semaphore->GetHorizontalTrafficLight()->SetState(RED);
		semaphore->GetVerticalTrafficLight()->SetState(RED);
		semaphore->SetCurrentCycleTime(0);
		semaphore->SetSemaphoreState(5);
		semaphore->SetVerticalVehicleSensorState(0);
		semaphore->SetHorizontalVehicleSensorState(0);
		semaphore->SetSensorState(MANY);
	}
}