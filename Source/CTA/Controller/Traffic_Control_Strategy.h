///////////////////////////////////////////////////////////
//  Traffic_Control_Strategy.h
//  Implementation of the Class Traffic_Control_Strategy
//  Created on:      27-jul-2015 11:02:50
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "../Simulator/Semaphore.h"

#include <iostream>
#include <list>
using namespace std;


class Traffic_Control_Strategy
{
public:
	Traffic_Control_Strategy();
	virtual ~Traffic_Control_Strategy();

	virtual void control(list<Semaphore*>* pSemaphores);
	virtual void initialize(list<Semaphore*> * pSemaphores);
};
