///////////////////////////////////////////////////////////
//  Congestion_Level_Strategy_NOP.cpp
//  Implementation of the Class Congestion_Level_Strategy_NOP
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Congestion_Level_Strategy_NOP.h"


Congestion_Level_Strategy_NOP::Congestion_Level_Strategy_NOP(){

}



Congestion_Level_Strategy_NOP::~Congestion_Level_Strategy_NOP(){

}

void Congestion_Level_Strategy_NOP::control(std::list <Semaphore*>* pSemaphores){
	std::list<Semaphore*>::const_iterator it;
	for (it = pSemaphores->begin(); it != pSemaphores->end(); ++it){
		Semaphore* semaphore = (*it);
		semaphore->increaseCycleTime();
	}
}