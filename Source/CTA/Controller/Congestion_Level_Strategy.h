///////////////////////////////////////////////////////////
//  Congestion_Level_Strategy.h
//  Implementation of the Class Congestion_Level_Strategy
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Control_Strategy.h"

class Congestion_Level_Strategy : public Traffic_Control_Strategy
{

public:
	Congestion_Level_Strategy();
	virtual ~Congestion_Level_Strategy();

	void control(std::list <Semaphore*>* pSemaphores);

};
