///////////////////////////////////////////////////////////
//  Independent_Strategy_NOP.h
//  Implementation of the Class Independent_Strategy_NOP
//  Created on:      27-jul-2015 11:02:48
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Control_Strategy.h"

class Independent_Strategy_NOP : public Traffic_Control_Strategy
{

public:
	Independent_Strategy_NOP();
	virtual ~Independent_Strategy_NOP();

	void control(std::list <Semaphore*>* pSemaphores);

};
