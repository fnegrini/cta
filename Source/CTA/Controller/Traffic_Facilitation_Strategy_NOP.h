///////////////////////////////////////////////////////////
//  Traffic_Facilitation_Strategy_NOP.h
//  Implementation of the Class Traffic_Facilitation_Strategy_NOP
//  Created on:      30-jul-2015 15:06:15
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Control_Strategy.h"

class Traffic_Facilitation_Strategy_NOP : public Traffic_Control_Strategy
{

public:
	Traffic_Facilitation_Strategy_NOP();
	virtual ~Traffic_Facilitation_Strategy_NOP();

	void control(std::list<Semaphore*>* pSemaphores);

};

