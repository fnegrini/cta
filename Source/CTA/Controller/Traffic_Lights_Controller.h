///////////////////////////////////////////////////////////
//  Traffic_Lights_Controller.h
//  Implementation of the Class Traffic_Lights_Controller
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once
#include "Thread.h"
#include "Traffic_Control_Strategy.h"
#include "../Simulator/Semaphore.h"
#include "../Util/PerformanceMeasure.h"
#include <list>
using namespace std;

class Traffic_Lights_Controller : public Thread, public PerformanceMeasure
{

public:
	Traffic_Lights_Controller(int pSteps, int pStepLength, Traffic_Control_Strategy* pStrategy);
	virtual ~Traffic_Lights_Controller();

	void addSemaphore(Semaphore* semaphore);
	virtual void control();
	Traffic_Control_Strategy* GetStrategy();
	virtual void run();
	void SetStrategy(Traffic_Control_Strategy* newVal);

private:
	int controllerTime;
	Traffic_Control_Strategy* strategy;
	list<Semaphore*> semaphores;
	int steps;
	int stepLength;

};

