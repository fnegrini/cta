///////////////////////////////////////////////////////////
//  Traffic_Lights_Controller.cpp
//  Implementation of the Class Traffic_Lights_Controller
//  Created on:      27-jul-2015 11:02:51
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Traffic_Lights_Controller.h"


Traffic_Lights_Controller::Traffic_Lights_Controller(int pSteps, int pStepLength, Traffic_Control_Strategy* pStrategy) :PerformanceMeasure(){
	strategy = pStrategy;
	steps = pSteps;
	stepLength = pStepLength;
}



Traffic_Lights_Controller::~Traffic_Lights_Controller(){

}


void Traffic_Lights_Controller::addSemaphore(Semaphore* semaphore){
	semaphores.push_back(semaphore);
}


void Traffic_Lights_Controller::control(){
	controllerTime++;
	if (controllerTime == 1){
		strategy->initialize(&semaphores);
	}
	else{
		strategy->control(&semaphores);
	}
}


Traffic_Control_Strategy* Traffic_Lights_Controller::GetStrategy(){

	return strategy;
}


void Traffic_Lights_Controller::run(){
	resetMeasure();
	for (int i = 1; i <= steps; i++){
		lock();
		startMeasure();
		control();
		stopMeasure();
		showMeasureResult();
		unlock();
		Sleep(stepLength);
	}
}


void Traffic_Lights_Controller::SetStrategy(Traffic_Control_Strategy* newVal){

	strategy = newVal;
}