///////////////////////////////////////////////////////////
//  Independent_Strategy.h
//  Implementation of the Class Independent_Strategy
//  Created on:      27-jul-2015 11:02:48
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#pragma once

#include "Traffic_Control_Strategy.h"

class Independent_Strategy : public Traffic_Control_Strategy
{

public:
	Independent_Strategy();
	virtual ~Independent_Strategy();

	void control(std::list <Semaphore*>* pSemaphores);

};
