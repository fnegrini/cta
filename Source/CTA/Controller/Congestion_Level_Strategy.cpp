///////////////////////////////////////////////////////////
//  Congestion_Level_Strategy.cpp
//  Implementation of the Class Congestion_Level_Strategy
//  Created on:      27-jul-2015 11:02:47
//  Original author: Leonardo Faix Pordeus
///////////////////////////////////////////////////////////

#include "Congestion_Level_Strategy.h"
#include "../Simulator/Traffic_Light.h"

Congestion_Level_Strategy::Congestion_Level_Strategy(){

}



Congestion_Level_Strategy::~Congestion_Level_Strategy(){

}


void Congestion_Level_Strategy::control(std::list <Semaphore*>* pSemaphores){
	std::list<Semaphore*>::const_iterator it;

	for (it = pSemaphores->begin(); it != pSemaphores->end(); ++it){
		Semaphore* semaphore = (*it);

		semaphore->increaseCycleTime();

		if (semaphore->GetCurrentCycleTime() == 2
			&& semaphore->GetSemaphoreState() == 5){
			semaphore->SetSemaphoreState(0);
			semaphore->GetHorizontalTrafficLight()->SetState(GREEN);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightGreen Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 38
			&& semaphore->GetSemaphoreState() == 0){
			semaphore->SetSemaphoreState(1);
			semaphore->GetHorizontalTrafficLight()->SetState(YELLOW);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightYellow Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 30
			&& semaphore->GetSemaphoreState() == 6){
			semaphore->SetSemaphoreState(1);
			semaphore->GetHorizontalTrafficLight()->SetState(YELLOW);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightYellow Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 6
			&& semaphore->GetSemaphoreState() == 1){
			semaphore->SetSemaphoreState(2);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightRed Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 6
			&& semaphore->GetSemaphoreState() == 7){
			semaphore->SetSemaphoreState(2);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightRed Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 2
			&& semaphore->GetSemaphoreState() == 2){
			semaphore->SetSemaphoreState(3);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(GREEN);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightGreen Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 38
			&& semaphore->GetSemaphoreState() == 3){
			semaphore->SetSemaphoreState(4);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(YELLOW);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightYellow Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 30
			&& semaphore->GetSemaphoreState() == 8){
			semaphore->SetSemaphoreState(4);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(YELLOW);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightYellow Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 6
			&& semaphore->GetSemaphoreState() == 4){
			semaphore->SetSemaphoreState(5);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightRed Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() == 6
			&& semaphore->GetSemaphoreState() == 9){
			semaphore->SetSemaphoreState(5);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightRed Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() <= 17
			&& semaphore->GetSemaphoreState() == 0
			&& semaphore->GetHorizontalVehicleSensorState() == 1){
			semaphore->SetSemaphoreState(6);
			semaphore->GetHorizontalTrafficLight()->SetState(GREEN);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			//cout << "mtHorizontalTrafficLightGreenCBCL Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() <= 17
			&& semaphore->GetSemaphoreState() == 0
			&& semaphore->GetHorizontalVehicleSensorState() == 2){
			semaphore->SetSemaphoreState(6);
			semaphore->GetHorizontalTrafficLight()->SetState(GREEN);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			//cout << "mtHorizontalTrafficLightGreenCBCL Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() >= 18
			&& semaphore->GetCurrentCycleTime() < 32
			&& semaphore->GetSemaphoreState() == 0
			&& semaphore->GetVerticalVehicleSensorState() == 1){
			semaphore->SetSemaphoreState(7);
			semaphore->GetHorizontalTrafficLight()->SetState(YELLOW);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightYellowCBCL Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() >= 18
			&& semaphore->GetCurrentCycleTime() < 32
			&& semaphore->GetSemaphoreState() == 0
			&& semaphore->GetVerticalVehicleSensorState() == 2){
			semaphore->SetSemaphoreState(7);
			semaphore->GetHorizontalTrafficLight()->SetState(YELLOW);
			semaphore->GetVerticalTrafficLight()->SetState(RED);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtHorizontalTrafficLightYellowCBCL Semaphore_NOP" << endl;
		}

		else if (semaphore->GetCurrentCycleTime() <= 17
			&& semaphore->GetSemaphoreState() == 3
			&& semaphore->GetVerticalVehicleSensorState() == 1){
			semaphore->SetSemaphoreState(8);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(GREEN);
			//cout << "mtVerticalTrafficLightGreenCBCL Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() <= 17
			&& semaphore->GetSemaphoreState() == 3
			&& semaphore->GetVerticalVehicleSensorState() == 2){
			semaphore->SetSemaphoreState(8);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(GREEN);
			//cout << "mtVerticalTrafficLightGreenCBCL Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() >= 18
			&& semaphore->GetCurrentCycleTime() < 32
			&& semaphore->GetSemaphoreState() == 3
			&& semaphore->GetHorizontalVehicleSensorState() == 1){
			semaphore->SetSemaphoreState(9);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(YELLOW);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightRedCBCL Semaphore_NOP" << endl;
		}
		else if (semaphore->GetCurrentCycleTime() >= 18
			&& semaphore->GetCurrentCycleTime() < 32
			&& semaphore->GetSemaphoreState() == 3
			&& semaphore->GetHorizontalVehicleSensorState() == 2){
			semaphore->SetSemaphoreState(9);
			semaphore->GetHorizontalTrafficLight()->SetState(RED);
			semaphore->GetVerticalTrafficLight()->SetState(YELLOW);
			semaphore->SetCurrentCycleTime(0);
			//cout << "mtVerticalTrafficLightRedCBCL Semaphore_NOP" << endl;
		}

	}
}