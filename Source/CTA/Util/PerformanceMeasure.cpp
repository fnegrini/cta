#include "PerformanceMeasure.h"


PerformanceMeasure::PerformanceMeasure()
{
	QueryPerformanceFrequency(&Frequency);
	timeTotal = 0.0;
}


PerformanceMeasure::~PerformanceMeasure()
{
}

void PerformanceMeasure::startMeasure()
{
	QueryPerformanceCounter(&StartingTime);
}

void PerformanceMeasure::stopMeasure()
{
	QueryPerformanceCounter(&EndingTime);
}

void PerformanceMeasure::resetMeasure()
{
	timeTotal = 0.0;
}

void PerformanceMeasure::showMeasureResult()
{
	ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
	ElapsedMicroseconds.QuadPart *= 1000000;
	ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;
	timeTotal = timeTotal + ElapsedMicroseconds.QuadPart;
	cout << "Miliseconds: " << timeTotal << endl;
}