#pragma once
#include "windows.h"
#include <iostream>
using namespace std;

class PerformanceMeasure
{
protected:
	LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
	LARGE_INTEGER Frequency;
	float timeTotal;
public:
	PerformanceMeasure();
	virtual ~PerformanceMeasure();

	void startMeasure();
	void stopMeasure();
	void resetMeasure();
	void showMeasureResult();
};

